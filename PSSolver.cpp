#include "PSSolver.h"
#include <QMessageBox>

#define Element(a,b,c)  a[b*nDim+c]
#define RowVector(a,b)  (&a[b*nDim])
#define CopyVector(a,b) memcpy((a),(b),nDim*sizeof(double))

PSSolver::PSSolver(int dim) :
                    nDim(dim),
                    iterations(0), bestEnergy(1.0E20),
                    trialSolution(0), bestSolution(0), trialPoint(0),
                    popEnergy(0), population(0), pattern(0)
{
    trialSolution = new double[nDim];
    bestSolution  = new double[nDim];
    trialPoint	  = new double[nDim];
    popEnergy	  = new double[2*nDim];
    population	  = new double[2*nDim*nDim];
    pattern       = new double[2*nDim*nDim];
    initPoint     = new double[27*nDim];
    initEnergy    = new double[27];
    scaleDelta = new double[nDim];

    min_ = new double[nDim];
    max_ = new double[nDim];
    return;
}

PSSolver::~PSSolver(void)
{
    if (trialSolution) delete trialSolution;
    if (bestSolution) delete bestSolution;
    if (trialPoint) delete trialPoint;
    if (popEnergy) delete popEnergy;
    if (population) delete population;
    if (pattern) delete pattern;

    if (initPoint) delete initPoint;
    if (initEnergy) delete initEnergy;
    if (scaleDelta) delete scaleDelta;

    trialSolution = bestSolution = trialPoint = popEnergy = population = pattern = initPoint = initEnergy = 0;
    return;
}

double PSSolver::EnergyFunction(double *trial)
{
    iterations++;
    bool is_outLimits = false;
    for (int id = 0 ; id < nDim ; id++)
    {
        if (trial[id] < min_[id] || trial[id] > max_[id])
            is_outLimits = true;
    }

    if (is_outLimits)
        m_currentEnergy = 1.0E20;
    else
    {
        m_currentEnergy =  EvalFun::EnergyFunction(trial,m_input_image,m_output_image,m_reference,
                                   m_seg_index,m_metric_index,m_img_rows,m_img_cols,
                                   m_fixed,min_,max_,nDim,m_GlobalInfo.get_bands_string().toStdString());
    }
    return m_currentEnergy;
}

void PSSolver::Setup(double min[],double max[],QString searchOpt,
                     QString input_image, QString output_image,
                     vector<vector<Point2f> > reference,
                     int segIdx, int metIdx, int ref_rows, int ref_cols,
                     double* fixParams, int nDimFixed,
                     int meshIterMax, double meshSizeMin, double error, OptInfo GlobalInfo)
{
    //////////////////////////////////////////////////////
    //Added by Pedro
    //Parameters for the segmentation and metric
    m_input_image = input_image;
    m_output_image = output_image;
    m_reference = reference;
    m_seg_index = segIdx;
    m_metric_index = metIdx;
    m_img_rows = ref_rows;
    m_img_cols = ref_cols;
    m_nDimFixed = nDimFixed;
    m_fixed = new double[nDimFixed];
    firstTrialPoint = new double[nDim];
    m_searchOpt = searchOpt;
    m_meshIterMax = meshIterMax;
    m_meshSizeMin = meshSizeMin;
    m_error = error;
    m_GlobalInfo.copyFrom(GlobalInfo);
    m_times_to_run = m_GlobalInfo.get_times_to_run();

    if(nDimFixed > 1)
    {
        for(int i = 0 ; i < nDimFixed ; i++)
        {
            m_fixed[i] = fixParams[i];
        }
    }
    else
    {
        m_fixed[0] = 0;
    }
    qDebug()<<"Passed parameters";
    //////////////////////////////////////////////////////
    double range = 0;
    for (int i=0; i < nDim; i++)
    {
       min_[i]=min[i];
       max_[i]=max[i];
       range = max_[i] - min[i];
       scaleDelta[i] = range * 0.20;
    }

    for (int i=0; i < 2*nDim; i++)
    {
        for (int j=0; j < nDim; j++)
        {
            Element(population,i,j) = 0;
            Element(pattern,i,j) = 0;
        }        
    }

    for (int i=0; i < 2*nDim; i++)
    {
        for (int j=0; j < nDim; j++)
        {
            if (j==i)
            {
                Element(pattern,i,j) = 1;
                Element(pattern,i,j+nDim*nDim) = -1;
            }
        }
    }

    if (m_searchOpt == "Set_P[0]")
    {
        SearchPo();

    }
    return;
}

void PSSolver::SearchPo()
{
    QFile file("Init_P0.txt");
    m_P0.clear();
    m_bestEnergyP0.clear();

    if(!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::critical(NULL, QString("Error"), QString("Error opening with initial points."));
        qDebug()<<"Error opening with initial points.";
    }

    QTextStream iter(&file);
    while(!iter.atEnd())
    {
        QString line = iter.readLine();
        QStringList fields = line.split(",");
        vector<double> temp;
        for (int p = 0 ; p < (fields.length()-1); p++)
        {
            temp.push_back(fields.at(p).toDouble());
        }
        m_P0.push_back(temp);
        m_bestEnergyP0.push_back(fields.at(fields.length()-1).toDouble());
    }
    file.close();
}

void PSSolver::Solve()
{
    //Run many times the experiment, each time, it will save the results
    for(int times = 0 ; times < m_times_to_run ; times++)
    {
        //Emit and send the ID of current experiment
        emit n_ID(times+1);
        double time_process = 0;
        clock_t start,end;
        start = clock();

        //bool bAtSolution;
        int coordinate;
        double meshSize;
        double centralEnergy;
        int m = 0;
        int break_index = -1;
        bool lets_break=false;

        //Save temporal results
        QString name_energy("Energy_GPS_");
        name_energy.append(QString::number(times));
        name_energy.append(".txt");
        ofstream myfile;
        myfile.open(name_energy.toStdString().c_str());
        myfile << "trialEnergy	bestEnergy	meshSize	trialPoint...\n";
        myfile.close();
        m_current_File = name_energy;

        delta = 1;
        iterations = 0;
        meshSize = delta;
        m_meshSize = delta;

        //Setup values for an experiment
        for (int i=0; i < nDim; i++)
            bestSolution[i] = 1.0E20;

        for (int i=0; i < 2*nDim; i++)
        {
            popEnergy[i] = 1.0E20;
        }
        bestEnergy = 1.0E20;

        //Initialization with or without DE
        if(m_searchOpt == "SearchOff")
            SearchOff(min_,max_);
        else if (m_searchOpt == "SearchUD")
            SearchUD(min_,max_);
        else if (m_searchOpt == "SearchDE")
        {
            //TXT File to save the intermediate results related to DE initialization
            QString DE_name("Energy_GPS_DE_");
            DE_name.append(QString::number(times));
            DE_name.append(".txt");
            SearchDE(min_,max_);
            QFile::copy("Energy_DE_0.txt",DE_name);
            QFile::remove("Energy_DE_0.txt");
        }
        else if (m_searchOpt == "Set_P[0]")
        {
            vector<double> firstPoint;
            firstPoint = m_P0.at(times);

            for (int i = 0; i < nDim; i++)
            {
                 trialPoint[i] = firstPoint.at(i);
                 firstTrialPoint[i] = firstPoint.at(i);
            }
            trialEnergy = EnergyFunction(trialPoint);

            if (trialEnergy < bestEnergy)
            {
                 bestEnergy = trialEnergy;
                 CopyVector(trialSolution,trialPoint);
                 for (int i=0; i<nDim;i++)
                 {
                     bestSolution[i] = trialSolution[i];
                 }
            }
            ofstream myFile;
            myFile.open(m_current_File.toStdString().c_str(),std::ios::app);
            myFile<< trialEnergy << "	" << bestEnergy << "	" << meshSize<< "	";
            for(int j = 0 ; j < nDim ; j++)
            {
                if (j == (nDim-1))
                {   myFile <<  trialPoint[j]  << "\n";}
                else
                {   myFile <<  trialPoint[j]  << "	";}
            }
            myFile.close();
        }

        //Optimization Procedure
        while ( m < m_meshIterMax && m_meshSizeMin < meshSize)
//        while ( iterations < m_meshIterMax)
        {
            emit n_iterations(m);
            centralEnergy = bestEnergy;
            std::vector<std::vector<double>> trialPoints;
            for (int i=0; i < 2*nDim; i++)
            {
                std::vector<double> trialTemp;
                for (int j=0; j<nDim; j++)
                {
                    trialPoint[j]=trialSolution[j]+delta*Element(pattern,i,j)*scaleDelta[j];
//                    if (trialPoint[j]<min_[j])
//                        trialPoint[j]=min_[j];
//                    if (trialPoint[j]>max_[j])
//                        trialPoint[j]=max_[j];
                    trialTemp.push_back(trialPoint[j]);
                }
                trialEnergy = EnergyFunction(trialPoint);
                trialPoints.push_back(trialTemp);
                popEnergy[i] = trialEnergy;
                CopyVector(RowVector(population,i),trialPoint);
            }            
            m_meshSize = delta;

            for (int i = 0; i < 2*nDim; i++)
            {
                if (popEnergy[i] < bestEnergy)
                {
                    bestEnergy = popEnergy[i];
                    coordinate = i;                  
                }
                emit eval_exec(m_current_File,i);
                ofstream myFile;
                myFile.open(m_current_File.toStdString().c_str(),std::ios::app);
                for(int j = 0 ; j < nDim ; j++)
                {
                    if (j == (nDim-1))
                    {   myFile <<  trialPoints[i][j]  << "\n";}
                    else
                    {   myFile <<  trialPoints[i][j]  << "	";}
                }
                myFile.close();
            }            

            qDebug() << "bestEnergy: " << bestEnergy;
            qDebug() << "centralEnergy: " << centralEnergy;

            if ( bestEnergy < centralEnergy)
            {
                CopyVector(trialSolution,RowVector(population,coordinate));
                CopyVector(bestSolution,trialSolution);
                delta = delta*2;
            }
            else
            {
                delta = delta*0.5;
            }

            meshSize = delta;
            m++;

            //Stop if bestEnergy is lower than an error given by the user
            if(bestEnergy <= m_error)
                break;

            //Stop if the variation between trialPoints is minimum
//            for (int i = 0; i < 2*nDim; i++)
//                if (abs((popEnergy[i] - bestEnergy) < 1E-5) && i!=coordinate && abs((popEnergy[i] - bestEnergy) > 0))
//                {
//                    break_index = i;
//                    qDebug() << "Let's break!!! :'(";
//                    lets_break = true;
//                    break;
//                }
//            if (lets_break)
//                break;
        }
//        if(iterations < m_meshIterMax)
//           qDebug() << "Break by meshSize!";

        for(int p = 0 ; p < nDim; p++)
        {
            qDebug()<< "bestSolution[]" << bestSolution[p];
        }
        end = clock();
        time_process = ((double)end - (double)start)/CLOCKS_PER_SEC;        
        qDebug() << "Save Results Begins";
        Save_Results(times,m,time_process);
        qDebug() << "Save Results Ends";
    }
    emit finished();
}

void PSSolver::SearchOff(double *min, double *max)
{
    srand((unsigned)time(0));
    int randD;
    randD = rand()%(1000)+1;

    for (int i=0; i < nDim; i++)
    {
       trialPoint[i] = min[i] + (max[i]-min[i])*randD/1000;
       firstTrialPoint[i] = trialPoint[i];
    }

    trialEnergy = EvalFun::EnergyFunction(trialPoint,m_input_image,m_output_image,m_reference,
                                          m_seg_index,m_metric_index,m_img_rows,m_img_cols,
                                          m_fixed,min_,max_,nDim,m_GlobalInfo.get_bands_string().toStdString());
    if (trialEnergy < bestEnergy)
    {
        bestEnergy = trialEnergy;
        CopyVector(trialSolution,trialPoint);
        for (int i=0; i<nDim;i++)
        {
            bestSolution[i] = trialSolution[i];
        }
    }
    return;
}

void PSSolver::SearchUD(double *min, double *max)
{    
    double **unitPoint = new double*[nDim];
    for(int ii = 0 ; ii < nDim ; ++ii)
    {
        //second dimension
        unitPoint[ii] = new double[3];
    }

    int temp1 = int(pow(3.0,nDim));
    double **allPoint = new double*[temp1];
    for(int jj = 0 ; jj < temp1 ; ++jj)
    {
        allPoint[jj] = new double[nDim];
    }

    for (int i=0; i < nDim; i++)
        for (int j=0; j < 3; j++)
            unitPoint[i][j] = min[i] + (j+1)*(max[i]-min[i])/4;

    int n, g = 1;
    for (int j = 0; j < nDim; j++)
    {
        n = nDim - j;
        for (int i = 1; i < int(pow(3.0,nDim))+1; i++)
        {
            allPoint[i-1][nDim-j-1] = unitPoint[n-1][g-1];
            if ( i % int(pow(3.0,j)) == 0 )
                {
                    g = g + 1;
                    if (g > 3)
                        g = 1;
                }
        }
    }

    for (int i=0; i < int(pow(3.0,nDim)); i++)
    {
        for (int j=0; j<nDim; j++)
            trialPoint[j] = allPoint[i][j];

        trialEnergy = EvalFun::EnergyFunction(trialPoint,m_input_image,m_output_image,m_reference,
                                              m_seg_index,m_metric_index,m_img_rows,m_img_cols,
                                              m_fixed,min_,max_,nDim,m_GlobalInfo.get_bands_string().toStdString());
        if (trialEnergy < bestEnergy)
        {
            bestEnergy = trialEnergy;
            CopyVector(trialSolution,trialPoint);
        }
    }

    for (int j=0; j<nDim; j++)
    {
        firstTrialPoint[j] = trialSolution[j];
    }

    //free allPoint pointer
    for (int jj = 0; jj < temp1; ++jj)
        delete [] allPoint[jj];
    delete [] allPoint;

    //free unitPoint pointer
    for (int ii = 0; ii < nDim; ++ii)
        delete [] unitPoint[ii];
    delete [] unitPoint;

    return;
}

void PSSolver::SearchDE(double *min, double *max)
{    
    double *startPoint = new double[nDim];

    qDebug()<< "DESolver start";
    DESolver *solverSearchDE = new DESolver(nDim,10);

    solverSearchDE->Setup(min,max,stBest1Exp,0.5,0.8,1E-10,
                         m_input_image,m_output_image,
                         m_reference,m_seg_index,m_metric_index,
                         m_img_rows,m_img_cols,m_fixed,m_nDimFixed,4,false,m_GlobalInfo);

    connect(solverSearchDE,SIGNAL(eval_exec(QString)),solverSearchDE,SLOT(Write_Energy(QString)));
    connect(solverSearchDE,SIGNAL(finished()),solverSearchDE,SLOT(deleteLater()));
    qDebug()<< "DESolver setup";
    solverSearchDE->Solve();

    bestEnergy = solverSearchDE->Energy();
    startPoint = solverSearchDE->Solution();

    for (int i=0; i<nDim;i++)
    {
        trialSolution[i]=startPoint[i];
        bestSolution[i] = startPoint[i];
        firstTrialPoint[i] = startPoint[i];
        qDebug()<<"trialSolution: " << trialSolution[i];
    }

    qDebug()<< "DESolver end";
    return;
}

//////////////////////////////////////////////////////////////////////////
//              MADS IMPLEMENTATION                                     //
//////////////////////////////////////////////////////////////////////////

void PSSolver::SetupMADS(double *min,double *max, QString searchOpt,
                QString input_image, QString output_image,
                vector<vector<Point2f> > reference,
                int segIdx, int metIdx, int ref_rows, int ref_cols,
                double* fixParams, int nDimFixed,
                int meshIterMax, double meshSizeMin, double error, OptInfo GlobalInfo)
{
    //////////////////////////////////////////////////////
    //Added by Pedro
    //Parameters for the segmentation and metric
    m_input_image = input_image;
    m_output_image = output_image;
    m_reference = reference;
    m_seg_index = segIdx;
    m_metric_index = metIdx;
    m_img_rows = ref_rows;
    m_img_cols = ref_cols;
    m_nDimFixed = nDimFixed;
    m_fixed = new double[nDimFixed];
    firstTrialPoint = new double[nDim];
    m_searchOpt = searchOpt;
    m_meshIterMax = meshIterMax;
    m_meshSizeMin = meshSizeMin;
    m_error = error;
    m_GlobalInfo.copyFrom(GlobalInfo);
    m_times_to_run = m_GlobalInfo.get_times_to_run();

    if(nDimFixed > 1)
    {
        for(int i = 0 ; i < nDimFixed ; i++)
        {
            m_fixed[i] = fixParams[i];
        }
    }
    else
    {
        m_fixed[0] = 0;
    }
    qDebug()<<"Passed parameters";
    //////////////////////////////////////////////////////
    double range = 0;
    for (int i=0; i < nDim; i++)
    {
       min_[i]=min[i];
       max_[i]=max[i];
       range = max_[i] - min[i];
       scaleDelta[i] = range * 0.20;
    }

    for (int i=0; i < 2*nDim; i++)
    {
        for (int j=0; j < nDim; j++)
        {
            Element(population,i,j) = 0;
            Element(pattern,i,j) = 0;
        }
    }
    if (m_searchOpt == "Set_P[0]")
    {
        SearchPo();
    }


    return;
}

void PSSolver::SolveMADS()
{
    //Run many times the experiment, each time, it will save the results
    for(int times = 0 ; times < m_times_to_run ; times++)
    {
        //Emit and send the ID of current experiment
        emit n_ID(times+1);
        double time_process = 0;
        clock_t start,end;
        start = clock();

        //bool bAtSolution;
        int coordinate;
        double meshSize;
        double centralEnergy;
        int m = 0;
        int break_index = -1;

        QString name_energy("Energy_MADS_");
        name_energy.append(QString::number(times));
        name_energy.append(".txt");
        ofstream myfile;
        myfile.open(name_energy.toStdString().c_str());
        myfile << "trialEnergy	bestEnergy	meshSize	trialPoint...\n";
        myfile.close();
        m_current_File = name_energy;

        //Setup values for an experiment
        bestEnergy = 1.0E20;
        deltaM = 1;
        deltaP = 1;
        m_meshSize = deltaM;
        meshSize = deltaM;
        iterations = 0;

        bool lets_break = false;

        for (int i=0; i < 2*nDim; i++)
        {
            popEnergy[i] = 1.0E20;
        }

        for (int i=0; i < nDim; i++)
            bestSolution[i] = 1.0E20;

        //Initialization with or without DE
        if(m_searchOpt == "SearchOff")
            SearchOff(min_,max_);
        else if (m_searchOpt == "SearchUD")
            SearchUD(min_,max_);
        else if (m_searchOpt == "SearchDE")
        {
            //TXT File to save the intermediate results related to DE initialization
            QString DE_name("Energy_MADS_DE_");
            DE_name.append(QString::number(times));
            DE_name.append(".txt");
            SearchDE(min_,max_);
            QFile::copy("Energy_DE_0.txt",DE_name);
            QFile::remove("Energy_DE_0.txt");
        }
        else if (m_searchOpt == "Set_P[0]")
        {
            vector<double> firstPoint;
            firstPoint = m_P0.at(times);

            for (int i = 0; i < nDim; i++)
            {
                 trialPoint[i] = firstPoint.at(i);
                 firstTrialPoint[i] = firstPoint.at(i);
            }
            trialEnergy = EnergyFunction(trialPoint);

            if (trialEnergy < bestEnergy)
            {
                 bestEnergy = trialEnergy;
                 CopyVector(trialSolution,trialPoint);
                 for (int i=0; i<nDim;i++)
                 {
                     bestSolution[i] = trialSolution[i];
                 }
            }
            ofstream myFile;
            myFile.open(m_current_File.toStdString().c_str(),std::ios::app);
            myFile<< trialEnergy << "	" << bestEnergy << "	" << meshSize << "	";
            for(int j = 0 ; j < nDim ; j++)
            {
                if (j == (nDim-1))
                {   myFile <<  trialPoint[j]  << "\n";}
                else
                {   myFile <<  trialPoint[j]  << "	";}
            }
            myFile.close();
        }

        //Optimization Procedure
        while ( m < m_meshIterMax && m_meshSizeMin < meshSize)
//        while ( iterations < m_meshIterMax)
        {
            emit n_iterations(m);
            BasisFrame();
            centralEnergy = bestEnergy;
            std::vector<std::vector<double>> trialPoints;
            for (int i=0; i < 2*nDim; i++)
            {
                std::vector<double> trialTemp;
                for (int j=0; j<nDim; j++)
                {
                    trialPoint[j]=trialSolution[j]+deltaM*Element(pattern,i,j)*scaleDelta[j];
//                    if (trialPoint[j]<min_[j])
//                        trialPoint[j]=min_[j];
//                    if (trialPoint[j]>max_[j])
//                        trialPoint[j]=max_[j];
                    trialTemp.push_back(trialPoint[j]);
                }
                trialEnergy = EnergyFunction(trialPoint);
                trialPoints.push_back(trialTemp);
                popEnergy[i] = trialEnergy;
                CopyVector(RowVector(population,i),trialPoint);
            }

            m_meshSize = deltaM;

            for (int i = 0; i < 2*nDim; i++)
            {
                if (popEnergy[i] < bestEnergy)
                {
                    bestEnergy = popEnergy[i];
                    coordinate = i;
                }
                emit eval_exec(m_current_File,i);
                ofstream myFile;
                myFile.open(m_current_File.toStdString().c_str(),std::ios::app);
                for(int j = 0 ; j < nDim ; j++)
                {
                    if (j == (nDim-1))
                    {   myFile <<  trialPoints[i][j]  << "\n";}
                    else
                    {   myFile <<  trialPoints[i][j]  << "	";}
                }
                myFile.close();
            }
            if ( bestEnergy < centralEnergy)
            {
                CopyVector(trialSolution,RowVector(population,coordinate));
                CopyVector(bestSolution,trialSolution);
                if (deltaM < 0.25) deltaM = deltaM*4;
            }
            else
            {
                deltaM = deltaM/4;
            }
            meshSize = deltaM;
            m++;           

            //Stop if bestEnergy is lower than an error given by the user
            if(bestEnergy <= m_error)
                break;

            //Stop if the variation between trialPoints is minimum
//            for (int i = 0; i < 2*nDim; i++)
//                if (abs((popEnergy[i] - bestEnergy) < 1E-5) && i!=coordinate && abs((popEnergy[i] - bestEnergy) > 0))
//                {
//                    break_index = i;
//                    qDebug() << "Let's break!!! :'(";
//                    lets_break = true;
//                    break;
//                }
//            if (lets_break)
//                break;
        }
//        if(iterations < m_meshIterMax)
//           qDebug() << "Break by meshSize!";

        end = clock();
        time_process = ((double)end - (double)start)/CLOCKS_PER_SEC;
        Save_Results(times,m,time_process);        
    }
    emit finished();
}

void PSSolver::BasisFrame(void)
{
    srand((unsigned)time(0));    
    double *B = new double[nDim*nDim];

    int randD;
    int deltaMo;
    deltaMo = 1/sqrt(deltaM);
    for (int i=0; i < nDim; i++)
        for (int j=0; j < nDim; j++)
        {
            if (j<i)
            {
                randD = -deltaMo + rand()%(2*deltaMo-1)+1;
                Element(B,i,j) = 1;
            }
            else if (j==i)
            {
                randD = pow(-1.0,rand()%2)*deltaMo;
                Element(B,i,j) = randD;
            }
            else Element(B,i,j) = 0;
        }

    //bool state[nDim*nDim];
    bool *state = new bool[nDim*nDim];

    for (int i=0; i < nDim*nDim; i++)
        state[i]=false;

    int index=0;
    for (int i=0; i < nDim*nDim; i++)
    {
        do{
            index = rand()%(nDim*nDim);
        }while (state[index]);
        pattern[i] = B[index];
        pattern[i+nDim*nDim] = (-1)*B[index];
        state[index]=true;
    }

    deltaP = sqrt(deltaM);
    delete[] B;
    delete[] state;
}

void PSSolver::Save_Results(int ID,int m,double time)
{
    double stop = EnergyFunction(bestSolution);//bestEnergy;
    string final_result_name, final_shapefiles;

    qDebug() << QString("Fitness: %1  Iterations: %2").arg(stop).arg(iterations);

    //Only for Region Growing (BAATZ), input and output images have to be BMPs
    if(m_seg_index == 0)
    {
        final_result_name = QString("./Results/").toStdString() + m_GlobalInfo.get_opt_outLineEdit().toStdString() + ".png";
    }
    else if(m_seg_index == 4)//Only for MultiSeg
    {
        final_result_name = QString("./Results/").toStdString() + m_GlobalInfo.get_opt_outLineEdit().toStdString() + ".jpg";
    }
    else
    {
        final_result_name = QString("./Results/").toStdString() + m_GlobalInfo.get_opt_outLineEdit().toStdString() + ".png";
    }

    final_shapefiles = QString("./Results/OutputVector").toStdString();

    //Copying result files from ./temp to ./Results
    QFile::copy(QString(m_GlobalInfo.get_out_name().c_str()),QString(final_result_name.c_str()));
    QFile::copy(QString("./temp/result3.shp"),QString::fromStdString(final_shapefiles+".shp"));
    QFile::copy(QString("./temp/result3.shx"),QString::fromStdString(final_shapefiles+".shx"));
    QFile::copy(QString("./temp/result3.dbf"),QString::fromStdString(final_shapefiles+".dbf"));

    float result_testing = evaluate_metric(readShapeFile(final_shapefiles),
                                           readShapeFile(m_GlobalInfo.get_refImTestFileName().toStdString()),
                                           m_metric_index,
                                           m_img_rows,m_img_cols);

    QFile data(QString("./Results/report")+QString::number(ID)+(".txt"));
    data.open(QFile::WriteOnly | QFile::Truncate);
    QTextStream output(&data);
    output << "              Segmentation Parameter Tuner v." << m_GlobalInfo.get_version() << "\n";
    output << "**************************************************************\n\n";
    output << "General Information: \n";
    output << " - Base Image:   " << m_GlobalInfo.get_initialImgFileName() << "\n";
    output << " - Reference Training:    " << m_GlobalInfo.get_initialRefFileName() << "\n";
    output << " - Reference Testing:    " << m_GlobalInfo.get_refImTestFileName()<< "\n";
    output << " - Image Dim.:   " << QString::number(m_GlobalInfo.get_rows_ref()) + " x " +
                                     QString::number(m_GlobalInfo.get_cols_ref()) + " x " +
                                     QString::number(m_GlobalInfo.get_bands_ref()) << "\n\n\n";
    output << "General Configurations: \n";
    output << " - Optimization Algorithm:   " << m_GlobalInfo.get_optAlgComboBox_text() << "\n";
    output << " - Segmentation Algorithm:   " << m_GlobalInfo.get_segAlgComboBox_text() << "\n";
    output << " - Fitness Function:         " << m_GlobalInfo.get_fitComboBox_text() << "\n\n\n";
    output << "Output Results: \n";
    output << " - Output Image:     " << QString(final_result_name.c_str()) << "\n";
    output << " - Vector Data:      " << QString::fromStdString(final_shapefiles+".shp") << "\n";
    output << " - Fitness Value (Training):    " << stop << "\n";
    output << " - Fitness Value (Testing):    " << result_testing << "\n";
    output << " - Num. Execution of Target Function:  " << iterations << "\n";
    output << " - Num. Iterations:  " << m << "\n";
    output << " - Processing Time:  " << time << "\n";
    output << " - ID :" << ID << "\n";

    output << " - First Trial Point:" << "\n";
    for(int p = 0 ; p < nDim; p++)
    {
        output<<"              " << firstTrialPoint[p] << "\n";
        qDebug()<< "bestSolution[]" << bestSolution[p];
    }

    output << " - Parameters tuned: \n";

    //Label 1 ///////////////////////////////////////////////////////////////////////////////////////////
    if (m_seg_index == 3)//only for Graph-based, it is double
    {
        if (!m_GlobalInfo.get_opt_segLabel1_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel1_text() << ":   " << (double)bestSolution[0] << "\n";
    }
    else//for the others, it is INT
    {
        if (!m_GlobalInfo.get_opt_segLabel1_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel1_text() << ":   " << (int)bestSolution[0] << "\n";
    }

    //Label 2 ///////////////////////////////////////////////////////////////////////////////////////////
    if((m_seg_index == 0) || (m_seg_index == 3))//only for Baatz and Graph-based
    {
        if (!m_GlobalInfo.get_opt_segLabel2_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel2_text() << ":   " << (double)bestSolution[1] << "\n";

    }
    else if ((m_seg_index == 1) || (m_seg_index == 2))//for Meanshift and SPRING, it is INT
    {
        if (!m_GlobalInfo.get_opt_segLabel2_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel2_text() << ":   " << (int)bestSolution[1] << "\n";
    }
    else if(m_seg_index == 4)
    {
        if (!m_GlobalInfo.get_opt_segLabel2_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel2_text() << ":   " << ((int)((double)bestSolution[1]/0.2))*0.2 << "\n";
    }

    //Label 3 ///////////////////////////////////////////////////////////////////////////////////////////
    if(m_seg_index == 0)
    {
        if (!m_GlobalInfo.get_opt_segLabel3_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel3_text() << ":   " << (double)bestSolution[2] << "\n";
    }
    else if((m_seg_index == 1)||(m_seg_index == 2)||(m_seg_index == 3))
    {
        if (!m_GlobalInfo.get_opt_segLabel3_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel3_text() << ":   " << (int)bestSolution[2] << "\n";
    }
    else if(m_seg_index == 4)//Just for Multiseg
    {
        int conf = (int)bestSolution[2];
        double conf_real;
        switch (conf)
        {
        case 1:
            conf_real = 0.80;break;
        case 2:
            conf_real = 0.85;break;
        case 3:
            conf_real = 0.90;break;
        case 4:
            conf_real = 0.95;break;
        case 5:
            conf_real = 0.99;break;
        case 6:
            conf_real = 0.995;break;
        case 7:
            conf_real = 0.999;break;
        default:
            break;
        }
        if (!m_GlobalInfo.get_opt_segLabel3_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel3_text() << ":   " << QString::number(conf_real) << "\n";
    }

   //Label 4 /////=//////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel4_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel4_text() << ":   " << (int)bestSolution[3] << "\n";

   //Label 5 ///////////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel5_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel5_text() << ":   " << (int)bestSolution[4] << "\n";

   //Label 6 ///////////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel6_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel6_text() << ":   " << m_GlobalInfo.get_opt_segAlgComboBox2_text() << "\n";

   //Label 7 ///////////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel7_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel7_text() << ":   " << m_GlobalInfo.get_opt_segAlgComboBox3_text() << "\n";

   //Label 8 ///////////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel8_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel8_text() << ":   " << m_GlobalInfo.get_opt_segAlgComboBox4_text() << "\n";
   //Label 5 Coeff of Variation/////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel5_Coeff_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel5_Coeff_text() << ":   " << bestSolution[4] << "\n";
    data.close();
}

void PSSolver::Write_Energy(QString myFileName, int i)
{
    ofstream myFile;
    myFile.open(myFileName.toStdString().c_str(),std::ios::app);
    myFile <<  popEnergy[i]  << "	" << bestEnergy << "	" << m_meshSize << "	";
    myFile.close();
}
