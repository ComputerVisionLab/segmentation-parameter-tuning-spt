/********************************************************************************
** Form generated from reading UI file 'loading.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOADING_H
#define UI_LOADING_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QProgressBar>

QT_BEGIN_NAMESPACE

class Ui_Loading
{
public:
    QFormLayout *formLayout;
    QProgressBar *load_progressBar;

    void setupUi(QDialog *Loading)
    {
        if (Loading->objectName().isEmpty())
            Loading->setObjectName(QStringLiteral("Loading"));
        Loading->resize(200, 49);
        Loading->setModal(true);
        formLayout = new QFormLayout(Loading);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        load_progressBar = new QProgressBar(Loading);
        load_progressBar->setObjectName(QStringLiteral("load_progressBar"));
        load_progressBar->setValue(24);

        formLayout->setWidget(0, QFormLayout::SpanningRole, load_progressBar);


        retranslateUi(Loading);

        QMetaObject::connectSlotsByName(Loading);
    } // setupUi

    void retranslateUi(QDialog *Loading)
    {
        Loading->setWindowTitle(QApplication::translate("Loading", "Running...", 0));
    } // retranslateUi

};

namespace Ui {
    class Loading: public Ui_Loading {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOADING_H
