// Differential Evolution Solver Class
// Based on algorithms developed by Dr. Rainer Storn & Kenneth Price
// Written By: Lester E. Godwin
//             PushCorp, Inc.
//             Dallas, Texas
//             972-840-0208 x102
//             godwin@pushcorp.com
// Created: 6/8/98
// Last Modified: 6/8/98
// Revision: 1.0

#if !defined(_DESOLVER_H)
#define _DESOLVER_H
#include <memory.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include "ShpFunct.h"
#include "EvalFun.h"
#include <QObject>
#include "optinfo.h"

#define stBest1Exp			0
#define stRand1Exp			1
#define stRandToBest1Exp	2
#define stBest2Exp			3
#define stRand2Exp			4
#define stBest1Bin			5
#define stRand1Bin			6
#define stRandToBest1Bin	7
#define stBest2Bin			8
#define stRand2Bin			9

class DESolver;

typedef void (DESolver::*StrategyFunction)(int);

class DESolver : public QObject
{
    Q_OBJECT
public:
    DESolver(int dim,int popSize);
    ~DESolver(void);

    // Setup() must be called before solve to set min, max, strategy etc.
    void Setup(double min[], double max[], int deStrategy,
               double diffScale, double crossoverProb, double conv,
               QString input_image, QString output_image,
               vector<vector<Point2f> > reference,
               int segIdx, int metIdx, int ref_rows, int ref_cols,
               double* fixParams, int nDimFixed, int maxGenerations, bool mainOpt, OptInfo GlobalInfo);

    double EnergyFunction(double trial[]);
public slots:
    virtual void Solve();
    void Write_Energy(QString myFileName);
signals:
    void n_iterations(int n);
    void n_ID(int temp_ID);
    void n_Executions(int temp_exec);
    void finished();
    void eval_exec(QString current_file);
public:
    bool SolveHard(double* min,double* max,double* step);

    // EnergyFunction must be overridden for problem to solve
    // testSolution[] is nDim array for a candidate solution
    // setting bAtSolution = true indicates solution is found
    // and Solve() immediately returns true.

    // Call these functions after Solve() to get results.
    int Dimension(void) { return(nDim); }
    int Population(void) { return(nPop); }
    double Energy(void) { return(bestEnergy); }
    double *Solution(void) { return(bestSolution); }
    int Iterations(void) { return(iterations); }
    void Save_Results(int ID, int m , double time);

protected:
    void SelectSamples(int candidate,int *r1,int *r2=0,int *r3=0,
                                                int *r4=0,int *r5=0);
    double RandomUniform(double min,double max);

    int nDim;
    int nPop;
    int iterations;

    int strategy;
        StrategyFunction calcTrialSolution;
    double scale;
    double probability;

    double trialEnergy;
    double bestEnergy;
    double bestPopEnergy;
    double convergence;

    double *trialSolution;
    double *min_;
    double *max_;
    double *bestSolution;
    double *popEnergy;
    double *population;
    int m_maxGenerations;

    //Variables for Segmentation
    QString m_input_image;
    QString m_output_image;
    vector<vector<Point2f> >  m_reference;
    int m_seg_index;
    int m_metric_index;
    int m_img_rows;
    int m_img_cols;
    double* m_fixed;
    int m_nDimFixed;
    OptInfo m_GlobalInfo;
    int m_times_to_run;
    bool m_mainOpt;
    double m_currentEnergy;
    QString m_current_File;

private:
    void Best1Exp(int candidate);
    void Rand1Exp(int candidate);
    void RandToBest1Exp(int candidate);
    void Best2Exp(int candidate);
    void Rand2Exp(int candidate);
    void Best1Bin(int candidate);
    void Rand1Bin(int candidate);
    void RandToBest1Bin(int candidate);
    void Best2Bin(int candidate);
    void Rand2Bin(int candidate);
};

#endif // _DESOLVER_H
