/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_16;
    QTabWidget *tabWidget;
    QWidget *referenceTab;
    QGridLayout *gridLayout_2;
    QGroupBox *ref_rgbGroupBox;
    QVBoxLayout *verticalLayout_2;
    QLabel *ref_rgbRLabel;
    QComboBox *ref_rgbRComboBox;
    QLabel *ref_rgbGLabel;
    QComboBox *ref_rgbGComboBox;
    QLabel *ref_rgbBLabel;
    QComboBox *ref_rgbBComboBox;
    QPushButton *ref_composeButton;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout;
    QPushButton *ref_zoomButton;
    QPushButton *ref_panButton;
    QPushButton *ref_createButton;
    QPushButton *ref_editButton;
    QPushButton *ref_deleteButton;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_3;
    QPushButton *ref_clearRefButton;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *ref_saveRefButton;
    QPushButton *ref_loadImButton;
    QPushButton *ref_loadRefButton;
    QLabel *ref_InfoImgLabel;
    QPushButton *ref_loadRefTestButton;
    QHBoxLayout *layout_canvas;
    QSpacerItem *verticalSpacer;
    QGroupBox *groupBox_8;
    QTableWidget *ref_SegmentsTable;
    QPushButton *ref_SelectAllButton;
    QPushButton *ref_ClearAllButton;
    QCheckBox *ref_ShowNumbersCheckBox;
    QWidget *optimizationTab;
    QGridLayout *gridLayout_6;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_17;
    QGroupBox *opt_segAlgGroupBox;
    QGridLayout *gridLayout_5;
    QLineEdit *opt_segLineEditMax4;
    QLineEdit *opt_segLineEditMin4;
    QLabel *opt_segLabel8;
    QLabel *opt_segLabel1;
    QLabel *opt_segLabel5;
    QLabel *opt_segLabel3;
    QLabel *opt_label1_3;
    QLineEdit *opt_segLineEditMin3;
    QLabel *opt_segLabel6;
    QLabel *opt_segLabel7;
    QComboBox *opt_segAlgComboBox3;
    QLabel *opt_segLabel4;
    QComboBox *opt_segAlgComboBox4;
    QLineEdit *opt_segLineEditMax1;
    QLabel *opt_label1_4;
    QLineEdit *opt_segLineEditMin2;
    QComboBox *opt_segAlgComboBox;
    QLineEdit *opt_segLineEditMax3;
    QLineEdit *opt_segLineEditMax2;
    QLabel *opt_segLabel2;
    QComboBox *opt_segAlgComboBox2;
    QLineEdit *opt_segLineEditMin1;
    QLabel *opt_segLabelBands;
    QLineEdit *opt_segLineEditBands;
    QLabel *opt_segLabel5_Coeff;
    QLineEdit *opt_segLineEditMin5;
    QLineEdit *opt_segLineEditMax5;
    QLineEdit *opt_segLineEditMin6;
    QLineEdit *opt_segLineEditMax6;
    QGroupBox *opt_fitGroupBox;
    QGridLayout *gridLayout_8;
    QLabel *opt_segLabel9;
    QLineEdit *opt_segLineEditTimesToRun;
    QComboBox *opt_fitComboBox;
    QGroupBox *opt_outGroupBox;
    QGridLayout *gridLayout_7;
    QLineEdit *opt_outLineEdit;
    QPushButton *opt_outPushButton;
    QLabel *opt_outLabel;
    QLineEdit *opt_segLineEdit5;
    QPushButton *opt_runPushButton;
    QLabel *opt_segLabelID;
    QLabel *opt_segLabelIteration;
    QProgressBar *opt_ProgressBar;
    QGroupBox *opt_optAlgGroupBox;
    QGridLayout *gridLayout_4;
    QComboBox *opt_optAlgComboBox;
    QLineEdit *opt_optLineEdit2;
    QLabel *opt_optLabel2;
    QLineEdit *opt_optLineEdit1;
    QLineEdit *opt_optLineEdit4;
    QLabel *opt_optLabel4;
    QLabel *opt_optLabel1;
    QLabel *opt_optLabel5;
    QLineEdit *opt_optLineEdit3;
    QLabel *opt_optLabel3;
    QComboBox *opt_optAdvComboBox;
    QHBoxLayout *layout_canvas_opt;
    QWidget *segmentationTab;
    QGridLayout *gridLayout_15;
    QSpacerItem *verticalSpacer_5;
    QHBoxLayout *layout_canvas_seg;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_10;
    QComboBox *seg_metricsComboBox;
    QPushButton *seg_refCalculatePushButton;
    QLineEdit *seg_metricLineEdit;
    QLabel *label_2;
    QPushButton *seg_refLoadPushButton;
    QPushButton *seg_clearRefButton;
    QGroupBox *seg_segAlgGroupBox;
    QGridLayout *gridLayout_11;
    QLabel *seg_segLabel2;
    QLineEdit *seg_segLineEdit3;
    QPushButton *seg_LoadParamsFilePushButton;
    QComboBox *seg_segAlgComboBox3;
    QComboBox *seg_segAlgComboBox2;
    QComboBox *seg_segAlgComboBox4;
    QLabel *seg_segLabel6;
    QLabel *seg_segLabel8;
    QLabel *seg_segLabel7;
    QComboBox *seg_segAlgComboBox;
    QLineEdit *seg_segLineEdit5;
    QLabel *seg_segLabel5;
    QLineEdit *seg_segLineEdit1;
    QLineEdit *seg_segLineEdit4;
    QLabel *seg_segLabel4;
    QLabel *seg_segLabel1;
    QLineEdit *seg_segLineEdit2;
    QLabel *seg_segLabel3;
    QLabel *seg_segLabelBands;
    QLineEdit *seg_segLineEditBands;
    QLabel *seg_segLabel9;
    QLineEdit *seg_segLineEdit9;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_9;
    QPushButton *seg_runPushButton;
    QPushButton *seg_outPushButton;
    QLabel *label;
    QLineEdit *seg_outLineEdit;
    QWidget *assessmentTab;
    QGridLayout *gridLayout;
    QHBoxLayout *layout_canvas_assess;
    QSpacerItem *verticalSpacer_4;
    QGroupBox *groupBox_6;
    QGridLayout *gridLayout_14;
    QComboBox *assess_metricsComboBox;
    QPushButton *assess_Calculate;
    QGroupBox *assess_rgbGroupBox;
    QGridLayout *gridLayout_12;
    QLabel *label_4;
    QComboBox *assess_rgbRComboBox;
    QLabel *label_5;
    QComboBox *assess_rgbGComboBox;
    QLabel *label_6;
    QComboBox *assess_rgbBComboBox;
    QPushButton *assess_composeButton;
    QGroupBox *groupBox_5;
    QGridLayout *gridLayout_13;
    QPushButton *assess_ClearRef;
    QPushButton *assess_LoadRef;
    QPushButton *assess_LoadImButton;
    QPushButton *assess_LoadSegOut;
    QGroupBox *groupBox_7;
    QLineEdit *assess_metricLineEdit;
    QSpacerItem *horizontalSpacer;
    QLabel *assess_InfoImgLabel;
    QWidget *tab;
    QLabel *about_LogoLabel;
    QTextEdit *about_TextInfoLabel;
    QLabel *about_LogoPUCLabel;
    QLabel *label_3;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(894, 843);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(8);
        MainWindow->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/icons/spt.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy);
        gridLayout_16 = new QGridLayout(centralWidget);
        gridLayout_16->setSpacing(6);
        gridLayout_16->setContentsMargins(11, 11, 11, 11);
        gridLayout_16->setObjectName(QStringLiteral("gridLayout_16"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        sizePolicy.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy);
        QFont font1;
        font1.setFamily(QStringLiteral("MS Shell Dlg 2"));
        font1.setPointSize(8);
        tabWidget->setFont(font1);
        referenceTab = new QWidget();
        referenceTab->setObjectName(QStringLiteral("referenceTab"));
        gridLayout_2 = new QGridLayout(referenceTab);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        ref_rgbGroupBox = new QGroupBox(referenceTab);
        ref_rgbGroupBox->setObjectName(QStringLiteral("ref_rgbGroupBox"));
        verticalLayout_2 = new QVBoxLayout(ref_rgbGroupBox);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        ref_rgbRLabel = new QLabel(ref_rgbGroupBox);
        ref_rgbRLabel->setObjectName(QStringLiteral("ref_rgbRLabel"));

        verticalLayout_2->addWidget(ref_rgbRLabel);

        ref_rgbRComboBox = new QComboBox(ref_rgbGroupBox);
        ref_rgbRComboBox->setObjectName(QStringLiteral("ref_rgbRComboBox"));

        verticalLayout_2->addWidget(ref_rgbRComboBox);

        ref_rgbGLabel = new QLabel(ref_rgbGroupBox);
        ref_rgbGLabel->setObjectName(QStringLiteral("ref_rgbGLabel"));

        verticalLayout_2->addWidget(ref_rgbGLabel);

        ref_rgbGComboBox = new QComboBox(ref_rgbGroupBox);
        ref_rgbGComboBox->setObjectName(QStringLiteral("ref_rgbGComboBox"));

        verticalLayout_2->addWidget(ref_rgbGComboBox);

        ref_rgbBLabel = new QLabel(ref_rgbGroupBox);
        ref_rgbBLabel->setObjectName(QStringLiteral("ref_rgbBLabel"));

        verticalLayout_2->addWidget(ref_rgbBLabel);

        ref_rgbBComboBox = new QComboBox(ref_rgbGroupBox);
        ref_rgbBComboBox->setObjectName(QStringLiteral("ref_rgbBComboBox"));

        verticalLayout_2->addWidget(ref_rgbBComboBox);

        ref_composeButton = new QPushButton(ref_rgbGroupBox);
        ref_composeButton->setObjectName(QStringLiteral("ref_composeButton"));

        verticalLayout_2->addWidget(ref_composeButton);


        gridLayout_2->addWidget(ref_rgbGroupBox, 2, 0, 1, 1);

        groupBox_2 = new QGroupBox(referenceTab);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy1);
        groupBox_2->setAlignment(Qt::AlignCenter);
        verticalLayout = new QVBoxLayout(groupBox_2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout->setContentsMargins(2, -1, 2, -1);
        ref_zoomButton = new QPushButton(groupBox_2);
        ref_zoomButton->setObjectName(QStringLiteral("ref_zoomButton"));
        sizePolicy1.setHeightForWidth(ref_zoomButton->sizePolicy().hasHeightForWidth());
        ref_zoomButton->setSizePolicy(sizePolicy1);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/icons/zoom.png"), QSize(), QIcon::Normal, QIcon::Off);
        ref_zoomButton->setIcon(icon1);

        verticalLayout->addWidget(ref_zoomButton);

        ref_panButton = new QPushButton(groupBox_2);
        ref_panButton->setObjectName(QStringLiteral("ref_panButton"));
        sizePolicy1.setHeightForWidth(ref_panButton->sizePolicy().hasHeightForWidth());
        ref_panButton->setSizePolicy(sizePolicy1);
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icons/icons/pan.png"), QSize(), QIcon::Normal, QIcon::Off);
        ref_panButton->setIcon(icon2);

        verticalLayout->addWidget(ref_panButton);

        ref_createButton = new QPushButton(groupBox_2);
        ref_createButton->setObjectName(QStringLiteral("ref_createButton"));
        sizePolicy1.setHeightForWidth(ref_createButton->sizePolicy().hasHeightForWidth());
        ref_createButton->setSizePolicy(sizePolicy1);
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icons/icons/create.png"), QSize(), QIcon::Normal, QIcon::Off);
        ref_createButton->setIcon(icon3);

        verticalLayout->addWidget(ref_createButton);

        ref_editButton = new QPushButton(groupBox_2);
        ref_editButton->setObjectName(QStringLiteral("ref_editButton"));
        sizePolicy1.setHeightForWidth(ref_editButton->sizePolicy().hasHeightForWidth());
        ref_editButton->setSizePolicy(sizePolicy1);
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icons/icons/edit.png"), QSize(), QIcon::Normal, QIcon::Off);
        ref_editButton->setIcon(icon4);

        verticalLayout->addWidget(ref_editButton);

        ref_deleteButton = new QPushButton(groupBox_2);
        ref_deleteButton->setObjectName(QStringLiteral("ref_deleteButton"));
        sizePolicy1.setHeightForWidth(ref_deleteButton->sizePolicy().hasHeightForWidth());
        ref_deleteButton->setSizePolicy(sizePolicy1);
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/icons/icons/delete.png"), QSize(), QIcon::Normal, QIcon::Off);
        ref_deleteButton->setIcon(icon5);

        verticalLayout->addWidget(ref_deleteButton);


        gridLayout_2->addWidget(groupBox_2, 1, 0, 1, 1);

        groupBox = new QGroupBox(referenceTab);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout_3 = new QGridLayout(groupBox);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setContentsMargins(10, 2, 10, 2);
        ref_clearRefButton = new QPushButton(groupBox);
        ref_clearRefButton->setObjectName(QStringLiteral("ref_clearRefButton"));
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/icons/icons/clear.png"), QSize(), QIcon::Normal, QIcon::Off);
        ref_clearRefButton->setIcon(icon6);

        gridLayout_3->addWidget(ref_clearRefButton, 0, 4, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(135, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_2, 0, 6, 1, 1);

        ref_saveRefButton = new QPushButton(groupBox);
        ref_saveRefButton->setObjectName(QStringLiteral("ref_saveRefButton"));
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/icons/icons/save.png"), QSize(), QIcon::Normal, QIcon::Off);
        ref_saveRefButton->setIcon(icon7);

        gridLayout_3->addWidget(ref_saveRefButton, 0, 3, 1, 1);

        ref_loadImButton = new QPushButton(groupBox);
        ref_loadImButton->setObjectName(QStringLiteral("ref_loadImButton"));
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/icons/icons/load.png"), QSize(), QIcon::Normal, QIcon::Off);
        ref_loadImButton->setIcon(icon8);

        gridLayout_3->addWidget(ref_loadImButton, 0, 0, 1, 1);

        ref_loadRefButton = new QPushButton(groupBox);
        ref_loadRefButton->setObjectName(QStringLiteral("ref_loadRefButton"));
        ref_loadRefButton->setIcon(icon8);

        gridLayout_3->addWidget(ref_loadRefButton, 0, 1, 1, 1);

        ref_InfoImgLabel = new QLabel(groupBox);
        ref_InfoImgLabel->setObjectName(QStringLiteral("ref_InfoImgLabel"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(ref_InfoImgLabel->sizePolicy().hasHeightForWidth());
        ref_InfoImgLabel->setSizePolicy(sizePolicy2);
        ref_InfoImgLabel->setMinimumSize(QSize(120, 0));

        gridLayout_3->addWidget(ref_InfoImgLabel, 0, 5, 1, 1);

        ref_loadRefTestButton = new QPushButton(groupBox);
        ref_loadRefTestButton->setObjectName(QStringLiteral("ref_loadRefTestButton"));
        ref_loadRefTestButton->setIcon(icon8);

        gridLayout_3->addWidget(ref_loadRefTestButton, 0, 2, 1, 1);


        gridLayout_2->addWidget(groupBox, 0, 0, 1, 3);

        layout_canvas = new QHBoxLayout();
        layout_canvas->setSpacing(6);
        layout_canvas->setObjectName(QStringLiteral("layout_canvas"));
        layout_canvas->setSizeConstraint(QLayout::SetNoConstraint);

        gridLayout_2->addLayout(layout_canvas, 1, 1, 5, 2);

        verticalSpacer = new QSpacerItem(20, 122, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 4, 0, 1, 1);

        groupBox_8 = new QGroupBox(referenceTab);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        sizePolicy2.setHeightForWidth(groupBox_8->sizePolicy().hasHeightForWidth());
        groupBox_8->setSizePolicy(sizePolicy2);
        groupBox_8->setMinimumSize(QSize(95, 210));
        ref_SegmentsTable = new QTableWidget(groupBox_8);
        if (ref_SegmentsTable->columnCount() < 2)
            ref_SegmentsTable->setColumnCount(2);
        ref_SegmentsTable->setObjectName(QStringLiteral("ref_SegmentsTable"));
        ref_SegmentsTable->setGeometry(QRect(10, 20, 75, 160));
        ref_SegmentsTable->setRowCount(0);
        ref_SegmentsTable->setColumnCount(2);
        ref_SegmentsTable->horizontalHeader()->setVisible(false);
        ref_SegmentsTable->horizontalHeader()->setDefaultSectionSize(35);
        ref_SegmentsTable->horizontalHeader()->setMinimumSectionSize(27);
        ref_SegmentsTable->verticalHeader()->setVisible(false);
        ref_SegmentsTable->verticalHeader()->setDefaultSectionSize(20);
        ref_SelectAllButton = new QPushButton(groupBox_8);
        ref_SelectAllButton->setObjectName(QStringLiteral("ref_SelectAllButton"));
        ref_SelectAllButton->setGeometry(QRect(10, 180, 40, 23));
        ref_ClearAllButton = new QPushButton(groupBox_8);
        ref_ClearAllButton->setObjectName(QStringLiteral("ref_ClearAllButton"));
        ref_ClearAllButton->setGeometry(QRect(50, 180, 40, 23));
        ref_ShowNumbersCheckBox = new QCheckBox(groupBox_8);
        ref_ShowNumbersCheckBox->setObjectName(QStringLiteral("ref_ShowNumbersCheckBox"));
        ref_ShowNumbersCheckBox->setGeometry(QRect(60, 0, 21, 17));

        gridLayout_2->addWidget(groupBox_8, 3, 0, 1, 1);

        tabWidget->addTab(referenceTab, QString());
        optimizationTab = new QWidget();
        optimizationTab->setObjectName(QStringLiteral("optimizationTab"));
        optimizationTab->setMinimumSize(QSize(0, 0));
        gridLayout_6 = new QGridLayout(optimizationTab);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        gridLayout_6->setContentsMargins(-1, 9, -1, -1);
        scrollArea = new QScrollArea(optimizationTab);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        sizePolicy2.setHeightForWidth(scrollArea->sizePolicy().hasHeightForWidth());
        scrollArea->setSizePolicy(sizePolicy2);
        scrollArea->setMinimumSize(QSize(210, 0));
        scrollArea->setFrameShape(QFrame::NoFrame);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 228, 933));
        gridLayout_17 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_17->setSpacing(6);
        gridLayout_17->setContentsMargins(11, 11, 11, 11);
        gridLayout_17->setObjectName(QStringLiteral("gridLayout_17"));
        opt_segAlgGroupBox = new QGroupBox(scrollAreaWidgetContents);
        opt_segAlgGroupBox->setObjectName(QStringLiteral("opt_segAlgGroupBox"));
        sizePolicy2.setHeightForWidth(opt_segAlgGroupBox->sizePolicy().hasHeightForWidth());
        opt_segAlgGroupBox->setSizePolicy(sizePolicy2);
        opt_segAlgGroupBox->setMinimumSize(QSize(180, 0));
        opt_segAlgGroupBox->setFlat(false);
        opt_segAlgGroupBox->setCheckable(false);
        gridLayout_5 = new QGridLayout(opt_segAlgGroupBox);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        opt_segLineEditMax4 = new QLineEdit(opt_segAlgGroupBox);
        opt_segLineEditMax4->setObjectName(QStringLiteral("opt_segLineEditMax4"));
        QSizePolicy sizePolicy3(QSizePolicy::Ignored, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(opt_segLineEditMax4->sizePolicy().hasHeightForWidth());
        opt_segLineEditMax4->setSizePolicy(sizePolicy3);
        opt_segLineEditMax4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(opt_segLineEditMax4, 7, 2, 1, 1);

        opt_segLineEditMin4 = new QLineEdit(opt_segAlgGroupBox);
        opt_segLineEditMin4->setObjectName(QStringLiteral("opt_segLineEditMin4"));
        sizePolicy3.setHeightForWidth(opt_segLineEditMin4->sizePolicy().hasHeightForWidth());
        opt_segLineEditMin4->setSizePolicy(sizePolicy3);
        opt_segLineEditMin4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(opt_segLineEditMin4, 7, 1, 1, 1);

        opt_segLabel8 = new QLabel(opt_segAlgGroupBox);
        opt_segLabel8->setObjectName(QStringLiteral("opt_segLabel8"));

        gridLayout_5->addWidget(opt_segLabel8, 1, 2, 1, 1);

        opt_segLabel1 = new QLabel(opt_segAlgGroupBox);
        opt_segLabel1->setObjectName(QStringLiteral("opt_segLabel1"));
        sizePolicy2.setHeightForWidth(opt_segLabel1->sizePolicy().hasHeightForWidth());
        opt_segLabel1->setSizePolicy(sizePolicy2);

        gridLayout_5->addWidget(opt_segLabel1, 4, 0, 1, 1);

        opt_segLabel5 = new QLabel(opt_segAlgGroupBox);
        opt_segLabel5->setObjectName(QStringLiteral("opt_segLabel5"));
        QSizePolicy sizePolicy4(QSizePolicy::Ignored, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(opt_segLabel5->sizePolicy().hasHeightForWidth());
        opt_segLabel5->setSizePolicy(sizePolicy4);

        gridLayout_5->addWidget(opt_segLabel5, 9, 0, 1, 1);

        opt_segLabel3 = new QLabel(opt_segAlgGroupBox);
        opt_segLabel3->setObjectName(QStringLiteral("opt_segLabel3"));
        sizePolicy2.setHeightForWidth(opt_segLabel3->sizePolicy().hasHeightForWidth());
        opt_segLabel3->setSizePolicy(sizePolicy2);

        gridLayout_5->addWidget(opt_segLabel3, 6, 0, 1, 1);

        opt_label1_3 = new QLabel(opt_segAlgGroupBox);
        opt_label1_3->setObjectName(QStringLiteral("opt_label1_3"));
        sizePolicy3.setHeightForWidth(opt_label1_3->sizePolicy().hasHeightForWidth());
        opt_label1_3->setSizePolicy(sizePolicy3);
        opt_label1_3->setAlignment(Qt::AlignCenter);

        gridLayout_5->addWidget(opt_label1_3, 3, 1, 1, 1);

        opt_segLineEditMin3 = new QLineEdit(opt_segAlgGroupBox);
        opt_segLineEditMin3->setObjectName(QStringLiteral("opt_segLineEditMin3"));
        sizePolicy3.setHeightForWidth(opt_segLineEditMin3->sizePolicy().hasHeightForWidth());
        opt_segLineEditMin3->setSizePolicy(sizePolicy3);
        opt_segLineEditMin3->setMinimumSize(QSize(50, 0));
        opt_segLineEditMin3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(opt_segLineEditMin3, 6, 1, 1, 1);

        opt_segLabel6 = new QLabel(opt_segAlgGroupBox);
        opt_segLabel6->setObjectName(QStringLiteral("opt_segLabel6"));

        gridLayout_5->addWidget(opt_segLabel6, 1, 1, 1, 1);

        opt_segLabel7 = new QLabel(opt_segAlgGroupBox);
        opt_segLabel7->setObjectName(QStringLiteral("opt_segLabel7"));
        sizePolicy4.setHeightForWidth(opt_segLabel7->sizePolicy().hasHeightForWidth());
        opt_segLabel7->setSizePolicy(sizePolicy4);

        gridLayout_5->addWidget(opt_segLabel7, 1, 0, 1, 1);

        opt_segAlgComboBox3 = new QComboBox(opt_segAlgGroupBox);
        opt_segAlgComboBox3->setObjectName(QStringLiteral("opt_segAlgComboBox3"));
        sizePolicy1.setHeightForWidth(opt_segAlgComboBox3->sizePolicy().hasHeightForWidth());
        opt_segAlgComboBox3->setSizePolicy(sizePolicy1);

        gridLayout_5->addWidget(opt_segAlgComboBox3, 2, 0, 1, 1);

        opt_segLabel4 = new QLabel(opt_segAlgGroupBox);
        opt_segLabel4->setObjectName(QStringLiteral("opt_segLabel4"));
        sizePolicy2.setHeightForWidth(opt_segLabel4->sizePolicy().hasHeightForWidth());
        opt_segLabel4->setSizePolicy(sizePolicy2);

        gridLayout_5->addWidget(opt_segLabel4, 7, 0, 1, 1);

        opt_segAlgComboBox4 = new QComboBox(opt_segAlgGroupBox);
        opt_segAlgComboBox4->setObjectName(QStringLiteral("opt_segAlgComboBox4"));
        sizePolicy3.setHeightForWidth(opt_segAlgComboBox4->sizePolicy().hasHeightForWidth());
        opt_segAlgComboBox4->setSizePolicy(sizePolicy3);
        opt_segAlgComboBox4->setMinimumSize(QSize(60, 0));

        gridLayout_5->addWidget(opt_segAlgComboBox4, 2, 2, 1, 1);

        opt_segLineEditMax1 = new QLineEdit(opt_segAlgGroupBox);
        opt_segLineEditMax1->setObjectName(QStringLiteral("opt_segLineEditMax1"));
        sizePolicy3.setHeightForWidth(opt_segLineEditMax1->sizePolicy().hasHeightForWidth());
        opt_segLineEditMax1->setSizePolicy(sizePolicy3);
        opt_segLineEditMax1->setMinimumSize(QSize(10, 0));
        opt_segLineEditMax1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(opt_segLineEditMax1, 4, 2, 1, 1);

        opt_label1_4 = new QLabel(opt_segAlgGroupBox);
        opt_label1_4->setObjectName(QStringLiteral("opt_label1_4"));
        sizePolicy3.setHeightForWidth(opt_label1_4->sizePolicy().hasHeightForWidth());
        opt_label1_4->setSizePolicy(sizePolicy3);
        opt_label1_4->setAlignment(Qt::AlignCenter);

        gridLayout_5->addWidget(opt_label1_4, 3, 2, 1, 1);

        opt_segLineEditMin2 = new QLineEdit(opt_segAlgGroupBox);
        opt_segLineEditMin2->setObjectName(QStringLiteral("opt_segLineEditMin2"));
        sizePolicy3.setHeightForWidth(opt_segLineEditMin2->sizePolicy().hasHeightForWidth());
        opt_segLineEditMin2->setSizePolicy(sizePolicy3);
        opt_segLineEditMin2->setMinimumSize(QSize(50, 0));
        opt_segLineEditMin2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(opt_segLineEditMin2, 5, 1, 1, 1);

        opt_segAlgComboBox = new QComboBox(opt_segAlgGroupBox);
        opt_segAlgComboBox->setObjectName(QStringLiteral("opt_segAlgComboBox"));
        opt_segAlgComboBox->setEnabled(true);
        sizePolicy3.setHeightForWidth(opt_segAlgComboBox->sizePolicy().hasHeightForWidth());
        opt_segAlgComboBox->setSizePolicy(sizePolicy3);

        gridLayout_5->addWidget(opt_segAlgComboBox, 0, 0, 1, 3);

        opt_segLineEditMax3 = new QLineEdit(opt_segAlgGroupBox);
        opt_segLineEditMax3->setObjectName(QStringLiteral("opt_segLineEditMax3"));
        sizePolicy3.setHeightForWidth(opt_segLineEditMax3->sizePolicy().hasHeightForWidth());
        opt_segLineEditMax3->setSizePolicy(sizePolicy3);
        opt_segLineEditMax3->setMinimumSize(QSize(50, 0));
        opt_segLineEditMax3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(opt_segLineEditMax3, 6, 2, 1, 1);

        opt_segLineEditMax2 = new QLineEdit(opt_segAlgGroupBox);
        opt_segLineEditMax2->setObjectName(QStringLiteral("opt_segLineEditMax2"));
        sizePolicy3.setHeightForWidth(opt_segLineEditMax2->sizePolicy().hasHeightForWidth());
        opt_segLineEditMax2->setSizePolicy(sizePolicy3);
        opt_segLineEditMax2->setMinimumSize(QSize(50, 0));
        opt_segLineEditMax2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(opt_segLineEditMax2, 5, 2, 1, 1);

        opt_segLabel2 = new QLabel(opt_segAlgGroupBox);
        opt_segLabel2->setObjectName(QStringLiteral("opt_segLabel2"));
        sizePolicy2.setHeightForWidth(opt_segLabel2->sizePolicy().hasHeightForWidth());
        opt_segLabel2->setSizePolicy(sizePolicy2);

        gridLayout_5->addWidget(opt_segLabel2, 5, 0, 1, 1);

        opt_segAlgComboBox2 = new QComboBox(opt_segAlgGroupBox);
        opt_segAlgComboBox2->setObjectName(QStringLiteral("opt_segAlgComboBox2"));
        sizePolicy3.setHeightForWidth(opt_segAlgComboBox2->sizePolicy().hasHeightForWidth());
        opt_segAlgComboBox2->setSizePolicy(sizePolicy3);

        gridLayout_5->addWidget(opt_segAlgComboBox2, 2, 1, 1, 1);

        opt_segLineEditMin1 = new QLineEdit(opt_segAlgGroupBox);
        opt_segLineEditMin1->setObjectName(QStringLiteral("opt_segLineEditMin1"));
        sizePolicy3.setHeightForWidth(opt_segLineEditMin1->sizePolicy().hasHeightForWidth());
        opt_segLineEditMin1->setSizePolicy(sizePolicy3);
        opt_segLineEditMin1->setMinimumSize(QSize(10, 0));
        opt_segLineEditMin1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(opt_segLineEditMin1, 4, 1, 1, 1);

        opt_segLabelBands = new QLabel(opt_segAlgGroupBox);
        opt_segLabelBands->setObjectName(QStringLiteral("opt_segLabelBands"));
        sizePolicy4.setHeightForWidth(opt_segLabelBands->sizePolicy().hasHeightForWidth());
        opt_segLabelBands->setSizePolicy(sizePolicy4);

        gridLayout_5->addWidget(opt_segLabelBands, 10, 0, 1, 1);

        opt_segLineEditBands = new QLineEdit(opt_segAlgGroupBox);
        opt_segLineEditBands->setObjectName(QStringLiteral("opt_segLineEditBands"));
        sizePolicy3.setHeightForWidth(opt_segLineEditBands->sizePolicy().hasHeightForWidth());
        opt_segLineEditBands->setSizePolicy(sizePolicy3);
        opt_segLineEditBands->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(opt_segLineEditBands, 10, 1, 1, 2);

        opt_segLabel5_Coeff = new QLabel(opt_segAlgGroupBox);
        opt_segLabel5_Coeff->setObjectName(QStringLiteral("opt_segLabel5_Coeff"));
        sizePolicy2.setHeightForWidth(opt_segLabel5_Coeff->sizePolicy().hasHeightForWidth());
        opt_segLabel5_Coeff->setSizePolicy(sizePolicy2);
        opt_segLabel5_Coeff->setMinimumSize(QSize(0, 0));

        gridLayout_5->addWidget(opt_segLabel5_Coeff, 8, 0, 1, 1);

        opt_segLineEditMin5 = new QLineEdit(opt_segAlgGroupBox);
        opt_segLineEditMin5->setObjectName(QStringLiteral("opt_segLineEditMin5"));
        sizePolicy3.setHeightForWidth(opt_segLineEditMin5->sizePolicy().hasHeightForWidth());
        opt_segLineEditMin5->setSizePolicy(sizePolicy3);
        opt_segLineEditMin5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(opt_segLineEditMin5, 8, 1, 1, 1);

        opt_segLineEditMax5 = new QLineEdit(opt_segAlgGroupBox);
        opt_segLineEditMax5->setObjectName(QStringLiteral("opt_segLineEditMax5"));
        sizePolicy3.setHeightForWidth(opt_segLineEditMax5->sizePolicy().hasHeightForWidth());
        opt_segLineEditMax5->setSizePolicy(sizePolicy3);
        opt_segLineEditMax5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(opt_segLineEditMax5, 8, 2, 1, 1);

        opt_segLineEditMin6 = new QLineEdit(opt_segAlgGroupBox);
        opt_segLineEditMin6->setObjectName(QStringLiteral("opt_segLineEditMin6"));
        sizePolicy3.setHeightForWidth(opt_segLineEditMin6->sizePolicy().hasHeightForWidth());
        opt_segLineEditMin6->setSizePolicy(sizePolicy3);
        opt_segLineEditMin6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(opt_segLineEditMin6, 9, 1, 1, 1);

        opt_segLineEditMax6 = new QLineEdit(opt_segAlgGroupBox);
        opt_segLineEditMax6->setObjectName(QStringLiteral("opt_segLineEditMax6"));
        sizePolicy3.setHeightForWidth(opt_segLineEditMax6->sizePolicy().hasHeightForWidth());
        opt_segLineEditMax6->setSizePolicy(sizePolicy3);
        opt_segLineEditMax6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(opt_segLineEditMax6, 9, 2, 1, 1);

        opt_segLabel3->raise();
        opt_segLineEditMin1->raise();
        opt_segLabel2->raise();
        opt_segAlgComboBox->raise();
        opt_segLabel1->raise();
        opt_label1_3->raise();
        opt_label1_4->raise();
        opt_segLineEditMax1->raise();
        opt_segLineEditMax2->raise();
        opt_segLineEditMin2->raise();
        opt_segLineEditMin3->raise();
        opt_segLineEditMax3->raise();
        opt_segLabel4->raise();
        opt_segLineEditMin4->raise();
        opt_segLineEditMax4->raise();
        opt_segLabel5->raise();
        opt_segLabel7->raise();
        opt_segLabel6->raise();
        opt_segLabel8->raise();
        opt_segAlgComboBox2->raise();
        opt_segAlgComboBox3->raise();
        opt_segAlgComboBox4->raise();
        opt_segLabelBands->raise();
        opt_segLineEditBands->raise();
        opt_segLabel5_Coeff->raise();
        opt_segLineEditMin5->raise();
        opt_segLineEditMax5->raise();
        opt_segLineEditMin6->raise();
        opt_segLineEditMax6->raise();

        gridLayout_17->addWidget(opt_segAlgGroupBox, 1, 0, 1, 1);

        opt_fitGroupBox = new QGroupBox(scrollAreaWidgetContents);
        opt_fitGroupBox->setObjectName(QStringLiteral("opt_fitGroupBox"));
        opt_fitGroupBox->setFlat(false);
        opt_fitGroupBox->setCheckable(false);
        gridLayout_8 = new QGridLayout(opt_fitGroupBox);
        gridLayout_8->setSpacing(6);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        opt_segLabel9 = new QLabel(opt_fitGroupBox);
        opt_segLabel9->setObjectName(QStringLiteral("opt_segLabel9"));
        sizePolicy4.setHeightForWidth(opt_segLabel9->sizePolicy().hasHeightForWidth());
        opt_segLabel9->setSizePolicy(sizePolicy4);

        gridLayout_8->addWidget(opt_segLabel9, 1, 0, 1, 1);

        opt_segLineEditTimesToRun = new QLineEdit(opt_fitGroupBox);
        opt_segLineEditTimesToRun->setObjectName(QStringLiteral("opt_segLineEditTimesToRun"));
        sizePolicy3.setHeightForWidth(opt_segLineEditTimesToRun->sizePolicy().hasHeightForWidth());
        opt_segLineEditTimesToRun->setSizePolicy(sizePolicy3);

        gridLayout_8->addWidget(opt_segLineEditTimesToRun, 1, 1, 1, 1);

        opt_fitComboBox = new QComboBox(opt_fitGroupBox);
        opt_fitComboBox->setObjectName(QStringLiteral("opt_fitComboBox"));

        gridLayout_8->addWidget(opt_fitComboBox, 0, 0, 1, 2);


        gridLayout_17->addWidget(opt_fitGroupBox, 2, 0, 1, 1);

        opt_outGroupBox = new QGroupBox(scrollAreaWidgetContents);
        opt_outGroupBox->setObjectName(QStringLiteral("opt_outGroupBox"));
        sizePolicy4.setHeightForWidth(opt_outGroupBox->sizePolicy().hasHeightForWidth());
        opt_outGroupBox->setSizePolicy(sizePolicy4);
        gridLayout_7 = new QGridLayout(opt_outGroupBox);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        opt_outLineEdit = new QLineEdit(opt_outGroupBox);
        opt_outLineEdit->setObjectName(QStringLiteral("opt_outLineEdit"));
        sizePolicy1.setHeightForWidth(opt_outLineEdit->sizePolicy().hasHeightForWidth());
        opt_outLineEdit->setSizePolicy(sizePolicy1);
        opt_outLineEdit->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_7->addWidget(opt_outLineEdit, 0, 1, 1, 1);

        opt_outPushButton = new QPushButton(opt_outGroupBox);
        opt_outPushButton->setObjectName(QStringLiteral("opt_outPushButton"));

        gridLayout_7->addWidget(opt_outPushButton, 1, 1, 1, 1);

        opt_outLabel = new QLabel(opt_outGroupBox);
        opt_outLabel->setObjectName(QStringLiteral("opt_outLabel"));

        gridLayout_7->addWidget(opt_outLabel, 0, 0, 1, 1);

        opt_segLineEdit5 = new QLineEdit(opt_outGroupBox);
        opt_segLineEdit5->setObjectName(QStringLiteral("opt_segLineEdit5"));
        sizePolicy3.setHeightForWidth(opt_segLineEdit5->sizePolicy().hasHeightForWidth());
        opt_segLineEdit5->setSizePolicy(sizePolicy3);
        opt_segLineEdit5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_7->addWidget(opt_segLineEdit5, 2, 1, 1, 1);


        gridLayout_17->addWidget(opt_outGroupBox, 3, 0, 1, 1);

        opt_runPushButton = new QPushButton(scrollAreaWidgetContents);
        opt_runPushButton->setObjectName(QStringLiteral("opt_runPushButton"));
        QSizePolicy sizePolicy5(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(opt_runPushButton->sizePolicy().hasHeightForWidth());
        opt_runPushButton->setSizePolicy(sizePolicy5);

        gridLayout_17->addWidget(opt_runPushButton, 4, 0, 1, 1);

        opt_segLabelID = new QLabel(scrollAreaWidgetContents);
        opt_segLabelID->setObjectName(QStringLiteral("opt_segLabelID"));
        sizePolicy3.setHeightForWidth(opt_segLabelID->sizePolicy().hasHeightForWidth());
        opt_segLabelID->setSizePolicy(sizePolicy3);
        opt_segLabelID->setMinimumSize(QSize(0, 20));

        gridLayout_17->addWidget(opt_segLabelID, 5, 0, 1, 1);

        opt_segLabelIteration = new QLabel(scrollAreaWidgetContents);
        opt_segLabelIteration->setObjectName(QStringLiteral("opt_segLabelIteration"));
        sizePolicy5.setHeightForWidth(opt_segLabelIteration->sizePolicy().hasHeightForWidth());
        opt_segLabelIteration->setSizePolicy(sizePolicy5);
        opt_segLabelIteration->setMinimumSize(QSize(20, 0));

        gridLayout_17->addWidget(opt_segLabelIteration, 6, 0, 1, 1);

        opt_ProgressBar = new QProgressBar(scrollAreaWidgetContents);
        opt_ProgressBar->setObjectName(QStringLiteral("opt_ProgressBar"));
        sizePolicy3.setHeightForWidth(opt_ProgressBar->sizePolicy().hasHeightForWidth());
        opt_ProgressBar->setSizePolicy(sizePolicy3);
        opt_ProgressBar->setValue(24);

        gridLayout_17->addWidget(opt_ProgressBar, 7, 0, 1, 1);

        opt_optAlgGroupBox = new QGroupBox(scrollAreaWidgetContents);
        opt_optAlgGroupBox->setObjectName(QStringLiteral("opt_optAlgGroupBox"));
        sizePolicy4.setHeightForWidth(opt_optAlgGroupBox->sizePolicy().hasHeightForWidth());
        opt_optAlgGroupBox->setSizePolicy(sizePolicy4);
        opt_optAlgGroupBox->setMinimumSize(QSize(0, 0));
        opt_optAlgGroupBox->setFlat(false);
        opt_optAlgGroupBox->setCheckable(false);
        gridLayout_4 = new QGridLayout(opt_optAlgGroupBox);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        opt_optAlgComboBox = new QComboBox(opt_optAlgGroupBox);
        opt_optAlgComboBox->setObjectName(QStringLiteral("opt_optAlgComboBox"));

        gridLayout_4->addWidget(opt_optAlgComboBox, 1, 0, 1, 2);

        opt_optLineEdit2 = new QLineEdit(opt_optAlgGroupBox);
        opt_optLineEdit2->setObjectName(QStringLiteral("opt_optLineEdit2"));
        sizePolicy1.setHeightForWidth(opt_optLineEdit2->sizePolicy().hasHeightForWidth());
        opt_optLineEdit2->setSizePolicy(sizePolicy1);
        opt_optLineEdit2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(opt_optLineEdit2, 3, 1, 1, 1);

        opt_optLabel2 = new QLabel(opt_optAlgGroupBox);
        opt_optLabel2->setObjectName(QStringLiteral("opt_optLabel2"));

        gridLayout_4->addWidget(opt_optLabel2, 3, 0, 1, 1);

        opt_optLineEdit1 = new QLineEdit(opt_optAlgGroupBox);
        opt_optLineEdit1->setObjectName(QStringLiteral("opt_optLineEdit1"));
        sizePolicy1.setHeightForWidth(opt_optLineEdit1->sizePolicy().hasHeightForWidth());
        opt_optLineEdit1->setSizePolicy(sizePolicy1);
        opt_optLineEdit1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(opt_optLineEdit1, 2, 1, 1, 1);

        opt_optLineEdit4 = new QLineEdit(opt_optAlgGroupBox);
        opt_optLineEdit4->setObjectName(QStringLiteral("opt_optLineEdit4"));
        sizePolicy1.setHeightForWidth(opt_optLineEdit4->sizePolicy().hasHeightForWidth());
        opt_optLineEdit4->setSizePolicy(sizePolicy1);
        opt_optLineEdit4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(opt_optLineEdit4, 5, 1, 1, 1);

        opt_optLabel4 = new QLabel(opt_optAlgGroupBox);
        opt_optLabel4->setObjectName(QStringLiteral("opt_optLabel4"));

        gridLayout_4->addWidget(opt_optLabel4, 5, 0, 1, 1);

        opt_optLabel1 = new QLabel(opt_optAlgGroupBox);
        opt_optLabel1->setObjectName(QStringLiteral("opt_optLabel1"));

        gridLayout_4->addWidget(opt_optLabel1, 2, 0, 1, 1);

        opt_optLabel5 = new QLabel(opt_optAlgGroupBox);
        opt_optLabel5->setObjectName(QStringLiteral("opt_optLabel5"));

        gridLayout_4->addWidget(opt_optLabel5, 6, 0, 1, 1);

        opt_optLineEdit3 = new QLineEdit(opt_optAlgGroupBox);
        opt_optLineEdit3->setObjectName(QStringLiteral("opt_optLineEdit3"));
        sizePolicy1.setHeightForWidth(opt_optLineEdit3->sizePolicy().hasHeightForWidth());
        opt_optLineEdit3->setSizePolicy(sizePolicy1);
        opt_optLineEdit3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(opt_optLineEdit3, 4, 1, 1, 1);

        opt_optLabel3 = new QLabel(opt_optAlgGroupBox);
        opt_optLabel3->setObjectName(QStringLiteral("opt_optLabel3"));

        gridLayout_4->addWidget(opt_optLabel3, 4, 0, 1, 1);

        opt_optAdvComboBox = new QComboBox(opt_optAlgGroupBox);
        opt_optAdvComboBox->setObjectName(QStringLiteral("opt_optAdvComboBox"));

        gridLayout_4->addWidget(opt_optAdvComboBox, 6, 1, 1, 1);

        opt_optLabel1->raise();
        opt_optAlgComboBox->raise();
        opt_optAdvComboBox->raise();
        opt_optLabel5->raise();
        opt_optLabel2->raise();
        opt_optLabel3->raise();
        opt_optLabel4->raise();
        opt_optLineEdit1->raise();
        opt_optLineEdit2->raise();
        opt_optLineEdit3->raise();
        opt_optLineEdit4->raise();

        gridLayout_17->addWidget(opt_optAlgGroupBox, 0, 0, 1, 1);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout_6->addWidget(scrollArea, 0, 0, 9, 1);

        layout_canvas_opt = new QHBoxLayout();
        layout_canvas_opt->setSpacing(6);
        layout_canvas_opt->setObjectName(QStringLiteral("layout_canvas_opt"));
        layout_canvas_opt->setSizeConstraint(QLayout::SetNoConstraint);

        gridLayout_6->addLayout(layout_canvas_opt, 0, 1, 9, 1);

        tabWidget->addTab(optimizationTab, QString());
        segmentationTab = new QWidget();
        segmentationTab->setObjectName(QStringLiteral("segmentationTab"));
        segmentationTab->setMaximumSize(QSize(16777215, 16777215));
        segmentationTab->setToolTipDuration(-1);
        gridLayout_15 = new QGridLayout(segmentationTab);
        gridLayout_15->setSpacing(6);
        gridLayout_15->setContentsMargins(11, 11, 11, 11);
        gridLayout_15->setObjectName(QStringLiteral("gridLayout_15"));
        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_15->addItem(verticalSpacer_5, 4, 0, 1, 1);

        layout_canvas_seg = new QHBoxLayout();
        layout_canvas_seg->setSpacing(6);
        layout_canvas_seg->setObjectName(QStringLiteral("layout_canvas_seg"));
        layout_canvas_seg->setSizeConstraint(QLayout::SetNoConstraint);

        gridLayout_15->addLayout(layout_canvas_seg, 0, 1, 5, 1);

        groupBox_4 = new QGroupBox(segmentationTab);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        sizePolicy2.setHeightForWidth(groupBox_4->sizePolicy().hasHeightForWidth());
        groupBox_4->setSizePolicy(sizePolicy2);
        gridLayout_10 = new QGridLayout(groupBox_4);
        gridLayout_10->setSpacing(6);
        gridLayout_10->setContentsMargins(11, 11, 11, 11);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        seg_metricsComboBox = new QComboBox(groupBox_4);
        seg_metricsComboBox->setObjectName(QStringLiteral("seg_metricsComboBox"));

        gridLayout_10->addWidget(seg_metricsComboBox, 0, 0, 1, 2);

        seg_refCalculatePushButton = new QPushButton(groupBox_4);
        seg_refCalculatePushButton->setObjectName(QStringLiteral("seg_refCalculatePushButton"));

        gridLayout_10->addWidget(seg_refCalculatePushButton, 3, 0, 1, 2);

        seg_metricLineEdit = new QLineEdit(groupBox_4);
        seg_metricLineEdit->setObjectName(QStringLiteral("seg_metricLineEdit"));
        seg_metricLineEdit->setEnabled(false);

        gridLayout_10->addWidget(seg_metricLineEdit, 4, 1, 1, 1);

        label_2 = new QLabel(groupBox_4);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_10->addWidget(label_2, 4, 0, 1, 1);

        seg_refLoadPushButton = new QPushButton(groupBox_4);
        seg_refLoadPushButton->setObjectName(QStringLiteral("seg_refLoadPushButton"));
        QSizePolicy sizePolicy6(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(seg_refLoadPushButton->sizePolicy().hasHeightForWidth());
        seg_refLoadPushButton->setSizePolicy(sizePolicy6);

        gridLayout_10->addWidget(seg_refLoadPushButton, 1, 0, 1, 2);

        seg_clearRefButton = new QPushButton(groupBox_4);
        seg_clearRefButton->setObjectName(QStringLiteral("seg_clearRefButton"));
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/icons/QPolygons/icons/clear.png"), QSize(), QIcon::Normal, QIcon::Off);
        seg_clearRefButton->setIcon(icon9);

        gridLayout_10->addWidget(seg_clearRefButton, 2, 0, 1, 2);


        gridLayout_15->addWidget(groupBox_4, 3, 0, 1, 1);

        seg_segAlgGroupBox = new QGroupBox(segmentationTab);
        seg_segAlgGroupBox->setObjectName(QStringLiteral("seg_segAlgGroupBox"));
        sizePolicy4.setHeightForWidth(seg_segAlgGroupBox->sizePolicy().hasHeightForWidth());
        seg_segAlgGroupBox->setSizePolicy(sizePolicy4);
        seg_segAlgGroupBox->setMinimumSize(QSize(180, 0));
        gridLayout_11 = new QGridLayout(seg_segAlgGroupBox);
        gridLayout_11->setSpacing(6);
        gridLayout_11->setContentsMargins(11, 11, 11, 11);
        gridLayout_11->setObjectName(QStringLiteral("gridLayout_11"));
        seg_segLabel2 = new QLabel(seg_segAlgGroupBox);
        seg_segLabel2->setObjectName(QStringLiteral("seg_segLabel2"));
        sizePolicy2.setHeightForWidth(seg_segLabel2->sizePolicy().hasHeightForWidth());
        seg_segLabel2->setSizePolicy(sizePolicy2);

        gridLayout_11->addWidget(seg_segLabel2, 4, 0, 1, 1);

        seg_segLineEdit3 = new QLineEdit(seg_segAlgGroupBox);
        seg_segLineEdit3->setObjectName(QStringLiteral("seg_segLineEdit3"));
        sizePolicy3.setHeightForWidth(seg_segLineEdit3->sizePolicy().hasHeightForWidth());
        seg_segLineEdit3->setSizePolicy(sizePolicy3);
        seg_segLineEdit3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_11->addWidget(seg_segLineEdit3, 5, 1, 1, 2);

        seg_LoadParamsFilePushButton = new QPushButton(seg_segAlgGroupBox);
        seg_LoadParamsFilePushButton->setObjectName(QStringLiteral("seg_LoadParamsFilePushButton"));

        gridLayout_11->addWidget(seg_LoadParamsFilePushButton, 12, 0, 1, 3);

        seg_segAlgComboBox3 = new QComboBox(seg_segAlgGroupBox);
        seg_segAlgComboBox3->setObjectName(QStringLiteral("seg_segAlgComboBox3"));
        sizePolicy5.setHeightForWidth(seg_segAlgComboBox3->sizePolicy().hasHeightForWidth());
        seg_segAlgComboBox3->setSizePolicy(sizePolicy5);

        gridLayout_11->addWidget(seg_segAlgComboBox3, 2, 0, 1, 1);

        seg_segAlgComboBox2 = new QComboBox(seg_segAlgGroupBox);
        seg_segAlgComboBox2->setObjectName(QStringLiteral("seg_segAlgComboBox2"));
        sizePolicy5.setHeightForWidth(seg_segAlgComboBox2->sizePolicy().hasHeightForWidth());
        seg_segAlgComboBox2->setSizePolicy(sizePolicy5);

        gridLayout_11->addWidget(seg_segAlgComboBox2, 2, 1, 1, 1);

        seg_segAlgComboBox4 = new QComboBox(seg_segAlgGroupBox);
        seg_segAlgComboBox4->setObjectName(QStringLiteral("seg_segAlgComboBox4"));
        sizePolicy3.setHeightForWidth(seg_segAlgComboBox4->sizePolicy().hasHeightForWidth());
        seg_segAlgComboBox4->setSizePolicy(sizePolicy3);
        seg_segAlgComboBox4->setMinimumSize(QSize(60, 0));

        gridLayout_11->addWidget(seg_segAlgComboBox4, 2, 2, 1, 1);

        seg_segLabel6 = new QLabel(seg_segAlgGroupBox);
        seg_segLabel6->setObjectName(QStringLiteral("seg_segLabel6"));
        QSizePolicy sizePolicy7(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(seg_segLabel6->sizePolicy().hasHeightForWidth());
        seg_segLabel6->setSizePolicy(sizePolicy7);

        gridLayout_11->addWidget(seg_segLabel6, 1, 1, 1, 1);

        seg_segLabel8 = new QLabel(seg_segAlgGroupBox);
        seg_segLabel8->setObjectName(QStringLiteral("seg_segLabel8"));
        sizePolicy7.setHeightForWidth(seg_segLabel8->sizePolicy().hasHeightForWidth());
        seg_segLabel8->setSizePolicy(sizePolicy7);

        gridLayout_11->addWidget(seg_segLabel8, 1, 2, 1, 1);

        seg_segLabel7 = new QLabel(seg_segAlgGroupBox);
        seg_segLabel7->setObjectName(QStringLiteral("seg_segLabel7"));
        sizePolicy4.setHeightForWidth(seg_segLabel7->sizePolicy().hasHeightForWidth());
        seg_segLabel7->setSizePolicy(sizePolicy4);

        gridLayout_11->addWidget(seg_segLabel7, 1, 0, 1, 1);

        seg_segAlgComboBox = new QComboBox(seg_segAlgGroupBox);
        seg_segAlgComboBox->setObjectName(QStringLiteral("seg_segAlgComboBox"));
        sizePolicy3.setHeightForWidth(seg_segAlgComboBox->sizePolicy().hasHeightForWidth());
        seg_segAlgComboBox->setSizePolicy(sizePolicy3);
        seg_segAlgComboBox->setMinimumSize(QSize(100, 0));

        gridLayout_11->addWidget(seg_segAlgComboBox, 0, 0, 1, 3);

        seg_segLineEdit5 = new QLineEdit(seg_segAlgGroupBox);
        seg_segLineEdit5->setObjectName(QStringLiteral("seg_segLineEdit5"));
        sizePolicy3.setHeightForWidth(seg_segLineEdit5->sizePolicy().hasHeightForWidth());
        seg_segLineEdit5->setSizePolicy(sizePolicy3);
        seg_segLineEdit5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_11->addWidget(seg_segLineEdit5, 7, 1, 1, 2);

        seg_segLabel5 = new QLabel(seg_segAlgGroupBox);
        seg_segLabel5->setObjectName(QStringLiteral("seg_segLabel5"));
        sizePolicy2.setHeightForWidth(seg_segLabel5->sizePolicy().hasHeightForWidth());
        seg_segLabel5->setSizePolicy(sizePolicy2);

        gridLayout_11->addWidget(seg_segLabel5, 7, 0, 1, 1);

        seg_segLineEdit1 = new QLineEdit(seg_segAlgGroupBox);
        seg_segLineEdit1->setObjectName(QStringLiteral("seg_segLineEdit1"));
        sizePolicy3.setHeightForWidth(seg_segLineEdit1->sizePolicy().hasHeightForWidth());
        seg_segLineEdit1->setSizePolicy(sizePolicy3);
        seg_segLineEdit1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_11->addWidget(seg_segLineEdit1, 3, 1, 1, 2);

        seg_segLineEdit4 = new QLineEdit(seg_segAlgGroupBox);
        seg_segLineEdit4->setObjectName(QStringLiteral("seg_segLineEdit4"));
        sizePolicy3.setHeightForWidth(seg_segLineEdit4->sizePolicy().hasHeightForWidth());
        seg_segLineEdit4->setSizePolicy(sizePolicy3);
        seg_segLineEdit4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_11->addWidget(seg_segLineEdit4, 6, 1, 1, 2);

        seg_segLabel4 = new QLabel(seg_segAlgGroupBox);
        seg_segLabel4->setObjectName(QStringLiteral("seg_segLabel4"));
        sizePolicy2.setHeightForWidth(seg_segLabel4->sizePolicy().hasHeightForWidth());
        seg_segLabel4->setSizePolicy(sizePolicy2);

        gridLayout_11->addWidget(seg_segLabel4, 6, 0, 1, 1);

        seg_segLabel1 = new QLabel(seg_segAlgGroupBox);
        seg_segLabel1->setObjectName(QStringLiteral("seg_segLabel1"));
        sizePolicy2.setHeightForWidth(seg_segLabel1->sizePolicy().hasHeightForWidth());
        seg_segLabel1->setSizePolicy(sizePolicy2);

        gridLayout_11->addWidget(seg_segLabel1, 3, 0, 1, 1);

        seg_segLineEdit2 = new QLineEdit(seg_segAlgGroupBox);
        seg_segLineEdit2->setObjectName(QStringLiteral("seg_segLineEdit2"));
        sizePolicy3.setHeightForWidth(seg_segLineEdit2->sizePolicy().hasHeightForWidth());
        seg_segLineEdit2->setSizePolicy(sizePolicy3);
        seg_segLineEdit2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_11->addWidget(seg_segLineEdit2, 4, 1, 1, 2);

        seg_segLabel3 = new QLabel(seg_segAlgGroupBox);
        seg_segLabel3->setObjectName(QStringLiteral("seg_segLabel3"));
        sizePolicy2.setHeightForWidth(seg_segLabel3->sizePolicy().hasHeightForWidth());
        seg_segLabel3->setSizePolicy(sizePolicy2);

        gridLayout_11->addWidget(seg_segLabel3, 5, 0, 1, 1);

        seg_segLabelBands = new QLabel(seg_segAlgGroupBox);
        seg_segLabelBands->setObjectName(QStringLiteral("seg_segLabelBands"));
        sizePolicy2.setHeightForWidth(seg_segLabelBands->sizePolicy().hasHeightForWidth());
        seg_segLabelBands->setSizePolicy(sizePolicy2);

        gridLayout_11->addWidget(seg_segLabelBands, 10, 0, 1, 1);

        seg_segLineEditBands = new QLineEdit(seg_segAlgGroupBox);
        seg_segLineEditBands->setObjectName(QStringLiteral("seg_segLineEditBands"));
        sizePolicy3.setHeightForWidth(seg_segLineEditBands->sizePolicy().hasHeightForWidth());
        seg_segLineEditBands->setSizePolicy(sizePolicy3);
        seg_segLineEditBands->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_11->addWidget(seg_segLineEditBands, 10, 1, 1, 2);

        seg_segLabel9 = new QLabel(seg_segAlgGroupBox);
        seg_segLabel9->setObjectName(QStringLiteral("seg_segLabel9"));
        sizePolicy2.setHeightForWidth(seg_segLabel9->sizePolicy().hasHeightForWidth());
        seg_segLabel9->setSizePolicy(sizePolicy2);

        gridLayout_11->addWidget(seg_segLabel9, 8, 0, 1, 1);

        seg_segLineEdit9 = new QLineEdit(seg_segAlgGroupBox);
        seg_segLineEdit9->setObjectName(QStringLiteral("seg_segLineEdit9"));
        sizePolicy3.setHeightForWidth(seg_segLineEdit9->sizePolicy().hasHeightForWidth());
        seg_segLineEdit9->setSizePolicy(sizePolicy3);
        seg_segLineEdit9->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_11->addWidget(seg_segLineEdit9, 8, 1, 1, 2);


        gridLayout_15->addWidget(seg_segAlgGroupBox, 0, 0, 1, 1);

        groupBox_3 = new QGroupBox(segmentationTab);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        sizePolicy4.setHeightForWidth(groupBox_3->sizePolicy().hasHeightForWidth());
        groupBox_3->setSizePolicy(sizePolicy4);
        gridLayout_9 = new QGridLayout(groupBox_3);
        gridLayout_9->setSpacing(6);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        seg_runPushButton = new QPushButton(groupBox_3);
        seg_runPushButton->setObjectName(QStringLiteral("seg_runPushButton"));
        sizePolicy6.setHeightForWidth(seg_runPushButton->sizePolicy().hasHeightForWidth());
        seg_runPushButton->setSizePolicy(sizePolicy6);

        gridLayout_9->addWidget(seg_runPushButton, 1, 0, 1, 2);

        seg_outPushButton = new QPushButton(groupBox_3);
        seg_outPushButton->setObjectName(QStringLiteral("seg_outPushButton"));

        gridLayout_9->addWidget(seg_outPushButton, 2, 0, 1, 2);

        label = new QLabel(groupBox_3);
        label->setObjectName(QStringLiteral("label"));
        sizePolicy4.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy4);

        gridLayout_9->addWidget(label, 0, 0, 1, 1);

        seg_outLineEdit = new QLineEdit(groupBox_3);
        seg_outLineEdit->setObjectName(QStringLiteral("seg_outLineEdit"));
        sizePolicy1.setHeightForWidth(seg_outLineEdit->sizePolicy().hasHeightForWidth());
        seg_outLineEdit->setSizePolicy(sizePolicy1);
        seg_outLineEdit->setMinimumSize(QSize(100, 0));
        seg_outLineEdit->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_9->addWidget(seg_outLineEdit, 0, 1, 1, 1);


        gridLayout_15->addWidget(groupBox_3, 2, 0, 1, 1);

        tabWidget->addTab(segmentationTab, QString());
        assessmentTab = new QWidget();
        assessmentTab->setObjectName(QStringLiteral("assessmentTab"));
        gridLayout = new QGridLayout(assessmentTab);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        layout_canvas_assess = new QHBoxLayout();
        layout_canvas_assess->setSpacing(6);
        layout_canvas_assess->setObjectName(QStringLiteral("layout_canvas_assess"));
        layout_canvas_assess->setSizeConstraint(QLayout::SetNoConstraint);

        gridLayout->addLayout(layout_canvas_assess, 1, 1, 4, 3);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_4, 4, 0, 1, 1);

        groupBox_6 = new QGroupBox(assessmentTab);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        sizePolicy1.setHeightForWidth(groupBox_6->sizePolicy().hasHeightForWidth());
        groupBox_6->setSizePolicy(sizePolicy1);
        gridLayout_14 = new QGridLayout(groupBox_6);
        gridLayout_14->setSpacing(6);
        gridLayout_14->setContentsMargins(11, 11, 11, 11);
        gridLayout_14->setObjectName(QStringLiteral("gridLayout_14"));
        assess_metricsComboBox = new QComboBox(groupBox_6);
        assess_metricsComboBox->setObjectName(QStringLiteral("assess_metricsComboBox"));
        sizePolicy5.setHeightForWidth(assess_metricsComboBox->sizePolicy().hasHeightForWidth());
        assess_metricsComboBox->setSizePolicy(sizePolicy5);
        assess_metricsComboBox->setMinimumSize(QSize(100, 0));

        gridLayout_14->addWidget(assess_metricsComboBox, 0, 0, 1, 1);

        assess_Calculate = new QPushButton(groupBox_6);
        assess_Calculate->setObjectName(QStringLiteral("assess_Calculate"));
        sizePolicy6.setHeightForWidth(assess_Calculate->sizePolicy().hasHeightForWidth());
        assess_Calculate->setSizePolicy(sizePolicy6);

        gridLayout_14->addWidget(assess_Calculate, 0, 1, 1, 1);


        gridLayout->addWidget(groupBox_6, 0, 2, 1, 1);

        assess_rgbGroupBox = new QGroupBox(assessmentTab);
        assess_rgbGroupBox->setObjectName(QStringLiteral("assess_rgbGroupBox"));
        sizePolicy1.setHeightForWidth(assess_rgbGroupBox->sizePolicy().hasHeightForWidth());
        assess_rgbGroupBox->setSizePolicy(sizePolicy1);
        gridLayout_12 = new QGridLayout(assess_rgbGroupBox);
        gridLayout_12->setSpacing(6);
        gridLayout_12->setContentsMargins(11, 11, 11, 11);
        gridLayout_12->setObjectName(QStringLiteral("gridLayout_12"));
        label_4 = new QLabel(assess_rgbGroupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        sizePolicy7.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy7);

        gridLayout_12->addWidget(label_4, 0, 0, 1, 1);

        assess_rgbRComboBox = new QComboBox(assess_rgbGroupBox);
        assess_rgbRComboBox->setObjectName(QStringLiteral("assess_rgbRComboBox"));
        sizePolicy5.setHeightForWidth(assess_rgbRComboBox->sizePolicy().hasHeightForWidth());
        assess_rgbRComboBox->setSizePolicy(sizePolicy5);

        gridLayout_12->addWidget(assess_rgbRComboBox, 1, 0, 1, 1);

        label_5 = new QLabel(assess_rgbGroupBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        sizePolicy7.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy7);

        gridLayout_12->addWidget(label_5, 2, 0, 1, 1);

        assess_rgbGComboBox = new QComboBox(assess_rgbGroupBox);
        assess_rgbGComboBox->setObjectName(QStringLiteral("assess_rgbGComboBox"));
        sizePolicy5.setHeightForWidth(assess_rgbGComboBox->sizePolicy().hasHeightForWidth());
        assess_rgbGComboBox->setSizePolicy(sizePolicy5);

        gridLayout_12->addWidget(assess_rgbGComboBox, 3, 0, 1, 1);

        label_6 = new QLabel(assess_rgbGroupBox);
        label_6->setObjectName(QStringLiteral("label_6"));
        sizePolicy7.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy7);

        gridLayout_12->addWidget(label_6, 4, 0, 1, 1);

        assess_rgbBComboBox = new QComboBox(assess_rgbGroupBox);
        assess_rgbBComboBox->setObjectName(QStringLiteral("assess_rgbBComboBox"));
        sizePolicy5.setHeightForWidth(assess_rgbBComboBox->sizePolicy().hasHeightForWidth());
        assess_rgbBComboBox->setSizePolicy(sizePolicy5);

        gridLayout_12->addWidget(assess_rgbBComboBox, 5, 0, 1, 1);

        assess_composeButton = new QPushButton(assess_rgbGroupBox);
        assess_composeButton->setObjectName(QStringLiteral("assess_composeButton"));
        sizePolicy1.setHeightForWidth(assess_composeButton->sizePolicy().hasHeightForWidth());
        assess_composeButton->setSizePolicy(sizePolicy1);

        gridLayout_12->addWidget(assess_composeButton, 6, 0, 1, 1);


        gridLayout->addWidget(assess_rgbGroupBox, 1, 0, 1, 1);

        groupBox_5 = new QGroupBox(assessmentTab);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        sizePolicy1.setHeightForWidth(groupBox_5->sizePolicy().hasHeightForWidth());
        groupBox_5->setSizePolicy(sizePolicy1);
        gridLayout_13 = new QGridLayout(groupBox_5);
        gridLayout_13->setSpacing(6);
        gridLayout_13->setContentsMargins(11, 11, 11, 11);
        gridLayout_13->setObjectName(QStringLiteral("gridLayout_13"));
        assess_ClearRef = new QPushButton(groupBox_5);
        assess_ClearRef->setObjectName(QStringLiteral("assess_ClearRef"));
        sizePolicy6.setHeightForWidth(assess_ClearRef->sizePolicy().hasHeightForWidth());
        assess_ClearRef->setSizePolicy(sizePolicy6);
        assess_ClearRef->setIcon(icon6);

        gridLayout_13->addWidget(assess_ClearRef, 0, 3, 1, 1);

        assess_LoadRef = new QPushButton(groupBox_5);
        assess_LoadRef->setObjectName(QStringLiteral("assess_LoadRef"));
        sizePolicy6.setHeightForWidth(assess_LoadRef->sizePolicy().hasHeightForWidth());
        assess_LoadRef->setSizePolicy(sizePolicy6);
        assess_LoadRef->setIcon(icon8);

        gridLayout_13->addWidget(assess_LoadRef, 0, 2, 1, 1);

        assess_LoadImButton = new QPushButton(groupBox_5);
        assess_LoadImButton->setObjectName(QStringLiteral("assess_LoadImButton"));
        sizePolicy6.setHeightForWidth(assess_LoadImButton->sizePolicy().hasHeightForWidth());
        assess_LoadImButton->setSizePolicy(sizePolicy6);
        assess_LoadImButton->setIcon(icon8);

        gridLayout_13->addWidget(assess_LoadImButton, 0, 0, 1, 1);

        assess_LoadSegOut = new QPushButton(groupBox_5);
        assess_LoadSegOut->setObjectName(QStringLiteral("assess_LoadSegOut"));
        sizePolicy6.setHeightForWidth(assess_LoadSegOut->sizePolicy().hasHeightForWidth());
        assess_LoadSegOut->setSizePolicy(sizePolicy6);
        assess_LoadSegOut->setIcon(icon8);

        gridLayout_13->addWidget(assess_LoadSegOut, 0, 1, 1, 1);


        gridLayout->addWidget(groupBox_5, 0, 0, 1, 2);

        groupBox_7 = new QGroupBox(assessmentTab);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        groupBox_7->setMinimumSize(QSize(0, 50));
        assess_metricLineEdit = new QLineEdit(groupBox_7);
        assess_metricLineEdit->setObjectName(QStringLiteral("assess_metricLineEdit"));
        assess_metricLineEdit->setEnabled(false);
        assess_metricLineEdit->setGeometry(QRect(10, 20, 71, 20));

        gridLayout->addWidget(groupBox_7, 2, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 3, 1, 1);

        assess_InfoImgLabel = new QLabel(assessmentTab);
        assess_InfoImgLabel->setObjectName(QStringLiteral("assess_InfoImgLabel"));
        sizePolicy2.setHeightForWidth(assess_InfoImgLabel->sizePolicy().hasHeightForWidth());
        assess_InfoImgLabel->setSizePolicy(sizePolicy2);
        assess_InfoImgLabel->setMinimumSize(QSize(90, 0));

        gridLayout->addWidget(assess_InfoImgLabel, 3, 0, 1, 1);

        tabWidget->addTab(assessmentTab, QString());
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        about_LogoLabel = new QLabel(tab);
        about_LogoLabel->setObjectName(QStringLiteral("about_LogoLabel"));
        about_LogoLabel->setGeometry(QRect(30, 30, 110, 90));
        sizePolicy1.setHeightForWidth(about_LogoLabel->sizePolicy().hasHeightForWidth());
        about_LogoLabel->setSizePolicy(sizePolicy1);
        about_LogoLabel->setMinimumSize(QSize(110, 90));
        about_LogoLabel->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/spt.png")));
        about_LogoLabel->setScaledContents(true);
        about_TextInfoLabel = new QTextEdit(tab);
        about_TextInfoLabel->setObjectName(QStringLiteral("about_TextInfoLabel"));
        about_TextInfoLabel->setEnabled(false);
        about_TextInfoLabel->setGeometry(QRect(180, 20, 351, 311));
        sizePolicy.setHeightForWidth(about_TextInfoLabel->sizePolicy().hasHeightForWidth());
        about_TextInfoLabel->setSizePolicy(sizePolicy);
        about_TextInfoLabel->setFocusPolicy(Qt::WheelFocus);
        about_TextInfoLabel->setAutoFillBackground(false);
        about_TextInfoLabel->setFrameShape(QFrame::NoFrame);
        about_TextInfoLabel->setFrameShadow(QFrame::Sunken);
        about_LogoPUCLabel = new QLabel(tab);
        about_LogoPUCLabel->setObjectName(QStringLiteral("about_LogoPUCLabel"));
        about_LogoPUCLabel->setGeometry(QRect(40, 140, 80, 140));
        sizePolicy1.setHeightForWidth(about_LogoPUCLabel->sizePolicy().hasHeightForWidth());
        about_LogoPUCLabel->setSizePolicy(sizePolicy1);
        about_LogoPUCLabel->setMinimumSize(QSize(80, 140));
        about_LogoPUCLabel->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/logo_puc.gif")));
        about_LogoPUCLabel->setScaledContents(true);
        label_3 = new QLabel(tab);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 310, 151, 16));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/icons/icons/lvc-logo.png")));
        label_3->setScaledContents(true);
        tabWidget->addTab(tab, QString());

        gridLayout_16->addWidget(tabWidget, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        QWidget::setTabOrder(ref_loadImButton, ref_loadRefButton);
        QWidget::setTabOrder(ref_loadRefButton, ref_saveRefButton);
        QWidget::setTabOrder(ref_saveRefButton, ref_clearRefButton);
        QWidget::setTabOrder(ref_clearRefButton, ref_zoomButton);
        QWidget::setTabOrder(ref_zoomButton, ref_panButton);
        QWidget::setTabOrder(ref_panButton, ref_createButton);
        QWidget::setTabOrder(ref_createButton, ref_editButton);
        QWidget::setTabOrder(ref_editButton, ref_deleteButton);
        QWidget::setTabOrder(ref_deleteButton, ref_rgbRComboBox);
        QWidget::setTabOrder(ref_rgbRComboBox, ref_rgbGComboBox);
        QWidget::setTabOrder(ref_rgbGComboBox, ref_rgbBComboBox);
        QWidget::setTabOrder(ref_rgbBComboBox, ref_composeButton);
        QWidget::setTabOrder(ref_composeButton, ref_ShowNumbersCheckBox);
        QWidget::setTabOrder(ref_ShowNumbersCheckBox, ref_SegmentsTable);
        QWidget::setTabOrder(ref_SegmentsTable, ref_SelectAllButton);
        QWidget::setTabOrder(ref_SelectAllButton, ref_ClearAllButton);
        QWidget::setTabOrder(ref_ClearAllButton, opt_optAlgComboBox);
        QWidget::setTabOrder(opt_optAlgComboBox, opt_optLineEdit1);
        QWidget::setTabOrder(opt_optLineEdit1, opt_optLineEdit2);
        QWidget::setTabOrder(opt_optLineEdit2, opt_optLineEdit3);
        QWidget::setTabOrder(opt_optLineEdit3, opt_optLineEdit4);
        QWidget::setTabOrder(opt_optLineEdit4, opt_optAdvComboBox);
        QWidget::setTabOrder(opt_optAdvComboBox, opt_segAlgComboBox);
        QWidget::setTabOrder(opt_segAlgComboBox, opt_segAlgComboBox3);
        QWidget::setTabOrder(opt_segAlgComboBox3, opt_segAlgComboBox2);
        QWidget::setTabOrder(opt_segAlgComboBox2, opt_segAlgComboBox4);
        QWidget::setTabOrder(opt_segAlgComboBox4, opt_segLineEditMin1);
        QWidget::setTabOrder(opt_segLineEditMin1, opt_segLineEditMax1);
        QWidget::setTabOrder(opt_segLineEditMax1, opt_segLineEditMin2);
        QWidget::setTabOrder(opt_segLineEditMin2, opt_segLineEditMax2);
        QWidget::setTabOrder(opt_segLineEditMax2, opt_segLineEditMin3);
        QWidget::setTabOrder(opt_segLineEditMin3, opt_segLineEditMax3);
        QWidget::setTabOrder(opt_segLineEditMax3, opt_segLineEditMin4);
        QWidget::setTabOrder(opt_segLineEditMin4, opt_segLineEditMax4);
        QWidget::setTabOrder(opt_segLineEditMax4, opt_segLineEditMin5);
        QWidget::setTabOrder(opt_segLineEditMin5, opt_segLineEditMax5);
        QWidget::setTabOrder(opt_segLineEditMax5, opt_segLineEditBands);
        QWidget::setTabOrder(opt_segLineEditBands, opt_fitComboBox);
        QWidget::setTabOrder(opt_fitComboBox, opt_segLineEditTimesToRun);
        QWidget::setTabOrder(opt_segLineEditTimesToRun, opt_outLineEdit);
        QWidget::setTabOrder(opt_outLineEdit, opt_outPushButton);
        QWidget::setTabOrder(opt_outPushButton, opt_runPushButton);
        QWidget::setTabOrder(opt_runPushButton, seg_segAlgComboBox);
        QWidget::setTabOrder(seg_segAlgComboBox, seg_segAlgComboBox3);
        QWidget::setTabOrder(seg_segAlgComboBox3, seg_segAlgComboBox2);
        QWidget::setTabOrder(seg_segAlgComboBox2, seg_segAlgComboBox4);
        QWidget::setTabOrder(seg_segAlgComboBox4, seg_segLineEdit1);
        QWidget::setTabOrder(seg_segLineEdit1, seg_segLineEdit2);
        QWidget::setTabOrder(seg_segLineEdit2, seg_segLineEdit3);
        QWidget::setTabOrder(seg_segLineEdit3, seg_segLineEdit4);
        QWidget::setTabOrder(seg_segLineEdit4, seg_segLineEdit5);
        QWidget::setTabOrder(seg_segLineEdit5, seg_segLineEditBands);
        QWidget::setTabOrder(seg_segLineEditBands, seg_LoadParamsFilePushButton);
        QWidget::setTabOrder(seg_LoadParamsFilePushButton, seg_outLineEdit);
        QWidget::setTabOrder(seg_outLineEdit, seg_runPushButton);
        QWidget::setTabOrder(seg_runPushButton, seg_outPushButton);
        QWidget::setTabOrder(seg_outPushButton, seg_metricsComboBox);
        QWidget::setTabOrder(seg_metricsComboBox, seg_refLoadPushButton);
        QWidget::setTabOrder(seg_refLoadPushButton, seg_clearRefButton);
        QWidget::setTabOrder(seg_clearRefButton, seg_refCalculatePushButton);
        QWidget::setTabOrder(seg_refCalculatePushButton, seg_metricLineEdit);
        QWidget::setTabOrder(seg_metricLineEdit, assess_LoadImButton);
        QWidget::setTabOrder(assess_LoadImButton, assess_LoadSegOut);
        QWidget::setTabOrder(assess_LoadSegOut, assess_LoadRef);
        QWidget::setTabOrder(assess_LoadRef, assess_ClearRef);
        QWidget::setTabOrder(assess_ClearRef, assess_rgbRComboBox);
        QWidget::setTabOrder(assess_rgbRComboBox, assess_rgbGComboBox);
        QWidget::setTabOrder(assess_rgbGComboBox, assess_rgbBComboBox);
        QWidget::setTabOrder(assess_rgbBComboBox, assess_composeButton);
        QWidget::setTabOrder(assess_composeButton, assess_metricsComboBox);
        QWidget::setTabOrder(assess_metricsComboBox, assess_Calculate);
        QWidget::setTabOrder(assess_Calculate, assess_metricLineEdit);
        QWidget::setTabOrder(assess_metricLineEdit, tabWidget);
        QWidget::setTabOrder(tabWidget, scrollArea);
        QWidget::setTabOrder(scrollArea, about_TextInfoLabel);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);
        opt_fitComboBox->setCurrentIndex(4);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        ref_rgbGroupBox->setTitle(QApplication::translate("MainWindow", "Bands selection", 0));
        ref_rgbRLabel->setText(QApplication::translate("MainWindow", "Red Band", 0));
        ref_rgbGLabel->setText(QApplication::translate("MainWindow", "Green Band", 0));
        ref_rgbBLabel->setText(QApplication::translate("MainWindow", "Blue Band", 0));
        ref_composeButton->setText(QApplication::translate("MainWindow", "Compose", 0));
        groupBox_2->setTitle(QString());
        ref_panButton->setText(QString());
        ref_createButton->setText(QString());
        ref_editButton->setText(QString());
        ref_deleteButton->setText(QString());
        groupBox->setTitle(QString());
        ref_clearRefButton->setText(QApplication::translate("MainWindow", "Clear References", 0));
#ifndef QT_NO_TOOLTIP
        ref_saveRefButton->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p><br/></p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        ref_saveRefButton->setText(QApplication::translate("MainWindow", "Save References", 0));
#ifndef QT_NO_TOOLTIP
        ref_loadImButton->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        ref_loadImButton->setText(QApplication::translate("MainWindow", "Load Image", 0));
#ifndef QT_NO_TOOLTIP
        ref_loadRefButton->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        ref_loadRefButton->setText(QApplication::translate("MainWindow", "Load Ref. Train", 0));
        ref_InfoImgLabel->setText(QString());
        ref_loadRefTestButton->setText(QApplication::translate("MainWindow", "Load Ref. Test", 0));
        groupBox_8->setTitle(QApplication::translate("MainWindow", "Segments", 0));
        ref_SelectAllButton->setText(QApplication::translate("MainWindow", "All", 0));
        ref_ClearAllButton->setText(QApplication::translate("MainWindow", "Clear", 0));
        ref_ShowNumbersCheckBox->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(referenceTab), QApplication::translate("MainWindow", "Reference Editor", 0));
        opt_segAlgGroupBox->setTitle(QApplication::translate("MainWindow", "Segmentation Algorithms", 0));
        opt_segLabel8->setText(QApplication::translate("MainWindow", "Format", 0));
        opt_segLabel1->setText(QApplication::translate("MainWindow", "Scale", 0));
        opt_segLabel5->setText(QApplication::translate("MainWindow", "ENL", 0));
        opt_segLabel3->setText(QApplication::translate("MainWindow", "Wcmpct", 0));
        opt_label1_3->setText(QApplication::translate("MainWindow", "Min.", 0));
        opt_segLineEditMin3->setText(QApplication::translate("MainWindow", "0.1", 0));
        opt_segLabel6->setText(QApplication::translate("MainWindow", "Img Model", 0));
        opt_segLabel7->setText(QApplication::translate("MainWindow", "Img Type", 0));
        opt_segAlgComboBox3->clear();
        opt_segAlgComboBox3->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Radar", 0)
         << QApplication::translate("MainWindow", "Optical", 0)
        );
        opt_segLabel4->setText(QApplication::translate("MainWindow", "N. Iter", 0));
        opt_segAlgComboBox4->clear();
        opt_segAlgComboBox4->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Amplitude", 0)
         << QApplication::translate("MainWindow", "Intensity", 0)
        );
        opt_segLineEditMax1->setText(QApplication::translate("MainWindow", "100", 0));
        opt_label1_4->setText(QApplication::translate("MainWindow", "Max.", 0));
        opt_segLineEditMin2->setText(QApplication::translate("MainWindow", "0.1", 0));
        opt_segAlgComboBox->clear();
        opt_segAlgComboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Multiresolution Region Growing", 0)
         << QApplication::translate("MainWindow", "Mean Shift", 0)
         << QApplication::translate("MainWindow", "SPRING", 0)
         << QApplication::translate("MainWindow", "Graph Based", 0)
         << QApplication::translate("MainWindow", "MultiSeg", 0)
        );
        opt_segLineEditMax3->setText(QApplication::translate("MainWindow", "0.9", 0));
        opt_segLineEditMax2->setText(QApplication::translate("MainWindow", "0.9", 0));
        opt_segLabel2->setText(QApplication::translate("MainWindow", "Wcolor", 0));
        opt_segAlgComboBox2->clear();
        opt_segAlgComboBox2->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Cartoon", 0)
        );
        opt_segLineEditMin1->setText(QApplication::translate("MainWindow", "10", 0));
        opt_segLabelBands->setText(QApplication::translate("MainWindow", "Bands", 0));
        opt_segLineEditBands->setText(QApplication::translate("MainWindow", "0,1", 0));
        opt_segLabel5_Coeff->setText(QApplication::translate("MainWindow", "Coeff. of \n"
"Variation", 0));
        opt_segLineEditMin5->setText(QApplication::translate("MainWindow", "0.2", 0));
        opt_segLineEditMax5->setText(QApplication::translate("MainWindow", "0.6", 0));
        opt_segLineEditMin6->setText(QApplication::translate("MainWindow", "2", 0));
        opt_segLineEditMax6->setText(QApplication::translate("MainWindow", "9", 0));
        opt_fitGroupBox->setTitle(QApplication::translate("MainWindow", "Fitness Function", 0));
        opt_segLabel9->setText(QApplication::translate("MainWindow", "Times to Run", 0));
        opt_fitComboBox->clear();
        opt_fitComboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Hoover Index", 0)
         << QApplication::translate("MainWindow", "Area-Fit-Index", 0)
         << QApplication::translate("MainWindow", "Shape Index", 0)
         << QApplication::translate("MainWindow", "Rand Index", 0)
         << QApplication::translate("MainWindow", "Precision & Recall", 0)
         << QApplication::translate("MainWindow", "Segmentation Covering", 0)
         << QApplication::translate("MainWindow", "RBSB", 0)
        );
        opt_outGroupBox->setTitle(QApplication::translate("MainWindow", "Output File", 0));
        opt_outLineEdit->setText(QApplication::translate("MainWindow", "OutputFile", 0));
        opt_outPushButton->setText(QApplication::translate("MainWindow", "Show Output", 0));
        opt_outLabel->setText(QApplication::translate("MainWindow", "Name", 0));
        opt_runPushButton->setText(QApplication::translate("MainWindow", "RUN", 0));
        opt_segLabelID->setText(QString());
        opt_segLabelIteration->setText(QString());
        opt_optAlgGroupBox->setTitle(QApplication::translate("MainWindow", "Optimization Algorithms", 0));
        opt_optAlgComboBox->clear();
        opt_optAlgComboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Differential Evolution", 0)
         << QApplication::translate("MainWindow", "Generalized Pattern Search", 0)
         << QApplication::translate("MainWindow", "Mesh Adaptive Direct Search", 0)
         << QApplication::translate("MainWindow", "Nelder-Mead Algorithm", 0)
        );
        opt_optLineEdit2->setText(QApplication::translate("MainWindow", "20", 0));
        opt_optLabel2->setText(QApplication::translate("MainWindow", "Population", 0));
        opt_optLineEdit1->setText(QApplication::translate("MainWindow", "3", 0));
        opt_optLineEdit4->setText(QApplication::translate("MainWindow", "1E-16", 0));
        opt_optLabel4->setText(QApplication::translate("MainWindow", "Min. Tol.", 0));
        opt_optLabel1->setText(QApplication::translate("MainWindow", "Dimensions", 0));
        opt_optLabel5->setText(QApplication::translate("MainWindow", "Strategy", 0));
        opt_optLineEdit3->setText(QApplication::translate("MainWindow", "10", 0));
        opt_optLabel3->setText(QApplication::translate("MainWindow", "Generation", 0));
        opt_optAdvComboBox->clear();
        opt_optAdvComboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "stBest1Exp", 0)
         << QApplication::translate("MainWindow", "stRand1Exp", 0)
         << QApplication::translate("MainWindow", "stRandToBest1Exp", 0)
         << QApplication::translate("MainWindow", "stBest2Exp", 0)
         << QApplication::translate("MainWindow", "stRand2Exp", 0)
         << QApplication::translate("MainWindow", "stBest1Bin", 0)
         << QApplication::translate("MainWindow", "stRand1Bin", 0)
         << QApplication::translate("MainWindow", "stRandToBest1Bin", 0)
         << QApplication::translate("MainWindow", "stBest2Bin", 0)
         << QApplication::translate("MainWindow", "stRand2Bin", 0)
        );
        tabWidget->setTabText(tabWidget->indexOf(optimizationTab), QApplication::translate("MainWindow", "Optimization", 0));
        groupBox_4->setTitle(QApplication::translate("MainWindow", "Segmentation Assessment", 0));
        seg_metricsComboBox->clear();
        seg_metricsComboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Hoover Index", 0)
         << QApplication::translate("MainWindow", "Area-Fit-Index", 0)
         << QApplication::translate("MainWindow", "Shape Index", 0)
         << QApplication::translate("MainWindow", "Rand Index", 0)
         << QApplication::translate("MainWindow", "Precision & Recall", 0)
         << QApplication::translate("MainWindow", "Segmentation Covering", 0)
         << QApplication::translate("MainWindow", "RBSB", 0)
        );
        seg_refCalculatePushButton->setText(QApplication::translate("MainWindow", "Calculate", 0));
        label_2->setText(QApplication::translate("MainWindow", "Value:", 0));
        seg_refLoadPushButton->setText(QApplication::translate("MainWindow", "Load References (raster or .shp)", 0));
        seg_clearRefButton->setText(QApplication::translate("MainWindow", "Clear References", 0));
        seg_segAlgGroupBox->setTitle(QApplication::translate("MainWindow", "Segmentation Algorithms", 0));
        seg_segLabel2->setText(QApplication::translate("MainWindow", "Wcolor", 0));
        seg_LoadParamsFilePushButton->setText(QApplication::translate("MainWindow", "Load Parameters File (.txt)", 0));
        seg_segAlgComboBox3->clear();
        seg_segAlgComboBox3->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Radar", 0)
         << QApplication::translate("MainWindow", "Optical", 0)
        );
        seg_segAlgComboBox2->clear();
        seg_segAlgComboBox2->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Cartoon", 0)
        );
        seg_segAlgComboBox4->clear();
        seg_segAlgComboBox4->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Amplitude", 0)
         << QApplication::translate("MainWindow", "Intensity", 0)
        );
        seg_segLabel6->setText(QApplication::translate("MainWindow", "Img Model", 0));
        seg_segLabel8->setText(QApplication::translate("MainWindow", "Format", 0));
        seg_segLabel7->setText(QApplication::translate("MainWindow", "Img Type", 0));
        seg_segAlgComboBox->clear();
        seg_segAlgComboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Multiresolution Region Growing", 0)
         << QApplication::translate("MainWindow", "Mean Shift", 0)
         << QApplication::translate("MainWindow", "SPRING", 0)
         << QApplication::translate("MainWindow", "Graph Based", 0)
         << QApplication::translate("MainWindow", "MultiSeg", 0)
        );
        seg_segLabel5->setText(QApplication::translate("MainWindow", "ENL", 0));
        seg_segLabel4->setText(QApplication::translate("MainWindow", "N. Iter", 0));
        seg_segLabel1->setText(QApplication::translate("MainWindow", "Scale", 0));
        seg_segLabel3->setText(QApplication::translate("MainWindow", "Wcmpct", 0));
        seg_segLabelBands->setText(QApplication::translate("MainWindow", "Bands", 0));
        seg_segLineEditBands->setText(QApplication::translate("MainWindow", "0,1", 0));
        seg_segLabel9->setText(QApplication::translate("MainWindow", "Coeff. of \n"
"Variation", 0));
        seg_segLineEdit9->setText(QApplication::translate("MainWindow", "0.6", 0));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Output File", 0));
        seg_runPushButton->setText(QApplication::translate("MainWindow", "RUN", 0));
        seg_outPushButton->setText(QApplication::translate("MainWindow", "Show Output", 0));
        label->setText(QApplication::translate("MainWindow", "Name", 0));
        seg_outLineEdit->setText(QApplication::translate("MainWindow", "OutputFile", 0));
        tabWidget->setTabText(tabWidget->indexOf(segmentationTab), QApplication::translate("MainWindow", "Segmentation", 0));
        groupBox_6->setTitle(QApplication::translate("MainWindow", "Metric", 0));
        assess_metricsComboBox->clear();
        assess_metricsComboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Hoover Index", 0)
         << QApplication::translate("MainWindow", "Area-Fit-Index", 0)
         << QApplication::translate("MainWindow", "Shape Index", 0)
         << QApplication::translate("MainWindow", "Rand Index", 0)
         << QApplication::translate("MainWindow", "Precision & Recall", 0)
         << QApplication::translate("MainWindow", "Segmentation Covering", 0)
         << QApplication::translate("MainWindow", "RBSB", 0)
        );
        assess_Calculate->setText(QApplication::translate("MainWindow", "Calculate", 0));
        assess_rgbGroupBox->setTitle(QApplication::translate("MainWindow", "Bands selection", 0));
        label_4->setText(QApplication::translate("MainWindow", "Red Band", 0));
        label_5->setText(QApplication::translate("MainWindow", "Green Band", 0));
        label_6->setText(QApplication::translate("MainWindow", "Blue Band", 0));
        assess_composeButton->setText(QApplication::translate("MainWindow", "Compose", 0));
        groupBox_5->setTitle(QString());
        assess_ClearRef->setText(QApplication::translate("MainWindow", "Clear References", 0));
        assess_LoadRef->setText(QApplication::translate("MainWindow", "Load Reference", 0));
        assess_LoadImButton->setText(QApplication::translate("MainWindow", "Load Image", 0));
        assess_LoadSegOut->setText(QApplication::translate("MainWindow", "Load Seg. Outcome", 0));
        groupBox_7->setTitle(QApplication::translate("MainWindow", "Result", 0));
        assess_InfoImgLabel->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(assessmentTab), QApplication::translate("MainWindow", "Assessment", 0));
        about_LogoLabel->setText(QString());
        about_LogoPUCLabel->setText(QString());
        label_3->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "About", 0));
        Q_UNUSED(MainWindow);
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
