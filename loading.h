#ifndef LOADING_H
#define LOADING_H

#include <QDialog>

namespace Ui {
class Loading;
}

class Loading : public QDialog
{
    Q_OBJECT

public:
    explicit Loading(QWidget *parent = 0);
    ~Loading();
public slots:
    void update_progress(int n);

private:
    Ui::Loading *ui;
};

#endif // LOADING_H
