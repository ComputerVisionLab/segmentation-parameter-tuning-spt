#ifndef OPTINFO_H
#define OPTINFO_H

#include <QString>
#include <QDebug>
class OptInfo
{
public:
    OptInfo();
    ~OptInfo();
    void copyFrom(OptInfo temp);
    void show();
    //Get Functions
    std::string get_out_name(void){return out_name;}
    QString get_opt_outLineEdit(void){return opt_outLineEdit;}
    QString get_initialImgFileName(void){return initialImgFileName;}
    QString get_initialRefFileName(void){return initialRefFileName;}
    int get_rows_ref(void){return rows_ref;}
    int get_cols_ref(void){return cols_ref;}
    int get_bands_ref(void){return bands_ref;}
    QString get_optAlgComboBox_text(void){return optAlgComboBox_text;}
    QString get_segAlgComboBox_text(void){return segAlgComboBox_text;}
    QString get_fitComboBox_text(void){return fitComboBox_text;}
    QString get_opt_segLabel1_text(void){return opt_segLabel1_text;}
    bool get_opt_segLabel1_bool(void){return opt_segLabel1_bool;}
    QString get_opt_segLabel2_text(void){return opt_segLabel2_text;}
    bool get_opt_segLabel2_bool(void){return opt_segLabel2_bool;}
    QString get_opt_segLabel3_text(void){return opt_segLabel3_text;}
    bool get_opt_segLabel3_bool(void){return opt_segLabel3_bool;}
    QString get_opt_segLabel4_text(void){return opt_segLabel4_text;}
    bool get_opt_segLabel4_bool(void){return opt_segLabel4_bool;}
    QString get_opt_segLabel5_text(void){return opt_segLabel5_text;}
    bool get_opt_segLabel5_bool(void){return opt_segLabel5_bool;}
    QString get_opt_segLabel6_text(void){return opt_segLabel6_text;}
    bool get_opt_segLabel6_bool(void){return opt_segLabel6_bool;}
    QString get_opt_segLabel7_text(void){return opt_segLabel7_text;}
    bool get_opt_segLabel7_bool(void){return opt_segLabel7_bool;}
    QString get_opt_segLabel8_text(void){return opt_segLabel8_text;}
    bool get_opt_segLabel8_bool(void){return opt_segLabel8_bool;}
    QString get_opt_segLabel5_Coeff_text(void){return opt_segLabel5_Coeff_text;}
    bool get_opt_segLabel5_Coeff_bool(void){return opt_segLabel5_Coeff_bool;}
    QString get_opt_segLineEdit5_text(void){return opt_segLineEdit5_text;}
    QString get_opt_segAlgComboBox2_text(void){return opt_segAlgComboBox2_text;}
    QString get_opt_segAlgComboBox3_text(void){return opt_segAlgComboBox3_text;}
    QString get_opt_segAlgComboBox4_text(void){return opt_segAlgComboBox4_text;}
    //Set Functions
    void set_out_name(std::string set_value){out_name = set_value;}
    void set_opt_outLineEdit(QString set_value){opt_outLineEdit = set_value;}
    void set_initialImgFileName(QString set_value){initialImgFileName = set_value;}
    void set_initialRefFileName(QString set_value){initialRefFileName = set_value;}
    void set_rows_ref(int set_value){rows_ref = set_value;}
    void set_cols_ref(int set_value){cols_ref = set_value;}
    void set_bands_ref(int set_value){bands_ref = set_value;}
    void set_optAlgComboBox_text(QString set_value){optAlgComboBox_text = set_value;}
    void set_segAlgComboBox_text(QString set_value){segAlgComboBox_text = set_value;}
    void set_fitComboBox_text(QString set_value){fitComboBox_text = set_value;}
    //Set Labels with its Text and State (bool) together
    void set_opt_segLabel1(QString set_value_text, bool set_value_bool){opt_segLabel1_text = set_value_text; opt_segLabel1_bool = set_value_bool;}
    void set_opt_segLabel2(QString set_value_text, bool set_value_bool){opt_segLabel2_text = set_value_text; opt_segLabel2_bool = set_value_bool;}
    void set_opt_segLabel3(QString set_value_text, bool set_value_bool){opt_segLabel3_text = set_value_text; opt_segLabel3_bool = set_value_bool;}
    void set_opt_segLabel4(QString set_value_text, bool set_value_bool){opt_segLabel4_text = set_value_text; opt_segLabel4_bool = set_value_bool;}
    void set_opt_segLabel5(QString set_value_text, bool set_value_bool){opt_segLabel5_text = set_value_text; opt_segLabel5_bool = set_value_bool;}
    void set_opt_segLabel6(QString set_value_text, bool set_value_bool){opt_segLabel6_text = set_value_text; opt_segLabel6_bool = set_value_bool;}
    void set_opt_segLabel7(QString set_value_text, bool set_value_bool){opt_segLabel7_text = set_value_text; opt_segLabel7_bool = set_value_bool;}
    void set_opt_segLabel8(QString set_value_text, bool set_value_bool){opt_segLabel8_text = set_value_text; opt_segLabel8_bool = set_value_bool;}
    void set_opt_segLabel5_Coeff(QString set_value_text, bool set_value_bool){opt_segLabel5_Coeff_text = set_value_text; opt_segLabel5_Coeff_bool = set_value_bool;}
    void set_opt_segLineEdit5_text(QString set_value_text){opt_segLineEdit5_text = set_value_text;}
    void set_opt_segAlgComboBox2_text(QString set_value_text){opt_segAlgComboBox2_text = set_value_text;}
    void set_opt_segAlgComboBox3_text(QString set_value_text){opt_segAlgComboBox3_text = set_value_text;}
    void set_opt_segAlgComboBox4_text(QString set_value_text){opt_segAlgComboBox4_text = set_value_text;}
    //How many times to run an experiment
    int get_times_to_run(void){return times_to_run;}
    void set_times_to_run(int set_value){times_to_run = set_value;}
    QString get_version(void){return version;}
    void set_version(QString set_value){version = set_value;}
    //Bands to be processed by MultiSeg Algorithm
    QString get_bands_string(void){return opt_segBands;}
    void set_bands_string(QString set_value){opt_segBands = set_value;}
    //Reference for testing
    QString get_refImTestFileName(void){return refImTestFileName;}
    void set_refImTestFileName(QString set_value){refImTestFileName = set_value;}


protected:
    std::string out_name;//Output name given by the user
    QString opt_outLineEdit;//Output name of Segmentation Outcome
    QString initialImgFileName;//Initial name of input image
    QString initialRefFileName;//Initial name of reference given by the user
    int rows_ref;//number of rows of input image
    int cols_ref;//number of cols of input image
    int bands_ref;//number of bands of input image
    QString optAlgComboBox_text;//Current text in the Optimization Algorithms Combo Box
    QString segAlgComboBox_text;//Current text in the Segmentation Algorithms Combo Box
    QString fitComboBox_text;//Current text in the Fitness Functions Combo Box
    QString opt_segLabel1_text;bool opt_segLabel1_bool;//Current Text and state of label 1
    QString opt_segLabel2_text;bool opt_segLabel2_bool;//Current Text and state of label 2
    QString opt_segLabel3_text;bool opt_segLabel3_bool;//Current Text and state of label 3
    QString opt_segLabel4_text;bool opt_segLabel4_bool;//Current Text and state of label 4
    QString opt_segLabel5_text;bool opt_segLabel5_bool;//Current Text and state of label 5
    QString opt_segLabel6_text;bool opt_segLabel6_bool;//Current Text and state of label 6
    QString opt_segLabel7_text;bool opt_segLabel7_bool;//Current Text and state of label 7
    QString opt_segLabel8_text;bool opt_segLabel8_bool;//Current Text and state of label 8
    QString opt_segLabel5_Coeff_text;bool opt_segLabel5_Coeff_bool;//Current Text and state of label 5 Coeff
    QString opt_segLineEdit5_text;//Current Text of line edit 5
    QString opt_segAlgComboBox2_text;//Current text of Combo Box 2
    QString opt_segAlgComboBox3_text;//Current text of Combo Box 3
    QString opt_segAlgComboBox4_text;//Current text of Combo Box 4
    int times_to_run;//Time to run the experiment
    QString version;//Current version of SPT
    QString opt_segBands;
    QString refImTestFileName;
};

#endif // OPTINFO_H
