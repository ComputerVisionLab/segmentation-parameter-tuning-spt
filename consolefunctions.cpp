#include "consolefunctions.h"
#include <QFile>
#include <QTextStream>

ExperimentData::ExperimentData()
{
    InputImage          = " ";
    TrainingReference   = " ";
    TestingReference    = " ";
    OptimizationAlgorithm = " ";
    SegmentationAlgorithm = " ";
    SegParameterFixed1    = 0;
    SegParameterFixed2    = " ";
    ImageType   =   0;
    ImageModel  =   0;
    Format      =   0;
    Metric      =   0;
    TimesToRun  =   0;
    OutputFileName = " ";
    thread_console - new QThread;
}

void ExperimentData::show()
{
    std::cout << "InputImage              :  " << InputImage.toStdString().c_str()              << std::endl;
    std::cout << "TrainingReference       :  " << TrainingReference.toStdString().c_str()       << std::endl;
    std::cout << "TestingReference        :  " << TestingReference.toStdString().c_str()        << std::endl;
    std::cout << "OptimizationAlgorithm   :  " << OptimizationAlgorithm.toStdString().c_str()   << std::endl;
    std::cout << "OptimizationParameters  :  ";   display_vector(OptimizationParameters);
    std::cout << "SegmentationAlgorithm   :  " << SegmentationAlgorithm.toStdString().c_str()   << std::endl;
    std::cout << "SegParameter1MinMax     :  ";   display_vector(SegParameter1MinMax );
    std::cout << "SegParameter2MinMax     :  ";   display_vector(SegParameter2MinMax );
    std::cout << "SegParameter3MinMax     :  ";   display_vector(SegParameter3MinMax );
    std::cout << "SegParameter4MinMax     :  ";   display_vector(SegParameter4MinMax );
    std::cout << "SegParameter5MinMax     :  ";   display_vector(SegParameter5MinMax );
    std::cout << "SegParameterFixed1      :  " << SegParameterFixed1<< std::endl;
    std::cout << "SegParameterFixed2      :  " << SegParameterFixed2.toStdString().c_str()      <<  std::endl;
    std::cout << "ImageType               :  " << ImageType         << std::endl;
    std::cout << "ImageModel              :  " << ImageModel        << std::endl;
    std::cout << "Format                  :  " << Format            << std::endl;
    std::cout << "Metric                  :  " << Metric            << std::endl;
    std::cout << "TimesToRun              :  " << TimesToRun        << std::endl;
    std::cout << "OutputFileName          :  " << OutputFileName.toStdString().c_str()          << "\n\n" <<std::endl;
    std::cout << "Processing..." << std::endl;
}

void display_vector(const std::vector<double> &v)
{
    std::cout << "[";
    std::copy(v.begin(), v.end(),std::ostream_iterator<double>(std::cout, " "));
    std::cout << "]\n";
    return;
}

ExperimentData ReadConfigurationFIle (const char* filename)
{
    ExperimentData ConfigurationData;

    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly))
    {
        std::cout << "Error opening configuration file." << std::endl;
    }
    else
    {
        QTextStream iter(&file);
        while(!iter.atEnd())
        {
            QString line = iter.readLine();
            QStringList fields = line.split(" ");

            if (line.contains("InputImage"))                {   ConfigurationData.InputImage            =  fields.at(2);    }
            if (line.contains("TrainingReference"))         {   ConfigurationData.TrainingReference     =  fields.at(2);    }
            if (line.contains("TestingReference"))          {   ConfigurationData.TestingReference      =  fields.at(2);    }
            if (line.contains("OptimizationAlgorithm"))     {   ConfigurationData.OptimizationAlgorithm =  fields.at(2);    }

            if (line.contains("OptimizationParameters"))
            {
                QStringList Parts       = fields.at(2).split(",");
                int NumberOfParameters  = Parts.length();
                for (int i = 0 ; i < NumberOfParameters ; ++i)
                    ConfigurationData.OptimizationParameters.push_back(Parts.at(i).toDouble());
            }

            if (line.contains("SegmentationAlgorithm"))     {   ConfigurationData.SegmentationAlgorithm =   fields.at(2);    }

            if (line.contains("SegParameter1MinMax"))
            {
                QStringList Parts       = fields.at(2).split(",");
                int NumberOfParameters  = Parts.length();
                for (int i = 0 ; i < NumberOfParameters ; ++i)
                    ConfigurationData.SegParameter1MinMax.push_back(Parts.at(i).toDouble());
            }

            if (line.contains("SegParameter2MinMax"))
            {
                QStringList Parts       = fields.at(2).split(",");
                int NumberOfParameters  = Parts.length();
                for (int i = 0 ; i < NumberOfParameters ; ++i)
                    ConfigurationData.SegParameter2MinMax.push_back(Parts.at(i).toDouble());
            }

            if (line.contains("SegParameter3MinMax"))
            {
                QStringList Parts       = fields.at(2).split(",");
                int NumberOfParameters  = Parts.length();
                for (int i = 0 ; i < NumberOfParameters ; ++i)
                    ConfigurationData.SegParameter3MinMax.push_back(Parts.at(i).toDouble());
            }

            if (line.contains("SegParameter4MinMax"))
            {
                QStringList Parts       = fields.at(2).split(",");
                int NumberOfParameters  = Parts.length();
                for (int i = 0 ; i < NumberOfParameters ; ++i)
                    ConfigurationData.SegParameter4MinMax.push_back(Parts.at(i).toDouble());
            }

            if (line.contains("SegParameter5MinMax"))
            {
                QStringList Parts       = fields.at(2).split(",");
                int NumberOfParameters  = Parts.length();
                for (int i = 0 ; i < NumberOfParameters ; ++i)
                    ConfigurationData.SegParameter5MinMax.push_back(Parts.at(i).toDouble());
            }

            if (line.contains("SegParameterFixed1"))        {   ConfigurationData.SegParameterFixed1    =       fields.at(2).toInt();    }
            if (line.contains("SegParameterFixed2"))        {   ConfigurationData.SegParameterFixed2    =       fields.at(2);    }
            if (line.contains("ImageType"))                 {   ConfigurationData.ImageType             =       fields.at(2).toInt();    }
            if (line.contains("ImageModel"))                {   ConfigurationData.ImageModel            =       fields.at(2).toInt();    }
            if (line.contains("Format"))                    {   ConfigurationData.Format                =       fields.at(2).toInt();    }
            if (line.contains("Metric"))                    {   ConfigurationData.Metric                =       fields.at(2).toInt();    }
            if (line.contains("TimesToRun"))                {   ConfigurationData.TimesToRun            =       fields.at(2).toInt();    }
            if (line.contains("OutputFileName"))            {   ConfigurationData.OutputFileName        =       fields.at(2);    }
        }
    }
    return ConfigurationData;
}

QString Get_TestingReferenceName(ExperimentData ConfigurationData)
{
    QString fileName = ConfigurationData.TestingReference.toStdString().c_str();
    QString TestingReferenceName;
    QFileInfo fileNameInfo(ConfigurationData.InputImage);
    QString tfwName = fileNameInfo.absolutePath() + "/" +
                      fileNameInfo.baseName() + ".tfw";

    QFile tempFile(tfwName);
    bool is_twfFile = tempFile.open(QIODevice::ReadOnly);
    std::vector<std::vector<cv::Point2f> > ui_refTestVectorPoints;
    int ui_rows_ref, ui_cols_ref, ui_bands_ref;
    ReadImg_Info(ConfigurationData.InputImage.toStdString().c_str(), &ui_rows_ref, &ui_cols_ref, &ui_bands_ref);

    if(fileName.contains(".shp"))//if a shapefile was loaded
    {
        if(is_twfFile)
        {
            double *twfValues = new double[8];
            ui_refTestVectorPoints = readShapeFile(fileName.toStdString(),tfwName,"./temp/test_temp.shp",ui_rows_ref,twfValues);
            TestingReferenceName = "./temp/test_temp.shp";
        }
        else
        {
            TestingReferenceName = ConfigurationData.TestingReference.toStdString().c_str();
        }
    }
    else
    {
        std::cout << "Error with Training Reference.\nOnly ESRI Shapefiles are accepted (*.shp)\n";
    }
    return TestingReferenceName;
}

OptInfo FillGlobalInfo(ExperimentData ConfigurationData)
{
    OptInfo GlobalInfo;

    string ui_out_name;
    if(ConfigurationData.SegmentationAlgorithm == "Multiseg")
    {
       ui_out_name = QString("./temp/").toStdString() + ConfigurationData.OutputFileName.toStdString() + ".jpg";
    }
    else
    {
       ui_out_name = QString("./temp/").toStdString() + ConfigurationData.OutputFileName.toStdString() + ".png";
    }

    GlobalInfo.set_out_name(ui_out_name);//Temporal output name, located in TEMP folder
    GlobalInfo.set_opt_outLineEdit(ConfigurationData.OutputFileName);//Name of output Image
    GlobalInfo.set_initialImgFileName(ConfigurationData.InputImage);//Original Name of Input Image
    GlobalInfo.set_initialRefFileName(ConfigurationData.TrainingReference);//Original Name of Reference
    GlobalInfo.set_refImTestFileName(Get_TestingReferenceName(ConfigurationData));//Reference for Testing

    int ui_rows_ref, ui_cols_ref, ui_bands_ref;
    ReadImg_Info(ConfigurationData.InputImage.toStdString().c_str(), &ui_rows_ref, &ui_cols_ref, &ui_bands_ref);
    GlobalInfo.set_rows_ref(ui_rows_ref);//Number of rows of the image
    GlobalInfo.set_cols_ref(ui_cols_ref);//Number of cols of the image
    GlobalInfo.set_bands_ref(ui_bands_ref);//Number of bands of the image

    GlobalInfo.set_optAlgComboBox_text(ConfigurationData.OptimizationAlgorithm);//Optimization Algorithm Selected
    GlobalInfo.set_segAlgComboBox_text(ConfigurationData.SegmentationAlgorithm);//Segmentation Algorithm Selected
    QString metricName;
    int metricIndex = ConfigurationData.Metric;
    switch (metricIndex) {
    case 0:
        metricName = "Hoover Index";break;
    case 1:
        metricName = "Area-Fit-Index";break;
    case 2:
        metricName = "Shape Index";break;
    case 3:
        metricName = "Rand Index";break;
    case 4:
        metricName = "Precision & Recall";break;
    case 5:
        metricName = "Segmentation Covering";break;
    case 6:
        metricName = "RBSB";break;
    default:
        break;
    }
    GlobalInfo.set_fitComboBox_text(metricName);//Metric Selected

    //Copying text and current state of Labels from 1 to 8
    if (ConfigurationData.SegmentationAlgorithm == "MRG")
    {
        GlobalInfo.set_opt_segLabel1(QString("Scale")       ,false);
        GlobalInfo.set_opt_segLabel2(QString("W_color")     ,false);
        GlobalInfo.set_opt_segLabel3(QString("W_compct")    ,false);
        GlobalInfo.set_opt_segLabel4(QString("4")           ,true);
        GlobalInfo.set_opt_segLabel5(QString("5")           ,true);
        GlobalInfo.set_opt_segLabel6(QString("ImageModel")  ,true);
        GlobalInfo.set_opt_segLabel7(QString("ImageType")   ,true);
        GlobalInfo.set_opt_segLabel8(QString("Format")      ,true);
        GlobalInfo.set_opt_segLabel5_Coeff(QString("SegParameter5"),true);
        GlobalInfo.set_opt_segLineEdit5_text(QString::number(ConfigurationData.SegParameterFixed1));
    }
    else if (ConfigurationData.SegmentationAlgorithm == "MS")
    {
        GlobalInfo.set_opt_segLabel1(QString("Spat.Radius") ,false);
        GlobalInfo.set_opt_segLabel2(QString("RangeRadius") ,false);
        GlobalInfo.set_opt_segLabel3(QString("Min.Area")     ,false);
        GlobalInfo.set_opt_segLabel4(QString("4")           ,true);
        GlobalInfo.set_opt_segLabel5(QString("5")           ,true);
        GlobalInfo.set_opt_segLabel6(QString("ImageModel")  ,true);
        GlobalInfo.set_opt_segLabel7(QString("ImageType")   ,true);
        GlobalInfo.set_opt_segLabel8(QString("Format")      ,true);
        GlobalInfo.set_opt_segLabel5_Coeff(QString("SegParameter5"),true);
        GlobalInfo.set_opt_segLineEdit5_text(QString::number(ConfigurationData.SegParameterFixed1));
    }
    else if (ConfigurationData.SegmentationAlgorithm == "SPRING")
    {
        GlobalInfo.set_opt_segLabel1(QString("Min.Distance"),false);
        GlobalInfo.set_opt_segLabel2(QString("Min.Area")    ,false);
        GlobalInfo.set_opt_segLabel3(QString("3")           ,true);
        GlobalInfo.set_opt_segLabel4(QString("4")           ,true);
        GlobalInfo.set_opt_segLabel5(QString("5")           ,true);
        GlobalInfo.set_opt_segLabel6(QString("ImageModel")  ,true);
        GlobalInfo.set_opt_segLabel7(QString("ImageType")   ,true);
        GlobalInfo.set_opt_segLabel8(QString("Format")      ,true);
        GlobalInfo.set_opt_segLabel5_Coeff(QString("SegParameter5"),true);
        GlobalInfo.set_opt_segLineEdit5_text(QString::number(ConfigurationData.SegParameterFixed1));
    }
    else if (ConfigurationData.SegmentationAlgorithm == "Graph")
    {
        GlobalInfo.set_opt_segLabel1(QString("Std.Deviation"),false);
        GlobalInfo.set_opt_segLabel2(QString("Threshold")    ,false);
        GlobalInfo.set_opt_segLabel3(QString("Min.Area")     ,false);
        GlobalInfo.set_opt_segLabel4(QString("4")            ,true);
        GlobalInfo.set_opt_segLabel5(QString("5")            ,true);
        GlobalInfo.set_opt_segLabel6(QString("ImageModel")   ,true);
        GlobalInfo.set_opt_segLabel7(QString("ImageType")    ,true);
        GlobalInfo.set_opt_segLabel8(QString("Format")       ,true);
        GlobalInfo.set_opt_segLabel5_Coeff(QString("SegParameter5"),true);
        GlobalInfo.set_opt_segLineEdit5_text(QString::number(ConfigurationData.SegParameterFixed1));
    }
    else if (ConfigurationData.SegmentationAlgorithm == "Multiseg")
    {
        GlobalInfo.set_opt_segLabel1(QString("N(Lavels)")       ,false);
        GlobalInfo.set_opt_segLabel2(QString("Similarity")      ,false);
        GlobalInfo.set_opt_segLabel3(QString("Conf.Level")      ,false);
        GlobalInfo.set_opt_segLabel4(QString("Min.Area")            ,false);
        if(ConfigurationData.ImageType == 0)
        {
            GlobalInfo.set_opt_segLabel5(QString("ENL") ,false);
            GlobalInfo.set_opt_segLabel5_Coeff(QString("Coeff.Variation"),true);
        }
        else
        {
            GlobalInfo.set_opt_segLabel5(QString("Coeff.Variation"),false);
            GlobalInfo.set_opt_segLabel5_Coeff(QString("Coeff.Variation"),false);
        }
        GlobalInfo.set_opt_segLabel6(QString("ImageModel")   ,false);
        GlobalInfo.set_opt_segLabel7(QString("ImageType")    ,false);
        GlobalInfo.set_opt_segLabel8(QString("Format")       ,false);

        GlobalInfo.set_opt_segLineEdit5_text(QString::number(ConfigurationData.SegParameterFixed1));
    }

    GlobalInfo.set_opt_segAlgComboBox2_text(QString::number(ConfigurationData.ImageModel));
    GlobalInfo.set_opt_segAlgComboBox3_text(QString::number(ConfigurationData.ImageType));
    GlobalInfo.set_opt_segAlgComboBox4_text(QString::number(ConfigurationData.Format));
    GlobalInfo.set_times_to_run(ConfigurationData.TimesToRun);
    //Copy SPT version for Final Report
    GlobalInfo.set_version(QString("3.9 Command WIndow Version"));
    //Bands to be processed
    GlobalInfo.set_bands_string(ConfigurationData.SegParameterFixed2);
    return GlobalInfo;
}

int GetnDim(ExperimentData ConfigurationData)
{
    int ui_nDim;
    if (ConfigurationData.SegmentationAlgorithm == "MRG")
    {
        ui_nDim = 3;
    }
    else if (ConfigurationData.SegmentationAlgorithm == "MS")
    {
        ui_nDim = 3;
    }
    else if (ConfigurationData.SegmentationAlgorithm == "SPRING")
    {
        ui_nDim = 2;
    }
    else if (ConfigurationData.SegmentationAlgorithm == "Graph")
    {
        ui_nDim = 3;
    }
    else if (ConfigurationData.SegmentationAlgorithm == "Multiseg")
    {
        ui_nDim = 5;
//        if (ConfigurationData.ImageType == 0)       { ui_nDim = 4;    } //Radar
//        else if (ConfigurationData.ImageType == 1)  { ui_nDim = 5;    } //Optical
    }
    else
    {
        std::cout << "Wrong Segmentation Algorihtm\Segmentation Algorithms accepted: MRG, MS, SPRING, Graph, Multiseg\n";
    }
    return ui_nDim;
}

double* Getui_min(ExperimentData ConfigurationData, int ui_nDim)
{
    double*ui_min = new double[ui_nDim];

    if (ConfigurationData.SegmentationAlgorithm == "MRG")
    {
        ui_min[0] = ConfigurationData.SegParameter1MinMax.at(0);
        ui_min[1] = ConfigurationData.SegParameter2MinMax.at(0);
        ui_min[2] = ConfigurationData.SegParameter3MinMax.at(0);
    }
    else if (ConfigurationData.SegmentationAlgorithm == "MS")
    {
        ui_min[0] = ConfigurationData.SegParameter1MinMax.at(0);
        ui_min[1] = ConfigurationData.SegParameter2MinMax.at(0);
        ui_min[2] = ConfigurationData.SegParameter3MinMax.at(0);
    }
    else if (ConfigurationData.SegmentationAlgorithm == "SPRING")
    {
        ui_min[0] = ConfigurationData.SegParameter1MinMax.at(0);
        ui_min[1] = ConfigurationData.SegParameter2MinMax.at(0);
    }
    else if (ConfigurationData.SegmentationAlgorithm == "Graph")
    {
        ui_min[0] = ConfigurationData.SegParameter1MinMax.at(0);
        ui_min[1] = ConfigurationData.SegParameter2MinMax.at(0);
        ui_min[2] = ConfigurationData.SegParameter3MinMax.at(0);
    }
    else if (ConfigurationData.SegmentationAlgorithm == "Multiseg")
    {
        ui_min[0] = ConfigurationData.SegParameter1MinMax.at(0);
        ui_min[1] = ConfigurationData.SegParameter2MinMax.at(0);

        double min3 = ConfigurationData.SegParameter3MinMax.at(0);
        if(min3==0.80)
        {ui_min[2]=1;}
        else if(min3==0.85)
        {ui_min[2]=2;}
        else if(min3==0.90)
        {ui_min[2]=3;}
        else if(min3==0.95)
        {ui_min[2]=4;}
        else if(min3==0.99)
        {ui_min[2]=5;}
        else if(min3==0.995)
        {ui_min[2]=6;}
        else if(min3==0.999)
        {ui_min[2]=7;}
        else if(min3==1)
        {ui_min[2]=8;}

        ui_min[3] = ConfigurationData.SegParameter4MinMax.at(0);
        ui_min[4] = ConfigurationData.SegParameter5MinMax.at(0);
    }

    return ui_min;
}

double* Getui_max(ExperimentData ConfigurationData, int ui_nDim)
{
    double*ui_max = new double[ui_nDim];

    if (ConfigurationData.SegmentationAlgorithm == "MRG")
    {
        ui_max[0] = ConfigurationData.SegParameter1MinMax.at(1);
        ui_max[1] = ConfigurationData.SegParameter2MinMax.at(1);
        ui_max[2] = ConfigurationData.SegParameter3MinMax.at(1);
    }
    else if (ConfigurationData.SegmentationAlgorithm == "MS")
    {
        ui_max[0] = ConfigurationData.SegParameter1MinMax.at(1);
        ui_max[1] = ConfigurationData.SegParameter2MinMax.at(1);
        ui_max[2] = ConfigurationData.SegParameter3MinMax.at(1);
    }
    else if (ConfigurationData.SegmentationAlgorithm == "SPRING")
    {
        ui_max[0] = ConfigurationData.SegParameter1MinMax.at(1);
        ui_max[1] = ConfigurationData.SegParameter2MinMax.at(1);
    }
    else if (ConfigurationData.SegmentationAlgorithm == "Graph")
    {
        ui_max[0] = ConfigurationData.SegParameter1MinMax.at(1);
        ui_max[1] = ConfigurationData.SegParameter2MinMax.at(1);
        ui_max[2] = ConfigurationData.SegParameter3MinMax.at(1);
    }
    else if (ConfigurationData.SegmentationAlgorithm == "Multiseg")
    {
        ui_max[0] = ConfigurationData.SegParameter1MinMax.at(1);
        ui_max[1] = ConfigurationData.SegParameter2MinMax.at(1);

        double max3 = ConfigurationData.SegParameter3MinMax.at(1);
        if(max3==0.80)
        {ui_max[2]=1;}
        else if(max3==0.85)
        {ui_max[2]=2;}
        else if(max3==0.90)
        {ui_max[2]=3;}
        else if(max3==0.95)
        {ui_max[2]=4;}
        else if(max3==0.99)
        {ui_max[2]=5;}
        else if(max3==0.995)
        {ui_max[2]=6;}
        else if(max3==0.999)
        {ui_max[2]=7;}

        ui_max[3] = ConfigurationData.SegParameter4MinMax.at(1);
        ui_max[4] = ConfigurationData.SegParameter5MinMax.at(1);
    }

    return ui_max;
}

std::vector<std::vector<cv::Point2f> > Getui_refVectorPoints(ExperimentData &ConfigurationData)
{
    std::vector<std::vector<cv::Point2f> > ui_refVectorPoints;
    //QFileInfo to the name of original image
    QString filename = ConfigurationData.TrainingReference;
    QFileInfo fileNameInfo(ConfigurationData.InputImage);
    QString tfwName = fileNameInfo.absolutePath() + "/" +
                      fileNameInfo.baseName() + ".tfw";

    int ui_rows_ref, ui_cols_ref, ui_bands_ref;
    ReadImg_Info(ConfigurationData.InputImage.toStdString().c_str(), &ui_rows_ref, &ui_cols_ref, &ui_bands_ref);

    QFile tempFile(tfwName);
    bool is_twfFile;
    is_twfFile = tempFile.open(QIODevice::ReadOnly);

    if(filename.contains(".shp"))//if a shapefile was loaded
    {
        if(is_twfFile)
        {
            double *twfValues =  new double[8];
            ui_refVectorPoints = readShapeFile(filename.toStdString(),tfwName,"./temp/abs_temp.shp",ui_rows_ref,twfValues);
            ConfigurationData.TrainingReference = "./temp/abs_temp.shp";
            ConfigurationData.is_twfFile = true;
            for (int j = 0 ; j <= 7 ; j++)
            {
                ConfigurationData.twfValues.push_back(twfValues[j]);
            }
            return ui_refVectorPoints;
         }
        else
        {
            ui_refVectorPoints = readShapeFile(filename.toStdString());
            ConfigurationData.is_twfFile = false;
            return ui_refVectorPoints;
        }
    }
    else
    {
        std::cout << "Error with Training Reference.\nOnly ESRI Shapefiles are accepted (*.shp)\n";
    }    
}

int Getui_segmentationIndex(ExperimentData ConfigurationData)
{
    int ui_segmentationIndex;

    if (ConfigurationData.SegmentationAlgorithm == "MRG")
    {
        ui_segmentationIndex = 0;
    }
    else if (ConfigurationData.SegmentationAlgorithm == "MS")
    {
        ui_segmentationIndex = 1;
    }
    else if (ConfigurationData.SegmentationAlgorithm == "SPRING")
    {
        ui_segmentationIndex = 2;
    }
    else if (ConfigurationData.SegmentationAlgorithm == "Graph")
    {
        ui_segmentationIndex = 3;
    }
    else if (ConfigurationData.SegmentationAlgorithm == "Multiseg")
    {
        ui_segmentationIndex = 4;
    }
    return ui_segmentationIndex;
}

void Get_uiFixed_uinDimFixed(double *&ui_fixed, int &ui_nDimFixed, ExperimentData ConfigurationData)
{
    if (ConfigurationData.is_twfFile)
    {
        qDebug() << "There is TWF File";
        ui_nDimFixed = 13;
        ui_fixed[0] = ConfigurationData.ImageModel; //Image Model
        ui_fixed[1] = ConfigurationData.ImageType; //Image Type
        ui_fixed[2] = ConfigurationData.Format; //Radar Format
        ui_fixed[3] = ConfigurationData.SegParameterFixed1; //Number of equivalent looks: n_elo
        ui_fixed[4] = 1;//Indicates that there is TFW file
        ui_fixed[5] =   ConfigurationData.twfValues[0];// x pixel resolution
        ui_fixed[6] =   ConfigurationData.twfValues[1];// rotational component x
        ui_fixed[7] =   ConfigurationData.twfValues[2];// rotational component y
        ui_fixed[8] =   ConfigurationData.twfValues[3];// y pixel resolution
        ui_fixed[9] =   ConfigurationData.twfValues[4];// Upper Left East coordinate
        ui_fixed[10] =  ConfigurationData.twfValues[5];// Upper Left North coordinate
        ui_fixed[11] =  ConfigurationData.twfValues[6];// rest_x
        ui_fixed[12] =  ConfigurationData.twfValues[7];// rest_y
    }
    else
    {
        qDebug() << "There is not TWF File";
        ui_nDimFixed = 5;
        ui_fixed[0] = ConfigurationData.ImageModel; //Image Model
        ui_fixed[1] = ConfigurationData.ImageType; //Image Type
        ui_fixed[2] = ConfigurationData.Format; //Radar Format
        ui_fixed[3] = ConfigurationData.SegParameterFixed1; //Number of equivalent looks: n_elo
        ui_fixed[4] = 0;//Indicates that there is TFW file
    }
}

void RunExperiment(ExperimentData ConfigurationData)
{
    OptInfo GlobalInfo = FillGlobalInfo(ConfigurationData);
//    GlobalInfo.show();

    //Reinterpret general parameters
    int ui_nDim     =   GetnDim(ConfigurationData);
    double *ui_min  =   Getui_min(ConfigurationData, ui_nDim);
    double *ui_max  =   Getui_max(ConfigurationData, ui_nDim);
    int ui_rows_ref, ui_cols_ref, ui_bands_ref;
    ReadImg_Info(ConfigurationData.InputImage.toStdString().c_str(), &ui_rows_ref, &ui_cols_ref, &ui_bands_ref);

    QString orgImFileName = ConfigurationData.InputImage;
    QString ui_out_name =   GlobalInfo.get_out_name().c_str();

    std::vector<std::vector<cv::Point2f> > ui_refVectorPoints = Getui_refVectorPoints(ConfigurationData);
    int ui_segmentationIndex = Getui_segmentationIndex(ConfigurationData);
    int ui_metricIndex = ConfigurationData.Metric;
    double* ui_fixed;

    if(ConfigurationData.is_twfFile)    {        ui_fixed = new double[13];     }
        else                            {        ui_fixed = new double[5];      }

    int ui_nDimFixed;
    Get_uiFixed_uinDimFixed(ui_fixed, ui_nDimFixed, ConfigurationData);

    if (ConfigurationData.OptimizationAlgorithm == "DE")//Differential Evolution Algorithm (DE)
    {
        //Reinterpret specific parameters
        int ui_population   =   ConfigurationData.OptimizationParameters.at(0);
        int ui_generations  =   ConfigurationData.OptimizationParameters.at(1);
        double ui_error     =   ConfigurationData.OptimizationParameters.at(2);
        int ui_strategy     =   ConfigurationData.OptimizationParameters.at(3);


        //Execute Optimization
        DESolver* solver = new DESolver(ui_nDim,ui_population);
//        solver->moveToThread(ConfigurationData.thread_console);
        solver->Setup(ui_min,ui_max,ui_strategy,0.5,0.8,ui_error,
                     orgImFileName,
                     ui_out_name,
                     ui_refVectorPoints,
                     ui_segmentationIndex,ui_metricIndex,
                     ui_rows_ref,ui_cols_ref,
                     ui_fixed,ui_nDimFixed,ui_generations,true,GlobalInfo);
        solver->Solve();
//        QObject::connect(ConfigurationData.thread_console,SIGNAL(started()),solver,SLOT(Solve()));
//        QObject::connect(solver,SIGNAL(finished()),ConfigurationData.thread_console,SLOT(quit()));
//        QObject::connect(solver,SIGNAL(finished()),solver,SLOT(deleteLater()));
//        QObject::connect(solver,SIGNAL(eval_exec(QString)),solver,SLOT(Write_Energy(QString)));
//        //Run optimization
//        ConfigurationData.thread_console->start();

    }
    else if(ConfigurationData.OptimizationAlgorithm == "GPS")//Generalized Pattern Search (GPS)
    {

    }
    else if(ConfigurationData.OptimizationAlgorithm == "MADS")//Mesh Adaptive Direct Search (MADS)
    {

    }
    else if(ConfigurationData.OptimizationAlgorithm == "NM")//Nelder Mead (NM)
    {

    }
    else
    {
        std::cout << "Wrong Optimization Algorihtm\nOptimization Algorithms accepted: DE, GPS, MADS, NM\n";
    }


//    if(ui->opt_optAlgComboBox->currentIndex() == 0)//Differential Evolution Algorithm (DE)
//    {
//        DESolver* solver = new DESolver(ui_nDim,ui_population);
//        solver->moveToThread(thread_opt);
//        solver->Setup(ui_min,ui_max,ui_strategy,0.5,0.8,ui_error,
//                     orgImFileName,
//                     QString(ui_out_name.c_str()),
//                     ui_refVectorPoints,
//                     ui_segmentationIndex,ui_metricIndex,
//                     ui_rows_ref,ui_cols_ref,
//                     ui_fixed,ui_nDimFixed,ui_generations,true,GlobalInfo);
//        //Connect SIGNALS and SLOTS
//        connect(thread_opt,SIGNAL(started()),solver,SLOT(Solve()));
//        connect(solver,SIGNAL(finished()),thread_opt,SLOT(quit()));
//        //connect(thread_opt,SIGNAL(finished()),thread_opt,SLOT(deleteLater()));
//        connect(solver,SIGNAL(n_iterations(int)),this,SLOT(opt_number_iterations(int)));
//        connect(solver,SIGNAL(n_ID(int)),this,SLOT(opt_n_ID(int)));
//        connect(solver,SIGNAL(n_Executions(int)),this,SLOT(opt_n_executions(int)));
//        connect(solver,SIGNAL(finished()),this,SLOT(opt_show_result()));
//        connect(solver,SIGNAL(finished()),this,SLOT(set_FullProcess()));
//        connect(solver,SIGNAL(finished()),solver,SLOT(deleteLater()));
//        connect(solver,SIGNAL(eval_exec(QString)),solver,SLOT(Write_Energy(QString)));
//        //Run optimization
//        thread_opt->start();
//    }
//    else if(ui->opt_optAlgComboBox->currentIndex() == 1)//Generalized Pattern Search (GPS)
//    {
//        PSSolver* solver = new PSSolver(ui_nDim);
//        solver->moveToThread(thread_opt);
//        solver->Setup(ui_min,ui_max,ui_search,
//                     orgImFileName,
//                     QString(ui_out_name.c_str()),
//                     ui_refVectorPoints,
//                     ui_segmentationIndex,ui_metricIndex,
//                     ui_rows_ref,ui_cols_ref,
//                     ui_fixed,ui_nDimFixed,ui_maxIter,ui_minMeshSize,ui_error,GlobalInfo);
//        //Connect SIGNALS and SLOTS
//        connect(thread_opt,SIGNAL(started()),solver,SLOT(Solve()));
//        connect(solver,SIGNAL(finished()),thread_opt,SLOT(quit()));
//        //connect(thread_opt,SIGNAL(finished()),thread_opt,SLOT(deleteLater()));
//        connect(solver,SIGNAL(n_iterations(int)),this,SLOT(opt_number_iterations(int)));
//        connect(solver,SIGNAL(n_ID(int)),this,SLOT(opt_n_ID(int)));
//        connect(solver,SIGNAL(n_Executions(int)),this,SLOT(opt_n_executions(int)));
//        connect(solver,SIGNAL(finished()),this,SLOT(opt_show_result()));
//        connect(solver,SIGNAL(finished()),this,SLOT(set_FullProcess()));
//        connect(solver,SIGNAL(finished()),solver,SLOT(deleteLater()));
//        connect(solver,SIGNAL(eval_exec(QString,int)),solver,SLOT(Write_Energy(QString,int)));
//        //Run optimization
//        thread_opt->start(QThread::TimeCriticalPriority);
//    }
//    else if(ui->opt_optAlgComboBox->currentIndex() == 2)//Mesh Adaptive Direct Search (MADS)
//    {
//        PSSolver* solver = new PSSolver(ui_nDim);
//        solver->moveToThread(thread_opt);
//        solver->SetupMADS(ui_min,ui_max,ui_search,
//                         orgImFileName,
//                         QString(ui_out_name.c_str()),
//                         ui_refVectorPoints,
//                         ui_segmentationIndex,ui_metricIndex,
//                         ui_rows_ref,ui_cols_ref,
//                         ui_fixed,ui_nDimFixed,ui_maxIter,ui_minMeshSize,ui_error,GlobalInfo);
//        //Connect SIGNALS and SLOTS
//        connect(thread_opt,SIGNAL(started()),solver,SLOT(SolveMADS()));
//        connect(solver,SIGNAL(finished()),thread_opt,SLOT(quit()));
//        //connect(thread_opt,SIGNAL(finished()),thread_opt,SLOT(deleteLater()));
//        connect(solver,SIGNAL(n_iterations(int)),this,SLOT(opt_number_iterations(int)));
//        connect(solver,SIGNAL(n_ID(int)),this,SLOT(opt_n_ID(int)));
//        connect(solver,SIGNAL(n_Executions(int)),this,SLOT(opt_n_executions(int)));
//        connect(solver,SIGNAL(finished()),this,SLOT(opt_show_result()));
//        connect(solver,SIGNAL(finished()),this,SLOT(set_FullProcess()));
//        connect(solver,SIGNAL(finished()),solver,SLOT(deleteLater()));
//        connect(solver,SIGNAL(eval_exec(QString,int)),solver,SLOT(Write_Energy(QString,int)));
//        //Run optimization
//        thread_opt->start();
//    }
//    else if(ui->opt_optAlgComboBox->currentIndex() == 3)//Nelder Mead (NM)
//    {
//        NMSolver* solver = new NMSolver(ui_nDim);
//        solver->moveToThread(thread_opt);
//        solver->Setup(ui_min,ui_max,ui_search,
//                     orgImFileName,
//                     QString(ui_out_name.c_str()),
//                     ui_refVectorPoints,
//                     ui_segmentationIndex,ui_metricIndex,
//                     ui_rows_ref,ui_cols_ref,
//                     ui_fixed,ui_nDimFixed,ui_maxIter,ui_error,GlobalInfo);
//        //Connect SIGNALS and SLOTS
//        connect(thread_opt,SIGNAL(started()),solver,SLOT(Solve()));
//        connect(solver,SIGNAL(finished()),thread_opt,SLOT(quit()));
//        //connect(thread_opt,SIGNAL(finished()),thread_opt,SLOT(deleteLater()));
//        connect(solver,SIGNAL(n_iterations(int)),this,SLOT(opt_number_iterations(int)));
//        connect(solver,SIGNAL(n_ID(int)),this,SLOT(opt_n_ID(int)));
//        connect(solver,SIGNAL(n_Executions(int)),this,SLOT(opt_n_executions(int)));
//        connect(solver,SIGNAL(finished()),this,SLOT(opt_show_result()));
//        connect(solver,SIGNAL(finished()),this,SLOT(set_FullProcess()));
//        connect(solver,SIGNAL(finished()),solver,SLOT(deleteLater()));
//        connect(solver,SIGNAL(eval_exec(QString)),solver,SLOT(Write_Energy(QString)));
//        //Run optimization
//        thread_opt->start();
//    }
}
