#include "DESolver.h"

#define Element(a,b,c)  a[b*nDim+c]
#define RowVector(a,b)  (&a[b*nDim])
#define CopyVector(a,b) memcpy((a),(b),nDim*sizeof(double))

DESolver::DESolver(int dim,int popSize) :
                    nDim(dim), nPop(popSize),
                    iterations(0), strategy(stRand1Exp),
                    scale(0.7), probability(0.5), bestEnergy(1.0E20),
                    trialSolution(0), bestSolution(0),
                    popEnergy(0), population(0)
{
    trialSolution = new double[nDim];
    bestSolution  = new double[nDim];
    popEnergy	  = new double[nPop];
    population	  = new double[nPop * nDim];
    min_ = new double[nDim];
    max_ = new double[nDim];
//    srand ((unsigned)time(0));
    return;
}

DESolver::~DESolver(void)
{
    if (trialSolution) delete trialSolution;
    if (bestSolution) delete bestSolution;
    if (popEnergy) delete popEnergy;
    if (population) delete population;

    trialSolution = bestSolution = popEnergy = population = 0;
    return;
}

void DESolver::Setup(double min[], double max[], int deStrategy,
           double diffScale, double crossoverProb, double conv,
           QString input_image, QString output_image,
           vector<vector<Point2f> > reference,
           int segIdx, int metIdx, int ref_rows, int ref_cols,
           double* fixParams, int nDimFixed, int maxGenerations,bool mainOpt, OptInfo GlobalInfo)
{
    //////////////////////////////////////////////////////
    //Added by Pedro
    //Parameters for the segmentation and metric
    m_input_image = input_image;
    m_output_image = output_image;
    m_reference = reference;
    m_seg_index = segIdx;
    m_metric_index = metIdx;
    m_img_rows = ref_rows;
    m_img_cols = ref_cols;
    m_nDimFixed = nDimFixed;
    m_maxGenerations = maxGenerations;
    m_GlobalInfo.copyFrom(GlobalInfo);
    m_fixed = new double[nDimFixed];
    m_mainOpt = mainOpt;
    m_currentEnergy = 1.0E20;    

    if(m_mainOpt)//DE is the main optimization algorithm
    {
        m_times_to_run = m_GlobalInfo.get_times_to_run();
    }
    else//DE is used as initialization for other algorithms
    {
        m_times_to_run = 1;
    }

    if(nDimFixed > 1)
    {
        for(int i = 0 ; i < nDimFixed ; i++)
        {            
            m_fixed[i] = fixParams[i];           
        }
    }
    else
    {        
        m_fixed[0] = 0;
    }
//    qDebug()<<"Passed parameters";
    //////////////////////////////////////////////////////

    int i;
    convergence = conv;
    strategy	= deStrategy;
    scale		= diffScale;
    probability = crossoverProb;

    for (i=0; i < nDim; i++)
    {
        min_[i]=min[i];
        max_[i]=max[i];
    }
    return;
}

double DESolver::EnergyFunction(double *trial)
    {
        iterations++;
        bool is_outLimits = false;
        for (int id = 0 ; id < nDim ; id++)
        {
            if (trial[id] < min_[id] || trial[id] > max_[id])
                is_outLimits = true;
        }

        if (is_outLimits)
            m_currentEnergy = 1.0E20;
        else
        {
            m_currentEnergy = EvalFun::EnergyFunction(trial,m_input_image,m_output_image,m_reference,
                                           m_seg_index,m_metric_index,m_img_rows,m_img_cols,
                                           m_fixed,min_,max_,nDim,m_GlobalInfo.get_bands_string().toStdString());
        }
//        qDebug() << "m_currentEnergy: "<<m_currentEnergy;
        std::cout << "\nIteration: " << iterations;
        return m_currentEnergy;
    }

bool DESolver::SolveHard(double* min,double* max,double* step)
{
    bool bAtSolution;

    bestEnergy = 1.0E20;
    bAtSolution = false;
    double stepdyn=0;
    double stepV[3];

    stepV[0] = (double) (max[0]-min[0])/(double)step[0];
    stepV[2] = (double) (max[2]-min[2])/(double)step[2];

    for (int i=0; i < nDim; i++)
        bestSolution[i] = 0.0;

    for (int i=0; i < nDim; i++)
    {
        min_[i]=min[i];
        max_[i]=max[i];
    }
    for (int i=0; i < nDim; i++)
        bestSolution[i] = 0.0;

    for (float a = min[0]; a<max[0]; a=a+stepV[0])
    {
        for (float c = min[2]; c<max[2]; c=c+stepV[2])
        {
            stepdyn = c/max[2]*step[1];
            if (stepdyn < 1)
                stepV[1]=max[1];
            else
                stepV[1]=(double) (max[1]-min[1])/(double)step[1];

            for (float b = min[1]; b<max[1]; b=b+stepV[1])
            {
                trialSolution[0]=a;
                trialSolution[1]=b;
                trialSolution[2]=c;
                trialEnergy = EnergyFunction(trialSolution);

                if (trialEnergy < bestEnergy)
                {
                    bestEnergy = trialEnergy;
                    CopyVector(bestSolution,trialSolution);
                }
            }
        }
    }
    return bAtSolution;
}

void DESolver::Solve()
{
    QString DE_init_name("Init_P0.txt");
    QFile::remove("Init_P0.txt");

    //Run many times the experiment, each time, it will save the results
    for(int times = 0 ; times < m_times_to_run ; times++)
    {
//        qDebug() << "Iteration: " <<times;
        iterations = 0;
        if(m_mainOpt)
            emit n_ID(times+1);

        double time_process = 0;
        clock_t start, end;
        start = clock();

        QString name_energy("Energy_DE_");
        name_energy.append(QString::number(times));
        name_energy.append(".txt");
        ofstream myfile;
        myfile.open(name_energy.toStdString().c_str());
        myfile << "trialEnergy	popEnergy	bestEnergy	trialPoint...\n";
        myfile.close();
        m_current_File = name_energy;

        int generation;
        int candidate;
        bool bAtSolution;
        double *lastEnergy = new double[m_maxGenerations];        
        double errorEnergy = 0;
        int m=0;

        bestEnergy = 1.0E20;
        bAtSolution = false;

        srand ((unsigned)time(0));
//        myfile.open(name_energy.toStdString().c_str(),std::ios::app);
        for (int i=0; i < nPop; i++)
        {
            for (int j=0; j < nDim; j++)
            {
                Element(population,i,j) = RandomUniform(min_[j],max_[j]);                                           
            }

//            myfile << "Population[" << i << "]: [" << Element(population,i,0) << ", "
//                                                   << Element(population,i,1) << ", "
//                                                   << Element(population,i,2) << "] \n";

            popEnergy[i] = 1.0E20;
        }
//        myfile << "////////////////////////////////////////////////////////////////////////\n";
//        myfile.close();

        for (int i=0; i < nDim; i++)
            bestSolution[i] = 0.0;

        for (generation=0;(generation < m_maxGenerations) && !bAtSolution;generation++)
        {
            bestPopEnergy=1.0E20;
            for (candidate=0; candidate < nPop; candidate++)
            {
                if(m_mainOpt)
                    emit n_iterations(m);

                switch (strategy)
                {
                        case stBest1Exp:
                                Best1Exp(candidate);break;
                        case stRand1Exp:
                                Rand1Exp(candidate);break;
                        case stRandToBest1Exp:
                                RandToBest1Exp(candidate);break;
                        case stBest2Exp:
                                Best2Exp(candidate);break;
                        case stRand2Exp:
                                Rand2Exp(candidate);break;
                        case stBest1Bin:
                                Best1Bin(candidate);break;
                        case stRand1Bin:
                                Rand1Bin(candidate);break;
                        case stRandToBest1Bin:
                                RandToBest1Bin(candidate);break;
                        case stBest2Bin:
                                Best2Bin(candidate);break;
                        case stRand2Bin:
                                Rand2Bin(candidate);break;
                    }
                trialEnergy = EnergyFunction(trialSolution);
                //qDebug() << "trialEnergy: "<< trialEnergy << " popEnergy[" << candidate << "]: " << popEnergy[candidate];

                // Set best pop energy
                if (trialEnergy < bestPopEnergy)
                {
                    bestPopEnergy = trialEnergy;
                    //qDebug() <<"if PopEnergy:  " << bestPopEnergy ;
                }

                if (trialEnergy < popEnergy[candidate])
                {
                    // New low for this candidate
                    popEnergy[candidate] = trialEnergy;                    
                    CopyVector(RowVector(population,candidate),trialSolution);                    

                    // Check if all-time low
                    if (trialEnergy < bestEnergy)
                    {
                        bestEnergy = trialEnergy;
                        CopyVector(bestSolution,trialSolution);
                        //qDebug() <<"if bestEnergy:  " << bestEnergy  ;
                    }

                    lastEnergy[generation]=bestEnergy;
                }
//                qDebug() << m_currentEnergy << "	" << bestPopEnergy << "	" << bestEnergy << "\n";
                emit eval_exec(m_current_File);
                m++;
            }

            if (generation == 1)
            {
                ofstream myfile2;
                myfile2.open(DE_init_name.toStdString().c_str(),std::ios::app);
                for (int ndim_idx = 0 ; ndim_idx < nDim ; ndim_idx++)
                {
                    myfile2 << bestSolution[ndim_idx] <<",";
                }
                myfile2 << bestEnergy <<"\n";
                myfile2.close();
            }

            if (generation > 20)
            {
                errorEnergy = 0;
                for (int i = 1; i < 11; i++){
                    errorEnergy = errorEnergy + lastEnergy[generation-i];}
                if (fabs(errorEnergy/10 - bestEnergy) < convergence)
                    break;
            }
        }
        end = clock();
        time_process = ((double)end - (double)start)/CLOCKS_PER_SEC;
        if(m_mainOpt)
        {
//            qDebug() << "Write Results DE";
            Save_Results(times,m,time_process);
        }

        delete[] lastEnergy;        
    }
    if(m_mainOpt)
        emit finished();
}

void DESolver::Best1Exp(int candidate)
{
    int r1, r2;
    int n;

    SelectSamples(candidate,&r1,&r2);
    n = (int)RandomUniform(0.0,(double)nDim);

    CopyVector(trialSolution,RowVector(population,candidate));
        for (int i=0; (RandomUniform(0.0,1.0) < probability) && (i < nDim); i++)
    {

        trialSolution[n] = bestSolution[n]
                            + scale * (Element(population,r1,n)
                            - Element(population,r2,n));
//                if (trialSolution[n]<min_[n])
//                    trialSolution[n]=min_[n];
//                if (trialSolution[n]>max_[n])
//                    trialSolution[n]=max_[n];
        n = (n + 1) % nDim;
    }        
    return;
}

void DESolver::Rand1Exp(int candidate)
{
    int r1, r2, r3;
    int n;

    SelectSamples(candidate,&r1,&r2,&r3);
    n = (int)RandomUniform(0.0,(double)nDim);

    CopyVector(trialSolution,RowVector(population,candidate));
    for (int i=0; (RandomUniform(0.0,1.0) < probability) && (i < nDim); i++)
    {
        trialSolution[n] = Element(population,r1,n)
                            + scale * (Element(population,r2,n)
                            - Element(population,r3,n));
//            if (trialSolution[n]<min_[n])
//                trialSolution[n]=min_[n];
//            if (trialSolution[n]>max_[n])
//                trialSolution[n]=max_[n];
        n = (n + 1) % nDim;
    }

    return;
}

void DESolver::RandToBest1Exp(int candidate)
{
    int r1, r2;
    int n;

    SelectSamples(candidate,&r1,&r2);
    n = (int)RandomUniform(0.0,(double)nDim);

    CopyVector(trialSolution,RowVector(population,candidate));
    for (int i=0; (RandomUniform(0.0,1.0) < probability) && (i < nDim); i++)
    {
        trialSolution[n] += scale * (bestSolution[n] - trialSolution[n])
                             + scale * (Element(population,r1,n)
                             - Element(population,r2,n));
//            if (trialSolution[n]<min_[n])
//                trialSolution[n]=min_[n];
//            if (trialSolution[n]>max_[n])
//                trialSolution[n]=max_[n];
        n = (n + 1) % nDim;
    }

    return;
}

void DESolver::Best2Exp(int candidate)
{
    int r1, r2, r3, r4;
    int n;

    SelectSamples(candidate,&r1,&r2,&r3,&r4);
    n = (int)RandomUniform(0.0,(double)nDim);

    CopyVector(trialSolution,RowVector(population,candidate));
    for (int i=0; (RandomUniform(0.0,1.0) < probability) && (i < nDim); i++)
    {
        trialSolution[n] = bestSolution[n] +
                            scale * (Element(population,r1,n)
                                        + Element(population,r2,n)
                                        - Element(population,r3,n)
                                        - Element(population,r4,n));
//            if (trialSolution[n]<min_[n])
//                trialSolution[n]=min_[n];
//            if (trialSolution[n]>max_[n])
//                trialSolution[n]=max_[n];
        n = (n + 1) % nDim;
    }

    return;
}

void DESolver::Rand2Exp(int candidate)
{
    int r1, r2, r3, r4, r5;
    int n;

    SelectSamples(candidate,&r1,&r2,&r3,&r4,&r5);
    n = (int)RandomUniform(0.0,(double)nDim);

    CopyVector(trialSolution,RowVector(population,candidate));
    for (int i=0; (RandomUniform(0.0,1.0) < probability) && (i < nDim); i++)
    {
        trialSolution[n] = Element(population,r1,n)
                            + scale * (Element(population,r2,n)
                                        + Element(population,r3,n)
                                        - Element(population,r4,n)
                                        - Element(population,r5,n));
//            if (trialSolution[n]<min_[n])
//                trialSolution[n]=min_[n];
//            if (trialSolution[n]>max_[n])
//                trialSolution[n]=max_[n];
        n = (n + 1) % nDim;
    }

    return;
}

void DESolver::Best1Bin(int candidate)
{
    int r1, r2;
    int n;

    SelectSamples(candidate,&r1,&r2);
    n = (int)RandomUniform(0.0,(double)nDim);

    CopyVector(trialSolution,RowVector(population,candidate));
    for (int i=0; i < nDim; i++)
    {
        if ((RandomUniform(0.0,1.0) < probability) || (i == (nDim - 1)))
            trialSolution[n] = bestSolution[n]
                                + scale * (Element(population,r1,n)
                                            - Element(population,r2,n));
//            if (trialSolution[n]<min_[n])
//                trialSolution[n]=min_[n];
//            if (trialSolution[n]>max_[n])
//                trialSolution[n]=max_[n];
        n = (n + 1) % nDim;
    }

    return;
}

void DESolver::Rand1Bin(int candidate)
{
    int r1, r2, r3;
    int n;

    SelectSamples(candidate,&r1,&r2,&r3);
    n = (int)RandomUniform(0.0,(double)nDim);

    CopyVector(trialSolution,RowVector(population,candidate));
    for (int i=0; i < nDim; i++)
    {
        if ((RandomUniform(0.0,1.0) < probability) || (i  == (nDim - 1)))
            trialSolution[n] = Element(population,r1,n)
                                + scale * (Element(population,r2,n)
                                                - Element(population,r3,n));
//            if (trialSolution[n]<min_[n])
//                trialSolution[n]=min_[n];
//            if (trialSolution[n]>max_[n])
//                trialSolution[n]=max_[n];
        n = (n + 1) % nDim;
    }

    return;
}

void DESolver::RandToBest1Bin(int candidate)
{
    int r1, r2;
    int n;

    SelectSamples(candidate,&r1,&r2);
    n = (int)RandomUniform(0.0,(double)nDim);

    CopyVector(trialSolution,RowVector(population,candidate));
    for (int i=0; i < nDim; i++)
    {
        if ((RandomUniform(0.0,1.0) < probability) || (i  == (nDim - 1)))
            trialSolution[n] += scale * (bestSolution[n] - trialSolution[n])
                                    + scale * (Element(population,r1,n)
                                                - Element(population,r2,n));
//            if (trialSolution[n]<min_[n])
//                trialSolution[n]=min_[n];
//            if (trialSolution[n]>max_[n])
//                trialSolution[n]=max_[n];
        n = (n + 1) % nDim;
    }

    return;
}

void DESolver::Best2Bin(int candidate)
{
    int r1, r2, r3, r4;
    int n;

    SelectSamples(candidate,&r1,&r2,&r3,&r4);
    n = (int)RandomUniform(0.0,(double)nDim);

    CopyVector(trialSolution,RowVector(population,candidate));
    for (int i=0; i < nDim; i++)
    {
        if ((RandomUniform(0.0,1.0) < probability) || (i  == (nDim - 1)))
            trialSolution[n] = bestSolution[n]
                                + scale * (Element(population,r1,n)
                                            + Element(population,r2,n)
                                            - Element(population,r3,n)
                                            - Element(population,r4,n));
//            if (trialSolution[n]<min_[n])
//                trialSolution[n]=min_[n];
//            if (trialSolution[n]>max_[n])
//                trialSolution[n]=max_[n];
        n = (n + 1) % nDim;
    }

    return;
}

void DESolver::Rand2Bin(int candidate)
{
    int r1, r2, r3, r4, r5;
    int n;

    SelectSamples(candidate,&r1,&r2,&r3,&r4,&r5);
    n = (int)RandomUniform(0.0,(double)nDim);

    CopyVector(trialSolution,RowVector(population,candidate));
    for (int i=0; i < nDim; i++)
    {
        if ((RandomUniform(0.0,1.0) < probability) || (i  == (nDim - 1)))
            trialSolution[n] = Element(population,r1,n)
                                + scale * (Element(population,r2,n)
                                            + Element(population,r3,n)
                                            - Element(population,r4,n)
                                            - Element(population,r5,n));
//            if (trialSolution[n]<min_[n])
//                trialSolution[n]=min_[n];
//            if (trialSolution[n]>max_[n])
//                trialSolution[n]=max_[n];
        n = (n + 1) % nDim;
    }

    return;
}

void DESolver::SelectSamples(int candidate,int *r1,int *r2,
                                        int *r3,int *r4,int *r5)
{
    if (r1)
    {
        do
        {
            *r1 = (int)RandomUniform(0.0,(double)nPop);
        }
        while (*r1 == candidate);
    }

    if (r2)
    {
        do
        {
            *r2 = (int)RandomUniform(0.0,(double)nPop);
        }
        while ((*r2 == candidate) || (*r2 == *r1));
    }

    if (r3)
    {
        do
        {
            *r3 = (int)RandomUniform(0.0,(double)nPop);
        }
        while ((*r3 == candidate) || (*r3 == *r2) || (*r3 == *r1));
    }

    if (r4)
    {
        do
        {
            *r4 = (int)RandomUniform(0.0,(double)nPop);
        }
        while ((*r4 == candidate) || (*r4 == *r3) || (*r4 == *r2) || (*r4 == *r1));
    }

    if (r5)
    {
        do
        {
            *r5 = (int)RandomUniform(0.0,(double)nPop);
        }
        while ((*r5 == candidate) || (*r5 == *r4) || (*r5 == *r3)
                                                    || (*r5 == *r2) || (*r5 == *r1));
    }

    return;
}

/*------Constants for RandomUniform()---------------------------------------*/
#define SEED 3
#define IM1 2147483563
#define IM2 2147483399
#define AM (1.0/IM1)
#define IMM1 (IM1-1)
#define IA1 40014
#define IA2 40692
#define IQ1 53668
#define IQ2 52774
#define IR1 12211
#define IR2 3791
#define NTAB 32
#define NDIV (1+IMM1/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)

double DESolver::RandomUniform(double minValue,double maxValue)
{    
    int randD;
    double result;
    randD = rand()%(1000)+1;

    result = minValue + (maxValue-minValue)*(double)randD/1000.0;

    return(result);

//    long j;
//    long k;
//    static long idum;
//    static long idum2=123456789;
//    static long iy=0;
//    static long iv[NTAB];
//    double result;

//    if (iy == 0)
//        idum = SEED;

//    if (idum <= 0)
//    {
//        if (-idum < 1)
//            idum = 1;
//        else
//            idum = -idum;

//        idum2 = idum;

//        for (j=NTAB+7; j>=0; j--)
//        {
//            k = idum / IQ1;
//            idum = IA1 * (idum - k*IQ1) - k*IR1;
//            if (idum < 0) idum += IM1;
//            if (j < NTAB) iv[j] = idum;
//        }

//        iy = iv[0];
//    }

//    k = idum / IQ1;
//    idum = IA1 * (idum - k*IQ1) - k*IR1;

//    if (idum < 0)
//        idum += IM1;

//    k = idum2 / IQ2;
//    idum2 = IA2 * (idum2 - k*IQ2) - k*IR2;

//    if (idum2 < 0)
//        idum2 += IM2;

//    j = iy / NDIV;
//    iy = iv[j] - idum2;
//    iv[j] = idum;

//    if (iy < 1)
//        iy += IMM1;

//    result = AM * iy;

//    if (result > RNMX)
//        result = RNMX;

//    result = minValue + result * (maxValue - minValue);
//    return(result);
}

void DESolver::Save_Results(int ID,int m,double time)
{    
    double stop = EnergyFunction(bestSolution);
    string final_result_name, final_shapefiles;

    std::cout << "\nFitness: " << stop << "   Iterations: " << iterations;
    std::cout << "\nGo to ./Results/report.txt to see the results of the experiment.";
//    qDebug() << QString("Fitness: %1  Iterations: %2").arg(stop).arg(iterations);

    //Only for Region Growing (BAATZ), input and output images have to be BMPs
    if(m_seg_index == 0)
    {
        final_result_name = QString("./Results/").toStdString() + m_GlobalInfo.get_opt_outLineEdit().toStdString() + ".png";
    }
    else if(m_seg_index == 4)//Only for MultiSeg
    {
        final_result_name = QString("./Results/").toStdString() + m_GlobalInfo.get_opt_outLineEdit().toStdString() + ".jpg";
    }
    else
    {
        final_result_name = QString("./Results/").toStdString() + m_GlobalInfo.get_opt_outLineEdit().toStdString() + ".png";
    }

    final_shapefiles = QString("./Results/OutputVector").toStdString();

    //Copying result files from ./temp to ./Results
    QFile::copy(QString(m_GlobalInfo.get_out_name().c_str()),QString(final_result_name.c_str()));
    QFile::copy(QString("./temp/result3.shp"),QString::fromStdString(final_shapefiles+".shp"));
    QFile::copy(QString("./temp/result3.shx"),QString::fromStdString(final_shapefiles+".shx"));
    QFile::copy(QString("./temp/result3.dbf"),QString::fromStdString(final_shapefiles+".dbf"));

    float result_testing = evaluate_metric(readShapeFile(final_shapefiles),
                                           readShapeFile(m_GlobalInfo.get_refImTestFileName().toStdString()),
                                           m_metric_index,
                                           m_img_rows,m_img_cols);

    QFile data(QString("./Results/report")+QString::number(ID)+(".txt"));
    data.open(QFile::WriteOnly | QFile::Truncate);
    QTextStream output(&data);
    output << "              Segmentation Parameter Tuner v." << m_GlobalInfo.get_version() << "\n";
    output << "**************************************************************\n\n";
    output << "General Information: \n";
    output << " - Base Image:   " << m_GlobalInfo.get_initialImgFileName() << "\n";
    output << " - Reference Training:    " << m_GlobalInfo.get_initialRefFileName() << "\n";
    output << " - Reference Testing:    " << m_GlobalInfo.get_refImTestFileName()<< "\n";
    output << " - Image Dim.:   " << QString::number(m_GlobalInfo.get_rows_ref()) + " x " +
                                     QString::number(m_GlobalInfo.get_cols_ref()) + " x " +
                                     QString::number(m_GlobalInfo.get_bands_ref()) << "\n\n\n";
    output << "General Configurations: \n";
    output << " - Optimization Algorithm:   " << m_GlobalInfo.get_optAlgComboBox_text() << "\n";
    output << " - Segmentation Algorithm:   " << m_GlobalInfo.get_segAlgComboBox_text() << "\n";
    output << " - Fitness Function:         " << m_GlobalInfo.get_fitComboBox_text() << "\n\n\n";
    output << "Output Results: \n";
    output << " - Output Image:     " << QString(final_result_name.c_str()) << "\n";
    output << " - Vector Data:      " << QString::fromStdString(final_shapefiles+".shp") << "\n";
    output << " - Fitness Value (Training):    " << stop << "\n";
    output << " - Fitness Value (Testing):  " << result_testing<< "\n";
    output << " - Num. Execution of Target Function:  " << iterations << "\n";
    output << " - Num. Iterations:  " << m << "\n";
    output << " - Processing Time:  " << time << "\n";
    output << " - ID :" << ID << "\n";

    output << " - Parameters tuned: \n";

    //Label 1 ///////////////////////////////////////////////////////////////////////////////////////////
    if (m_seg_index == 3)//only for Graph-based, it is double
    {
        if (!m_GlobalInfo.get_opt_segLabel1_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel1_text() << ":   " << (double)bestSolution[0] << "\n";
    }
    else//for the others, it is INT
    {
        if (!m_GlobalInfo.get_opt_segLabel1_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel1_text() << ":   " << (int)bestSolution[0] << "\n";
    }

    //Label 2 ///////////////////////////////////////////////////////////////////////////////////////////
    if((m_seg_index == 0) || (m_seg_index == 3))//only for Baatz and Graph-based
    {
        if (!m_GlobalInfo.get_opt_segLabel2_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel2_text() << ":   " << (double)bestSolution[1] << "\n";

    }
    else if ((m_seg_index == 1) || (m_seg_index == 2))//for Meanshift and SPRING, it is INT
    {
        if (!m_GlobalInfo.get_opt_segLabel2_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel2_text() << ":   " << (int)bestSolution[1] << "\n";
    }
    else if(m_seg_index == 4)
    {
        if (!m_GlobalInfo.get_opt_segLabel2_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel2_text() << ":   " << ((int)((double)bestSolution[1]/0.2))*0.2 << "\n";
    }

    //Label 3 ///////////////////////////////////////////////////////////////////////////////////////////
    if(m_seg_index == 0)
    {
        if (!m_GlobalInfo.get_opt_segLabel3_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel3_text() << ":   " << (double)bestSolution[2] << "\n";
    }
    else if((m_seg_index == 1)||(m_seg_index == 2)||(m_seg_index == 3))
    {
        if (!m_GlobalInfo.get_opt_segLabel3_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel3_text() << ":   " << (int)bestSolution[2] << "\n";
    }
    else if(m_seg_index == 4)//Just for Multiseg
    {
        int conf = (int)bestSolution[2];
        double conf_real;
        switch (conf)
        {
        case 1:
            conf_real = 0.80;break;
        case 2:
            conf_real = 0.85;break;
        case 3:
            conf_real = 0.90;break;
        case 4:
            conf_real = 0.95;break;
        case 5:
            conf_real = 0.99;break;
        case 6:
            conf_real = 0.995;break;
        case 7:
            conf_real = 0.999;break;
        default:
            break;
        }
        if (!m_GlobalInfo.get_opt_segLabel3_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel3_text() << ":   " << QString::number(conf_real) << "\n";
    }

   //Label 4 ///////////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel4_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel4_text() << ":   " << (int)bestSolution[3] << "\n";

   //Label 5 ///////////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel5_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel5_text() << ":   " << (int)bestSolution[4] << "\n";

   //Label 6 ///////////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel6_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel6_text() << ":   " << m_GlobalInfo.get_opt_segAlgComboBox2_text() << "\n";

   //Label 7 ///////////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel7_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel7_text() << ":   " << m_GlobalInfo.get_opt_segAlgComboBox3_text() << "\n";

   //Label 8 ///////////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel8_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel8_text() << ":   " << m_GlobalInfo.get_opt_segAlgComboBox4_text() << "\n";
   //Label 5 Coeff of Variation/////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel5_Coeff_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel5_Coeff_text() << ":   " << bestSolution[4] << "\n";
    data.close();
}

void DESolver::Write_Energy(QString myFileName)
{
    ofstream myFile;
    myFile.open(myFileName.toStdString().c_str(),std::ios::app);
    myFile << m_currentEnergy << "	" << bestPopEnergy << "	" << bestEnergy << "	";
    for (int i = 0 ; i < nDim ; i++)
    {
        if (i == (nDim-1))
        {   myFile << trialSolution[i] << "\n";}
        else
        {   myFile << trialSolution[i] << "	";}
    }
    myFile.close();
}
