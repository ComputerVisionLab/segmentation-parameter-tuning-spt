#-------------------------------------------------
#
# Project created by QtCreator 2014-03-24T10:47:03
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += console
#TARGET = SPT
TEMPLATE = app


SOURCES += main.cpp \
    mainwindow.cpp \
    DESolver.cpp \
    EvalFun.cpp \
    PSSolver.cpp \
    NMSolver.cpp \
    ShpFunct.cpp \
    Segmentation.cpp \
    polygon_canvas.cpp \
    loading.cpp \
    optinfo.cpp \
    consolefunctions.cpp

HEADERS  += \
    mainwindow.h \
    DESolver.h \
    EvalFun.h \
    PSSolver.h \
    NMSolver.h \
    ShpFunct.h \
    Segmentation.h \
    polygon_canvas.h \
    loading.h \
    optinfo.h \
    consolefunctions.h

FORMS    += mainwindow.ui \
    loading.ui

RESOURCES += \
    Resource.qrc

INCLUDEPATH += C://OpenCV2412//opencv//build//include


LIBS += shapelib.lib
LIBS += .///gdal///x64///lib//*.lib

QMAKE_CXXFLAGS+= -openmp
LIBS += -openmp

QMAKE_LFLAGS += /INCREMENTAL:NO

CONFIG += 64bit

CONFIG(32bit) {
    TARGET = SPT_x86
    QMAKE_CXXFLAGS += -m32
    LIBS += .///gdal///bld///lib//*.lib
    INCLUDEPATH += .//gdal//bld//include
    LIBS += C://OpenCV2412//opencv//build//x86//vc12//lib//*.lib
}

CONFIG(64bit) {
    TARGET = SPT_x64
    QMAKE_LFLAGS *= /MACHINE:X64
    LIBS += .///gdal///x64///lib//*.lib
    INCLUDEPATH += .//gdal//x64//include
    LIBS += C://OpenCV2412//opencv//build//x64//vc12//lib//*.lib
}
