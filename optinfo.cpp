#include "optinfo.h"

//Default Constructor
OptInfo::OptInfo()
{
    out_name = "";
    opt_outLineEdit = "";
    initialImgFileName = "";
    initialRefFileName = "";
    rows_ref = 0;
    cols_ref = 0;
    bands_ref = 0;
    optAlgComboBox_text = "";
    segAlgComboBox_text = "";
    fitComboBox_text = "";
    opt_segLabel1_text = "";
    opt_segLabel1_bool = false;
    opt_segLabel2_text = "";
    opt_segLabel2_bool = false;
    opt_segLabel3_text = "";
    opt_segLabel3_bool = false;
    opt_segLabel4_text = "";
    opt_segLabel4_bool = false;
    opt_segLabel5_text = "";
    opt_segLabel5_bool = false;
    opt_segLabel6_text = "";
    opt_segLabel6_bool = false;
    opt_segLabel7_text = "";
    opt_segLabel7_bool = false;
    opt_segLabel8_text = "";
    opt_segLabel8_bool = false;
    opt_segLabel5_Coeff_text = "";
    opt_segLabel5_Coeff_bool = false;
    opt_segLineEdit5_text = "";
    opt_segAlgComboBox2_text = "";
    opt_segAlgComboBox3_text = "";
    opt_segAlgComboBox4_text = "";
    times_to_run = 0;
    version = "";
    opt_segBands = "";
    refImTestFileName = "";
}

//Default Destructor
OptInfo::~OptInfo()
{}

//Copy Function
void OptInfo::copyFrom(OptInfo temp)
{
    out_name = temp.out_name;
    opt_outLineEdit = temp.opt_outLineEdit;
    initialImgFileName = temp.initialImgFileName;
    initialRefFileName = temp.initialRefFileName;
    rows_ref = temp.rows_ref;
    cols_ref = temp.cols_ref;
    bands_ref = temp.bands_ref;
    optAlgComboBox_text = temp.optAlgComboBox_text;
    segAlgComboBox_text = temp.segAlgComboBox_text;
    fitComboBox_text = temp.fitComboBox_text;
    opt_segLabel1_text = temp.opt_segLabel1_text;
    opt_segLabel1_bool = temp.opt_segLabel1_bool;
    opt_segLabel2_text = temp.opt_segLabel2_text;
    opt_segLabel2_bool = temp.opt_segLabel2_bool;
    opt_segLabel3_text = temp.opt_segLabel3_text;
    opt_segLabel3_bool = temp.opt_segLabel3_bool;
    opt_segLabel4_text = temp.opt_segLabel4_text;
    opt_segLabel4_bool = temp.opt_segLabel4_bool;
    opt_segLabel5_text = temp.opt_segLabel5_text;
    opt_segLabel5_bool = temp.opt_segLabel5_bool;
    opt_segLabel6_text = temp.opt_segLabel6_text;
    opt_segLabel6_bool = temp.opt_segLabel6_bool;
    opt_segLabel7_text = temp.opt_segLabel7_text;
    opt_segLabel7_bool = temp.opt_segLabel7_bool;
    opt_segLabel8_text = temp.opt_segLabel8_text;
    opt_segLabel8_bool = temp.opt_segLabel8_bool;
    opt_segLineEdit5_text = temp.opt_segLineEdit5_text;
    opt_segAlgComboBox2_text = temp.opt_segAlgComboBox2_text;
    opt_segAlgComboBox3_text = temp.opt_segAlgComboBox3_text;
    opt_segAlgComboBox4_text = temp.opt_segAlgComboBox4_text;
    times_to_run = temp.times_to_run;
    version = temp.version;
    opt_segBands = temp.opt_segBands;
    opt_segLabel5_Coeff_text = temp.opt_segLabel5_Coeff_text;
    opt_segLabel5_Coeff_bool = temp.opt_segLabel5_Coeff_bool;
    refImTestFileName = temp.refImTestFileName;
}

//Show content in current object
void OptInfo::show()
{
    qDebug() << "OptInfo data:";
    qDebug() << QString("SPT Version: %1").arg(version);
    qDebug() << QString("ui_out_name: %1").arg(out_name.c_str());
    qDebug() << QString("OutputName: %1 \nInitialImageFileName: %2 \nInitialRefFileName: %3")
                .arg(opt_outLineEdit)
                .arg(initialImgFileName)
                .arg(initialRefFileName);
    qDebug() << QString("Rows: %1 \nCols: %2 Bands: %3")
                .arg(rows_ref)
                .arg(cols_ref)
                .arg(bands_ref);
    qDebug() << QString("OptAlgComboBox_Text: %1 \nSegAlgComboBox_Text: %2 \nFitComboBox_Text: %3")
                .arg(optAlgComboBox_text)
                .arg(segAlgComboBox_text)
                .arg(fitComboBox_text);
    qDebug() << QString("opt_SegLabel1_Text: %1 opt_SegLabel1_bool: %2").arg(opt_segLabel1_text).arg(opt_segLabel1_bool);
    qDebug() << QString("opt_SegLabel2_Text: %1 opt_SegLabel2_bool: %2").arg(opt_segLabel2_text).arg(opt_segLabel2_bool);
    qDebug() << QString("opt_SegLabel3_Text: %1 opt_SegLabel3_bool: %2").arg(opt_segLabel3_text).arg(opt_segLabel3_bool);
    qDebug() << QString("opt_SegLabel4_Text: %1 opt_SegLabel4_bool: %2").arg(opt_segLabel4_text).arg(opt_segLabel4_bool);
    qDebug() << QString("opt_SegLabel5_Text: %1 opt_SegLabel5_bool: %2").arg(opt_segLabel5_text).arg(opt_segLabel5_bool);
    qDebug() << QString("opt_SegLabel6_Text: %1 opt_SegLabel6_bool: %2").arg(opt_segLabel6_text).arg(opt_segLabel6_bool);
    qDebug() << QString("opt_SegLabel7_Text: %1 opt_SegLabel7_bool: %2").arg(opt_segLabel7_text).arg(opt_segLabel7_bool);
    qDebug() << QString("opt_SegLabel8_Text: %1 opt_SegLabel8_bool: %2").arg(opt_segLabel8_text).arg(opt_segLabel8_bool);
    qDebug() << QString("opt_SegLabel5_Coeff_Text: %1 opt_SegLabel5_Coeff_bool: %2").arg(opt_segLabel5_Coeff_text).arg(opt_segLabel5_Coeff_bool);
    qDebug() << QString("Time_to_Run: %1").arg(times_to_run);
    qDebug() << QString("opt_segLineEdit5: %1").arg(opt_segLineEdit5_text);
    qDebug() << QString("opt_segAlgComboBox2: %1").arg(opt_segAlgComboBox2_text);
    qDebug() << QString("opt_segAlgComboBox3: %1").arg(opt_segAlgComboBox3_text);
    qDebug() << QString("opt_segAlgComboBox4: %1").arg(opt_segAlgComboBox4_text);
    qDebug() << QString("opt_segBands: %1").arg(opt_segBands);
    qDebug() << QString("refImTestFileName: %1").arg(refImTestFileName);
}
