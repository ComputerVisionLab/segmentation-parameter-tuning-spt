#include "EvalFun.h"
//#include <QDebug>
double EvalFun::EnergyFunction(double *var,
                      QString input_image,
                      QString output_image,
                      vector<vector<Point2f> > reference,
                      int segIdx, int metIdx,int rows, int cols,
                      double* fixParams,double* min, double* max,int nDim, string bands_string)
{
    double* new_var = new double[nDim];

    for(int i = 0 ; i < nDim ; i++)
    {        
        new_var[i] = var[i];        
    }

    evaluate_segmentation(input_image.toStdString(),
                          output_image.toStdString(),
                          new_var,fixParams,rows,cols,segIdx,bands_string);

    QString shape_result = "./temp/result3.shp";

    float result;
    if(fixParams[4] == 1)
    {        
        result = evaluate_metric(readShapeFile(shape_result.toStdString(),rows,
                                               fixParams[5],// x pixel resolution
                                               fixParams[8],// y pixel resolution
                                               fixParams[6],// rotational component x
                                               fixParams[7],// rotational component y
                                               fixParams[9],// Upper Left East coordinate
                                               fixParams[10],// Upper Left North coordinate
                                               fixParams[11],// rest_x
                                               fixParams[12]),// rest_y
                                 reference,metIdx,
                                 rows,cols);
    }
    else
    {        
        result = evaluate_metric(readShapeFile(shape_result.toStdString()),
                                 reference,metIdx,
                                 rows,cols);
    }
//    qDebug() << "Metric: " << result;
    delete[] new_var;
    return result;
}
