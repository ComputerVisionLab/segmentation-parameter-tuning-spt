#include "ShpFunct.h"

//convert a Vector of double points of OpenCV to a Shape File
void VectorPoints2Shape(const char* reference_path, vector< vector<Point> > GT)
{
    // Type of entities to be saved in the shapefile
    int shape_type = SHPT_POLYGON;
    //Creating the SHP, SHX and DBF files
    SHPHandle new_SHP = SHPCreate(reference_path, shape_type);
    DBFHandle new_DBF = DBFCreate(reference_path);
    //Iterators for entities and vertices of each entity.
    int entities_iter, vertices_iter;

    for (entities_iter = 0 ; entities_iter < GT.size() ; entities_iter++)
    {
        //Getting number of vertices
        int n_Vertices = GT[entities_iter].size();
        //arrays to save coordinates of points of each polygon
        //double pXs[n_Vertices];
        double *pXs = new double[n_Vertices];
        //double pYs[n_Vertices];
        double *pYs = new double[n_Vertices];
        for ( vertices_iter = 0 ; vertices_iter < n_Vertices ; vertices_iter++)
        {
            //Extracting coordinates X and Y from vector of vector of points
            pXs[vertices_iter] = (double)(GT[entities_iter][vertices_iter].x);
            pYs[vertices_iter] = (double)(GT[entities_iter][vertices_iter].y);
        }
        //Creating the SHPObject that contains the data for 1 polygon
        SHPObject *psCShape = SHPCreateSimpleObject(shape_type,n_Vertices,pXs,pYs,NULL);
        //Writing the object in the SHPHandle
        SHPWriteObject(new_SHP,-1,psCShape);
        //Destroying the SHPObject
        SHPDestroyObject(psCShape);
        delete[] pXs;
        delete[] pYs;
    }
    //Closing the files
    SHPClose(new_SHP);
    DBFClose(new_DBF);   
}

//Show a Shape File by its path
void showShapeFile(const char* reference_path)
{
    //objects to the SHAPE and DBF files
    SHPHandle current_SHP;
    DBFHandle current_DBF;

    int nEntities; //number of entities
    int nType;
    //reading the shape file
    current_SHP = SHPOpen(reference_path,"rb");
    current_DBF = DBFOpen(reference_path,"rb");

    int NFields = DBFGetFieldCount(current_DBF);
    qDebug()<<NFields;
    int iField;
    char* fieldname;
    DBFGetFieldInfo(current_DBF, iField, fieldname,NULL, NULL );

    qDebug()<<fieldname<<"    "<<iField;

    if( current_SHP == NULL || current_DBF == NULL )
    {
        qDebug()<<"Unable to open SHP or DBF files";
        return;
    }
    else
    {
        //Get the number of entities
        SHPGetInfo(current_SHP, &nEntities, &nType, NULL, NULL);
        int i,num_vertices,vert_iter;
        //If it is a GUI, it shows in the Application Output window
        //If it is a Win32 application, it will appear in the console
        qDebug()<<"# of Entities:"<<nEntities;
        qDebug()<<"Type of Shapes"<<nType;

        //Iterate over each entity
        for (i = 0 ; i < nEntities ; i++)
        {
           //extract each polygon
           SHPObject *psCShape;
           psCShape = SHPReadObject(current_SHP,i);
           //extract its number of vertices
           num_vertices = psCShape->nVertices;
           qDebug()<<"Polygon #"<<i+1;
           qDebug()<<"# of Vertices: "<<num_vertices;

           for(vert_iter = 0 ; vert_iter < num_vertices ; vert_iter++)
           {
               //plot coordinates of each point
               qDebug()<<"("<<psCShape->padfX[vert_iter]<<","<<psCShape->padfY[vert_iter]<<")";
           }
           //Destroy the SHPObject
           SHPDestroyObject(psCShape);
        }        
    }
    //Close the shape file
    SHPClose(current_SHP);
    DBFClose(current_DBF);
}

//Show a Shape File by the Handle of the file
void showShapeFile(SHPHandle current_SHP)
{
    int nEntities; //number of entities

    SHPGetInfo(current_SHP, &nEntities, NULL, NULL, NULL);
    int i,num_vertices,vert_iter;
    qDebug()<<"# of Entities:"<<nEntities;

    for (i = 0 ; i < nEntities ; i++)
    {
       SHPObject *psCShape;
       //extract each polygon
       psCShape = SHPReadObject(current_SHP,i);
       num_vertices = psCShape->nVertices;
       qDebug()<<"# of Vertices: "<<num_vertices;

       for(vert_iter = 0 ; vert_iter < num_vertices ; vert_iter++)
       {
           qDebug()<<"("<<psCShape->padfX[vert_iter]<<","<<psCShape->padfY[vert_iter]<<")";
       }
       SHPDestroyObject(psCShape);
    }
}

//Show a Vector of double points of OpenCV
void showVectorofPoints2d(vector<vector<Point> > references)
{
    unsigned int entities_iter, vertices_iter;
    qDebug()<<"Vector of Point2d:";

    for (entities_iter = 0 ; entities_iter < references.size() ; entities_iter++)
    {
        qDebug()<<"Polygon #"<<entities_iter+1;
        for ( vertices_iter = 0 ; vertices_iter < references[entities_iter].size() ; vertices_iter++)
        {
            qDebug()<<"("<<references[entities_iter][vertices_iter].x<<","<<references[entities_iter][vertices_iter].y<<")";
        }
    }
}

//Show a Vector of double points of OpenCV
void showVectorofPoints2d(vector<vector<Point2f> > references)
{
    unsigned int entities_iter, vertices_iter;
    qDebug()<<"Vector of Point2d:";

    for (entities_iter = 0 ; entities_iter < references.size() ; entities_iter++)
    {
        qDebug()<<"Polygon #"<<entities_iter+1;
        for ( vertices_iter = 0 ; vertices_iter < references[entities_iter].size() ; vertices_iter++)
        {
            qDebug()<<"("<<references[entities_iter][vertices_iter].x<<","<<references[entities_iter][vertices_iter].y<<")";
        }
    }
}

//Read a ShapeFile by its path and convert it into a vector< vector<Point2f> >
vector< vector<Point2f> > readShapeFile(string shape_path)
{
    vector< vector<Point2f> > allPolygons;
    //objects to the SHAPE and DBF files
    SHPHandle current_SHP;
    DBFHandle current_DBF;

    //reading the shape file
    current_SHP = SHPOpen(shape_path.c_str(),"rb");
    current_DBF = DBFOpen(shape_path.c_str(),"rb");

    DBFGetFieldInfo(current_DBF, NULL, NULL,NULL, NULL );

    if( current_SHP == NULL || current_DBF == NULL )
    {
        qDebug()<<"Unable to open SHP or DBF files";
    }
    else
    {        
        int num_vertices,nEntities,vert_iter;
        //Get the number of entities
        SHPGetInfo(current_SHP, &nEntities, NULL, NULL, NULL);
        int n_Parts;
        //Iterate over each entity
        for (int i = 0 ; i < nEntities ; i++)
        {
           vector<Point2f> tempPolygon;
           SHPObject *psCShape;
           //extract each polygon
           psCShape = SHPReadObject(current_SHP,i);
           //extract its number of vertices
           num_vertices = psCShape->nVertices;
           n_Parts = psCShape->nParts;

           if(n_Parts == 1)
           {
               for(vert_iter = 0 ; vert_iter < num_vertices ; vert_iter++)
               {
                   //Save coordinates X and Y into a QPoint
                   Point2f tempPoint;
                   tempPoint.x = psCShape->padfX[vert_iter];
                   tempPoint.y = psCShape->padfY[vert_iter];
                   //Save a Point in a Polygon
                   tempPolygon.push_back(tempPoint);
               }
           }
           else
           {
               int* panParts = new int[n_Parts];
               panParts = psCShape->panPartStart;
               for (int ind = 0 ; ind < 1 ; ind++)//ind < n_Parts - 1
               {
                   //iterate over each part, from the first until one before the last one
                   for(int v = panParts[ind] ; v < panParts[ind+1] -1; v++)
                   {
                       Point2f tempPoint;
                       tempPoint.x = psCShape->padfX[v];
                       tempPoint.y = psCShape->padfY[v];
                       tempPolygon.push_back(tempPoint);
                   }
               }
           }
           //Save a polygon
           allPolygons.push_back(tempPolygon);
           //Reset a Polygon
           tempPolygon.clear();
           //Destroy the SHPObject
           SHPDestroyObject(psCShape);
        }        
    }
    //Close the shape file
    SHPClose(current_SHP);
    DBFClose(current_DBF);
    //Return a vector of Polygons
    return allPolygons;
}

// Read a shapefile and the TWF file associated to a geo referenced TIF image
vector< vector<Point2f> > readShapeFile(string shape_path , QString twf_file, string abs_shape, int rows)
{
    //Open TWF File
    QFile file(twf_file);
    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug()<<"Error opening TWF file";
    }
    // Values obtained from the TWF file
    double  x_res, // x pixel resolution
            y_res, // x pixel resolution
            rot_x, // rotational component x
            rot_y, // rotational component y
            upperleft_east, // Upper Left East coordinate
            upperleft_north; // Upper Left North coordinate
    double rest_x, rest_y; //decimal parts of upperleft_east and upperleft_north

    QTextStream iter(&file);
    int n_line = 0; // line counter
    while(!iter.atEnd())
    {
        QString line = iter.readLine();
        QStringList fields = line.split(" ");
        if (n_line == 0) // x pixel resolution
        {
            x_res = fields.at(fields.length()-1).toDouble();
        }
        else if (n_line == 1 )// rotational component x
        {
            rot_x = fields.at(fields.length()-1).toDouble();
        }
        else if (n_line == 2)// rotational component y
        {
            rot_y = fields.at(fields.length()-1).toDouble();
        }
        else if (n_line == 3)// x pixel resolution
        {
            y_res = fields.at(fields.length()-1).toDouble();
        }
        else if (n_line == 4)// Upper Left East coordinate
        {
            upperleft_east = fields.at(fields.length()-1).toDouble();
            QStringList number = fields.at(fields.length()-1).split(".");
            rest_x = QString("0.").append(number.at(number.length()-1)).toDouble();
        }
        else if (n_line == 5)// Upper Left North coordinate
        {
            upperleft_north = fields.at(fields.length()-1).toDouble();
            QStringList number2 = fields.at(fields.length()-1).split(".");
            rest_y = QString("0.").append(number2.at(number2.length()-1)).toDouble();
        }
        n_line++;
    }

    vector< vector<Point2f> > allPolygons;
    //objects to the SHAPE and DBF files
    SHPHandle current_SHP, abs_SHP;
    DBFHandle current_DBF, abs_DBF;
    int shape_type = SHPT_POLYGON;

    //reading the shape file
    abs_SHP = SHPCreate(abs_shape.c_str(),shape_type);
    abs_DBF = DBFCreate(abs_shape.c_str());
    current_SHP = SHPOpen(shape_path.c_str(),"rb");
    current_DBF = DBFOpen(shape_path.c_str(),"rb");

    DBFGetFieldInfo(current_DBF, NULL, NULL,NULL, NULL );

    if( current_SHP == NULL || current_DBF == NULL )
    {
        qDebug()<<"Unable to open SHP or DBF files";
    }
    else
    {
        int num_vertices,nEntities;
        //Get the number of entities
        SHPGetInfo(current_SHP, &nEntities, NULL, NULL, NULL);
        int n_Parts;

        //Iterate over each entity
        for (int i = 0 ; i < nEntities ; i++)
        {
           vector<Point2f> tempPolygon;
           SHPObject *psCShape;
           //extract each polygon
           psCShape = SHPReadObject(current_SHP,i);
           //extract its number of vertices
           num_vertices = psCShape->nVertices;
           n_Parts = psCShape->nParts;

           //double pointers to copy the translated coordinates
           double *pXs_abs = new double[num_vertices];
           double *pYs_abs = new double[num_vertices];

           if(n_Parts == 1)
           {

               for(int vert_iter = 0 ; vert_iter < num_vertices ; vert_iter++)
               {
                   //Save coordinates X and Y into a QPoint
                   Point2f tempPoint;
                   //Translate the coordinates to an absolut coordinate reference system
                   tempPoint.x = ((psCShape->padfX[vert_iter] - upperleft_east)*1.0/x_res) + rest_x;
                   tempPoint.y = (rows - ((psCShape->padfY[vert_iter] - upperleft_north)*1.0/y_res) - rest_y);
                   //Save a Point in a Polygon
                   tempPolygon.push_back(tempPoint);
               }
           }
           else
           {
               int* panParts = new int[n_Parts];
               panParts = psCShape->panPartStart;
               for (int ind = 0 ; ind < 1 ; ind++)//ind < n_Parts - 1
               {
                   //iterate over each part, from the first until one before the last one
                   for(int v = panParts[ind] ; v < panParts[ind+1] -1; v++)
                   {
                       Point2f tempPoint;
                       tempPoint.x = ((psCShape->padfX[v] - upperleft_east)*1.0/x_res) + rest_x;
                       tempPoint.y = (rows - ((psCShape->padfY[v] - upperleft_north)*1.0/y_res) - rest_y);
                       tempPolygon.push_back(tempPoint);
                   }
               }
           }
           for(int vert_iter = 0 ; vert_iter < num_vertices ; vert_iter++)
           {              
               //Copy translated coordinates
               pXs_abs[vert_iter] = (double) ((psCShape->padfX[vert_iter] - upperleft_east)*1.0/x_res) + rest_x;
               pYs_abs[vert_iter] = (double) (rows - ((psCShape->padfY[vert_iter] - upperleft_north)*1.0/y_res) - rest_y);
           }
           SHPObject *psCShape_abs = SHPCreateObject(shape_type,psCShape->nShapeId,
                                                     psCShape->nParts,
                                                     psCShape->panPartStart,
                                                     psCShape->panPartType,
                                                     num_vertices,
                                                     pXs_abs,pYs_abs,NULL,NULL);
           SHPWriteObject(abs_SHP,-1,psCShape_abs);
           SHPDestroyObject(psCShape_abs);
           delete[] pXs_abs;
           delete[] pYs_abs;
           //Save a polygon
           allPolygons.push_back(tempPolygon);
           //Reset a Polygon
           tempPolygon.clear();
           //Destroy the SHPObject
           SHPDestroyObject(psCShape);
        }
    }
    //Close the shape file
    SHPClose(current_SHP);
    DBFClose(current_DBF);
    SHPClose(abs_SHP);
    DBFClose(abs_DBF);
    //Return a vector of Polygons
    return allPolygons;
}

//Read a shapefile, move it to relative coordinates and provide the values of the TFW file
vector< vector<Point2f> > readShapeFile(string shape_path , QString twf_file, string abs_shape, int rows, double *twf_values)
{
    //Open TWF File
    QFile file(twf_file);
    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug()<<"Error opening TWF file";
    }
    // Values obtained from the TWF file
    double  x_res, // x pixel resolution
            y_res, // y pixel resolution
            rot_x, // rotational component x
            rot_y, // rotational component y
            upperleft_east, // Upper Left East coordinate
            upperleft_north; // Upper Left North coordinate
    double rest_x, rest_y; //decimal parts of upperleft_east and upperleft_north

    QTextStream iter(&file);
    int n_line = 0; // line counter
    while(!iter.atEnd())
    {
        QString line = iter.readLine();
        QStringList fields = line.split(" ");
        if (n_line == 0) // x pixel resolution
        {
            x_res = fields.at(fields.length()-1).toDouble();
            twf_values[n_line] = x_res;
        }
        else if (n_line == 1 )// rotational component x
        {
            rot_x = fields.at(fields.length()-1).toDouble();
            twf_values[n_line] = rot_x;
        }
        else if (n_line == 2)// rotational component y
        {
            rot_y = fields.at(fields.length()-1).toDouble();
            twf_values[n_line] = rot_y;
        }
        else if (n_line == 3)// x pixel resolution
        {
            y_res = fields.at(fields.length()-1).toDouble();
            twf_values[n_line] = y_res;
        }
        else if (n_line == 4)// Upper Left East coordinate
        {
            upperleft_east = fields.at(fields.length()-1).toDouble();
            twf_values[n_line] = upperleft_east;
            QStringList number = fields.at(fields.length()-1).split(".");
            rest_x = QString("0.").append(number.at(number.length()-1)).toDouble();
            twf_values[6] = rest_x;
        }
        else if (n_line == 5)// Upper Left North coordinate
        {
            upperleft_north = fields.at(fields.length()-1).toDouble();
            twf_values[n_line] = upperleft_north;
            QStringList number2 = fields.at(fields.length()-1).split(".");
            rest_y = QString("0.").append(number2.at(number2.length()-1)).toDouble();
            twf_values[7] = rest_y;
        }
        n_line++;
    }

    vector< vector<Point2f> > allPolygons;
    //objects to the SHAPE and DBF files
    SHPHandle current_SHP, abs_SHP;
    DBFHandle current_DBF, abs_DBF;
    int shape_type = SHPT_POLYGON;

    //reading the shape file
    abs_SHP = SHPCreate(abs_shape.c_str(),shape_type);
    abs_DBF = DBFCreate(abs_shape.c_str());
    current_SHP = SHPOpen(shape_path.c_str(),"rb");
    current_DBF = DBFOpen(shape_path.c_str(),"rb");

    DBFGetFieldInfo(current_DBF, NULL, NULL,NULL, NULL );

    if( current_SHP == NULL || current_DBF == NULL )
    {
        qDebug()<<"Unable to open SHP or DBF files";
    }
    else
    {
        int num_vertices,nEntities;
        //Get the number of entities
        SHPGetInfo(current_SHP, &nEntities, NULL, NULL, NULL);
        int n_Parts;

        //Iterate over each entity
        for (int i = 0 ; i < nEntities ; i++)
        {
           vector<Point2f> tempPolygon;
           SHPObject *psCShape;
           //extract each polygon
           psCShape = SHPReadObject(current_SHP,i);
           //extract its number of vertices
           num_vertices = psCShape->nVertices;
           n_Parts = psCShape->nParts;

           //double pointers to copy the translated coordinates
           double *pXs_abs = new double[num_vertices];
           double *pYs_abs = new double[num_vertices];

           if(n_Parts == 1)
           {
               for(int vert_iter = 0 ; vert_iter < num_vertices ; vert_iter++)
               {
                   //Save coordinates X and Y into a QPoint
                   Point2f tempPoint;
                   //Translate the coordinates to an absolut coordinate reference system
                   tempPoint.x = ((psCShape->padfX[vert_iter] - upperleft_east)*1.0/x_res) + rest_x;
                   tempPoint.y = (rows - ((psCShape->padfY[vert_iter] - upperleft_north)*1.0/y_res) - rest_y);
    //               qDebug() << tempPoint.x << "    " << tempPoint.y;
                   //Save a Point in a Polygon
                   tempPolygon.push_back(tempPoint);
               }
           }
           else
           {
               int* panParts = new int[n_Parts];
               panParts = psCShape->panPartStart;
               for (int ind = 0 ; ind < 1 ; ind++)//ind < n_Parts - 1
               {
                   //iterate over each part, from the first until one before the last one
                   for(int v = panParts[ind] ; v < panParts[ind+1] -1; v++)
                   {
                       Point2f tempPoint;
                       tempPoint.x = ((psCShape->padfX[v] - upperleft_east)*1.0/x_res) + rest_x;
                       tempPoint.y = (rows - ((psCShape->padfY[v] - upperleft_north)*1.0/y_res) - rest_y);
                       tempPolygon.push_back(tempPoint);
                   }
               }
           }

           for(int vert_iter = 0 ; vert_iter < num_vertices ; vert_iter++)
           {              
               //Copy translated coordinates
               pXs_abs[vert_iter] = (double) ((psCShape->padfX[vert_iter] - upperleft_east)*1.0/x_res) + rest_x;
               pYs_abs[vert_iter] = (double) (rows - ((psCShape->padfY[vert_iter] - upperleft_north)*1.0/y_res) - rest_y);
//               qDebug() << "pXs_abs: " << psCShape->padfX[vert_iter] << " pYs_abs: " << psCShape->padfY[vert_iter] ;
           }
           SHPObject *psCShape_abs = SHPCreateObject(shape_type,psCShape->nShapeId,
                                                     psCShape->nParts,
                                                     psCShape->panPartStart,
                                                     psCShape->panPartType,
                                                     num_vertices,
                                                     pXs_abs,pYs_abs,NULL,NULL);
           SHPWriteObject(abs_SHP,-1,psCShape_abs);
           SHPDestroyObject(psCShape_abs);
           delete[] pXs_abs;
           delete[] pYs_abs;
           //Save a polygon
           allPolygons.push_back(tempPolygon);
           //Reset a Polygon
           tempPolygon.clear();
           //Destroy the SHPObject
           SHPDestroyObject(psCShape);
        }
    }
    //Close the shape file
    SHPClose(current_SHP);
    DBFClose(current_DBF);
    SHPClose(abs_SHP);
    DBFClose(abs_DBF);
    //Return a vector of Polygons
    return allPolygons;
}

vector< vector<Point2f> > readShapeFile(string shape_path, int rows, double x_res, double y_res, double rot_x,double rot_y, double upperleft_east, double upperleft_north, double rest_x, double rest_y)
{
    vector< vector<Point2f> > allPolygons;
    //objects to the SHAPE and DBF files
    SHPHandle current_SHP;
    DBFHandle current_DBF;
    int shape_type = SHPT_POLYGON;

    //reading the shape file    
    current_SHP = SHPOpen(shape_path.c_str(),"rb");
    current_DBF = DBFOpen(shape_path.c_str(),"rb");

    DBFGetFieldInfo(current_DBF, NULL, NULL,NULL, NULL );

    if( current_SHP == NULL || current_DBF == NULL )
    {
        qDebug()<<"Unable to open SHP or DBF files";
    }
    else
    {
        int num_vertices,nEntities;
        //Get the number of entities
        SHPGetInfo(current_SHP, &nEntities, NULL, NULL, NULL);
        int n_Parts;

        //Iterate over each entity
        for (int i = 0 ; i < nEntities ; i++)
        {
           vector<Point2f> tempPolygon;
           SHPObject *psCShape;
           //extract each polygon
           psCShape = SHPReadObject(current_SHP,i);
           //extract its number of vertices
           num_vertices = psCShape->nVertices;
           n_Parts = psCShape->nParts;

           if(n_Parts == 1)
           {
               for(int vert_iter = 0 ; vert_iter < num_vertices ; vert_iter++)
               {
                   //Save coordinates X and Y into a QPoint
                   Point2f tempPoint;
                   //Translate the coordinates to an absolut coordinate reference system
                   tempPoint.x = (double)(((psCShape->padfX[vert_iter] - upperleft_east)*1.0/x_res) + rest_x);
                   tempPoint.y = (double)(rows - ((psCShape->padfY[vert_iter] - upperleft_north)*1.0/y_res) - rest_y);
                   //Save a Point in a Polygon
                   tempPolygon.push_back(tempPoint);
               }
           }
           else
           {
               int* panParts = new int[n_Parts];
               panParts = psCShape->panPartStart;
               for (int ind = 0 ; ind < 1 ; ind++)//ind < n_Parts - 1
               {
                   //iterate over each part, from the first until one before the last one
                   for(int v = panParts[ind] ; v < panParts[ind+1] -1; v++)
                   {
                       Point2f tempPoint;
                       tempPoint.x = (double)(((psCShape->padfX[v] - upperleft_east)*1.0/x_res) + rest_x);
                       tempPoint.y = (double)(rows - ((psCShape->padfY[v] - upperleft_north)*1.0/y_res) - rest_y);
                       tempPolygon.push_back(tempPoint);
                   }
               }
           }
           //Save a polygon
           allPolygons.push_back(tempPolygon);
           //Reset a Polygon
           tempPolygon.clear();
           //Destroy the SHPObject
           SHPDestroyObject(psCShape);
        }
    }
    //Close the shape file
    SHPClose(current_SHP);
    DBFClose(current_DBF);
    //Return a vector of Polygons
    return allPolygons;
}

vector< vector<Point2f> > readShapeFile(string shape_path, vector<Point2f> &centroids)
{
    vector< vector<Point2f> > allPolygons;
    //objects to the SHAPE and DBF files
    SHPHandle current_SHP;
    DBFHandle current_DBF;

    //reading the shape file
    current_SHP = SHPOpen(shape_path.c_str(),"rb");
    current_DBF = DBFOpen(shape_path.c_str(),"rb");

    DBFGetFieldInfo(current_DBF, NULL, NULL,NULL, NULL );

    if( current_SHP == NULL || current_DBF == NULL )
    {
        qDebug()<<"Unable to open SHP or DBF files";
    }
    else
    {
        int num_vertices,nEntities,vert_iter;
        int n_Parts;
        //Get the number of entities
        SHPGetInfo(current_SHP, &nEntities, NULL, NULL, NULL);
        vector<Point2f> tempPolygon;
        Point2f tempCentroid;
        cv::Moments mu;
        centroids.clear();

        //Iterate over each entity
        for (int i = 0 ; i < nEntities ; i++)
        {
           tempPolygon.clear();
           SHPObject *psCShape;
           //extract each polygon
           psCShape = SHPReadObject(current_SHP,i);
           //extract its number of vertices
           num_vertices = psCShape->nVertices;
           n_Parts = psCShape->nParts;

           if(n_Parts == 1)
           {
               for(vert_iter = 0 ; vert_iter < num_vertices ; vert_iter++)
               {
                   Point2f tempPoint;
                   //Save coordinates X and Y into a QPoint
                   tempPoint.x = psCShape->padfX[vert_iter];
                   tempPoint.y = psCShape->padfY[vert_iter];
                   //Save a Point in a Polygon
                   tempPolygon.push_back(tempPoint);
               }
           }
           else
           {
               int* panParts = new int[n_Parts];
               panParts = psCShape->panPartStart;
               for (int ind = 0 ; ind < 1 ; ind++)//ind < n_Parts - 1
               {
                   //iterate over each part, from the first until one before the last one
                   for(int v = panParts[ind] ; v < panParts[ind+1] -1; v++)
                   {
                       Point2f tempPoint;
                       tempPoint.x = psCShape->padfX[v];
                       tempPoint.y = psCShape->padfY[v];
                       tempPolygon.push_back(tempPoint);
                   }
               }
           }

           //Calculate the centroids
           mu = cv::moments(tempPolygon,false);
           tempCentroid.x = mu.m10*1.0/mu.m00;
           tempCentroid.y = mu.m01*1.0/mu.m00;
           centroids.push_back(tempCentroid);
           //Save a polygon
           allPolygons.push_back(tempPolygon);
           //Reset a Polygon
           tempPolygon.clear();
           //Destroy the SHPObject
           SHPDestroyObject(psCShape);
        }
    }
    //Close the shape file
    SHPClose(current_SHP);
    DBFClose(current_DBF);
    //Return a vector of Polygons
    return allPolygons;
}

//Converts coordinates from shapefile generated by OTB to a format from (0,0) to (cols,rows)
void convertShapeFile(const char* new_path, const char* old_path, int rows,int cols)
{
    //objects to the SHAPE and DBF files
    SHPHandle new_SHP,old_SHP;
    DBFHandle new_DBF,old_DBF;
    int shape_type = SHPT_POLYGON;
    std::vector<int> EntitiesIndex;

    int nEntities; //number of entities

    //reading the shape file
    new_SHP = SHPCreate(new_path,shape_type);
    new_DBF = DBFCreate(new_path);
    old_SHP = SHPOpen(old_path,"rb");
    old_DBF = DBFOpen(old_path,"rb");

    char ftype[15];
    int	*panWidth;
    int	nWidth, nDecimals;
    panWidth = (int *) malloc( DBFGetFieldCount( old_DBF ) * sizeof(int) );

    for( int i = 0; i < DBFGetFieldCount(old_DBF); i++ )
    {
        char		szTitle[12];
        DBFFieldType	eType = DBFGetFieldInfo( old_DBF, i, szTitle, &nWidth, &nDecimals);

        switch ( DBFGetFieldInfo( old_DBF, i, szTitle, &nWidth, &nDecimals )) {
        case FTString:
            strcpy (ftype, "string");
            break;
        case FTInteger:
            strcpy (ftype, "integer");
            break;
        case FTDouble:
            strcpy (ftype, "float");
            break;
        case FTInvalid:
            strcpy (ftype, "invalid/unsupported");
            break;
        default:
            strcpy (ftype, "unknown");
            break;
        }
        DBFAddField(new_DBF,szTitle,eType,nWidth,nDecimals);
        int n_record2 = 0;
        for (int n_record = 0 ; n_record < DBFGetRecordCount(old_DBF) ; n_record++)
        {
            if (DBFReadIntegerAttribute(old_DBF,n_record,i) != 0)
            {
                DBFWriteIntegerAttribute(new_DBF,n_record2,i,DBFReadIntegerAttribute(old_DBF,n_record,i));
                n_record2 = n_record2 +1;
                EntitiesIndex.push_back(n_record);
                //qDebug() << n_record2;
            }
        }
    }

    if( old_SHP == NULL || old_DBF == NULL )
    {
        qDebug() <<"Unable to open SHP or DBF files";
        SHPClose(new_SHP);
        DBFClose(new_DBF);
        SHPClose(old_SHP);
        DBFClose(old_DBF);
        return;
    }
    else
    {        
        //Get the number of entities
        SHPGetInfo(old_SHP, &nEntities,NULL, NULL, NULL);

        double min_old_x = _I64_MAX, min_old_y = _I64_MAX;
        double max_old_x = 0, max_old_y = 0;

        for (int j = 0 ; j < nEntities ; j++)
        {
            SHPObject *psCShape_old;
            psCShape_old = SHPReadObject(old_SHP,j);
            int n_vertices = psCShape_old->nVertices;
            for (int k = 0 ; k < n_vertices ; k++)
            {
                if(psCShape_old->padfX[k] > max_old_x)
                {
                    max_old_x = psCShape_old->padfX[k];
                }
                if(psCShape_old->padfX[k] < min_old_x)
                {
                    min_old_x = psCShape_old->padfX[k];
                }
                if(psCShape_old->padfY[k] > max_old_y)
                {
                    max_old_y = psCShape_old->padfY[k];
                }
                if(psCShape_old->padfY[k] < min_old_y)
                {
                    min_old_y = psCShape_old->padfY[k];
                }
            }
            //Destroy the SHPObject
            SHPDestroyObject(psCShape_old);
        }
        //Iterate over each entity
        for (int i = 0 ; i < EntitiesIndex.size() ; i++)
        {
           //extract each polygon
           SHPObject *psCShape_old;
           psCShape_old = SHPReadObject(old_SHP,EntitiesIndex.at(i));
           //extract its number of vertices
           int num_vertices = psCShape_old->nVertices;

           double *pXs_new = new double[num_vertices];
           double *pYs_new = new double[num_vertices];

           for(int vert_iter = 0 ; vert_iter < num_vertices ; vert_iter++)
           {
               pXs_new[vert_iter] = (double)(psCShape_old->padfX[vert_iter] - min_old_x)*(cols)/(max_old_x - min_old_x);
               pYs_new[vert_iter] = (double)(psCShape_old->padfY[vert_iter] - min_old_y)*(rows)/(max_old_y - min_old_y);
           }
           SHPObject *psCShape_new = SHPCreateObject(shape_type,psCShape_old->nShapeId,
                                                     psCShape_old->nParts,
                                                     psCShape_old->panPartStart,
                                                     psCShape_old->panPartType,
                                                     num_vertices,
                                                     pXs_new,pYs_new,NULL,NULL);
           SHPWriteObject(new_SHP,-1,psCShape_new);
           //Destroy the SHPObject
           SHPDestroyObject(psCShape_new);
           delete[] pXs_new;
           delete[] pYs_new;
           //Destroy the SHPObject
           SHPDestroyObject(psCShape_old);
        }
    }
    //Close the shape file
    SHPClose(new_SHP);
    DBFClose(new_DBF);
    SHPClose(old_SHP);
    DBFClose(old_DBF);    
}

void flipShapeFile(const char* new_path, const char* old_path, int rows, int cols)
{
    //objects to the SHAPE and DBF files
    SHPHandle new_SHP,old_SHP;
    DBFHandle new_DBF,old_DBF;
    int shape_type = SHPT_POLYGON;

    int nEntities; //number of entities

    //reading the shape file
    new_SHP = SHPCreate(new_path,shape_type);
    new_DBF = DBFCreate(new_path);
    old_SHP = SHPOpen(old_path,"rb");
    old_DBF = DBFOpen(old_path,"rb+");

    char ftype[15];
    int	*panWidth;
    int	nWidth, nDecimals;
    panWidth = (int *) malloc( DBFGetFieldCount( old_DBF ) * sizeof(int) );

    for( int i = 0; i < DBFGetFieldCount(old_DBF); i++ )
    {
        char		szTitle[12];
        DBFFieldType	eType = DBFGetFieldInfo( old_DBF, i, szTitle, &nWidth, &nDecimals);

        switch ( DBFGetFieldInfo( old_DBF, i, szTitle, &nWidth, &nDecimals )) {
        case FTString:
            strcpy (ftype, "string");
            break;
        case FTInteger:
            strcpy (ftype, "integer");
            break;
        case FTDouble:
            strcpy (ftype, "float");
            break;
        case FTInvalid:
            strcpy (ftype, "invalid/unsupported");
            break;
        default:
            strcpy (ftype, "unknown");
            break;
        }
        DBFAddField(new_DBF,szTitle,eType,nWidth,nDecimals);
        for (int n_record = 0 ; n_record < DBFGetRecordCount(old_DBF) ; n_record++)
        {
            DBFWriteIntegerAttribute(new_DBF,n_record,i,DBFReadIntegerAttribute(old_DBF,n_record,i));
        }
    }

    if( old_SHP == NULL || old_DBF == NULL )
    {
        cout <<"Unable to open SHP or DBF files";
        return;
    }
    else
    {        
        //Get the number of entities
        SHPGetInfo(old_SHP, &nEntities,NULL, NULL, NULL);

        int num_vertices;

        //Iterate over each entity
        for (int i = 0 ; i < nEntities ; i++)
        {           
           SHPObject *psCShape_old;
           //extract each polygon
           psCShape_old = SHPReadObject(old_SHP,i);
           //extract its number of vertices
           num_vertices = psCShape_old->nVertices;

           double *pXs_new = new double[num_vertices];
           double *pYs_new = new double[num_vertices];

           for(int vert_iter = 0 ; vert_iter < num_vertices ; vert_iter++)
           {
               pXs_new[vert_iter] = (double)(psCShape_old->padfX[vert_iter]);
               pYs_new[vert_iter] = (double)(rows - psCShape_old->padfY[vert_iter]);
           }
           SHPObject *psCShape_new = SHPCreateObject(shape_type,psCShape_old->nShapeId,
                                                     psCShape_old->nParts,
                                                     psCShape_old->panPartStart,
                                                     psCShape_old->panPartType,
                                                     num_vertices,
                                                     pXs_new,pYs_new,NULL,NULL);
           SHPWriteObject(new_SHP,-1,psCShape_new);
           SHPDestroyObject(psCShape_new);
           delete[] pXs_new;
           delete[] pYs_new;
           //Destroy the SHPObject
           SHPDestroyObject(psCShape_old);
        }
    }
    //Close the shape file
    SHPClose(new_SHP);
    DBFClose(new_DBF);
    SHPClose(old_SHP);
    DBFClose(old_DBF);
}

//Overload of VectorPoints2Shape, generates a shapefil from a vector of vector of Points, coordinates are corrected.
void VectorPoints2Shape(const char* reference_path, vector< vector<Point2f> > GT,int rows, int cols)
{
    // Type of entities to be saved in the shapefile
    int shape_type = SHPT_POLYGON;
    //Creating the SHP, SHX and DBF files
    SHPHandle new_SHP = SHPCreate(reference_path, shape_type);
    DBFHandle new_DBF = DBFCreate(reference_path);
    //Iterators for entities and vertices of each entity.
    int entities_iter, vertices_iter;

    for (entities_iter = 0 ; entities_iter < GT.size() ; entities_iter++)
    {
        //Getting number of vertices
        int n_Vertices = GT[entities_iter].size();
        //arrays to save coordinates of points of each polygon
        //double pXs[n_Vertices];
        double *pXs = new double[n_Vertices];
        //double pYs[n_Vertices];
        double *pYs = new double[n_Vertices];
        for ( vertices_iter = 0 ; vertices_iter < n_Vertices ; vertices_iter++)
        {
            //Extracting coordinates X and Y from vector of vector of points
            pXs[vertices_iter] = (double)(GT[entities_iter][vertices_iter].x);
            pYs[vertices_iter] = (double)(rows - GT[entities_iter][vertices_iter].y);
        }
        //Creating the SHPObject that contains the data for 1 polygon
        SHPObject *psCShape = SHPCreateSimpleObject(shape_type,n_Vertices,pXs,pYs,NULL);
        //Writing the object in the SHPHandle
        SHPWriteObject(new_SHP,-1,psCShape);
        //Destroying the SHPObject
        SHPDestroyObject(psCShape);
        delete[] pXs;
        delete[] pYs;
    }
    //Closing the files
    SHPClose(new_SHP);
    DBFClose(new_DBF);
}

void GAFile2VectorPoints(const char* ga_file_path , vector<vector<Point2f> > &contours, int &rows, int &cols)
{
    QFile file(QString::fromUtf8(ga_file_path));
    if(!file.open(QIODevice::ReadOnly))
    {
        cout<<"Error!!"<<endl;
        return;
    }
    QTextStream iter(&file);
    int n_line = 0;
    int n_segments;
    vector< vector<Point> > Segments;//tp extract all segments
    int id_pixel;//represented taking as origin the top-left corner
    while(!iter.atEnd())
    {
        QString line = iter.readLine();
        QStringList fields = line.split(" ");
        if(n_line == 0)//First Line: #_segments rows cols
        {
            n_segments = fields.at(0).toInt();
            rows = fields.at(1).toInt();
            cols = fields.at(2).toInt();
        }
        else//Next Lines: Pixel_id is_in_borderline?
        {
            vector<Point> segment;//to extract pixels belonging to a segment
            int n_pixels = fields.size()/2;
            for (int i = 0 ; i < n_pixels; i++)
            {
                Point pixel;
                id_pixel = fields.at(2*i).toInt();
                pixel.x = id_pixel % cols;
                pixel.y = id_pixel / cols;
                segment.push_back(pixel);
            }
            Segments.push_back(segment);
        }
        n_line++;
    }
    file.close();

    for(int n_segment = 0 ; n_segment < Segments.size() ; n_segment++)
    {
        Mat mask = Mat::zeros(rows+2,cols+2,CV_8UC1);//this image is bigger to deal with image limits

        for(int n_pixel = 0 ; n_pixel < Segments[n_segment].size(); n_pixel++)
        {
            Point temp;
            temp.x = Segments[n_segment][n_pixel].x +1;
            temp.y = Segments[n_segment][n_pixel].y +1;
            mask.at<uchar>(temp) = 255;
        }
        vector< vector<Point> > contour_seg;//to extract contours of a segment
        vector<Vec4i> hierarchy;
        findContours(mask,contour_seg,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_NONE,Point(0,0));

        for(int n = 0 ; n < contour_seg.size() ; n++)
        {
            vector<Point2f> seg;
            for(int n_point = 0 ; n_point < contour_seg[n].size() ; n_point++)
            {
                Point2f temp2;
                temp2.x = contour_seg[n][n_point].x - 1;
                temp2.y = contour_seg[n][n_point].y - 1;
                temp2.x = temp2.x * cols/(cols-1);
                temp2.y = temp2.y * rows/(rows-1);
                seg.push_back(temp2);
            }
            contours.push_back(seg);
        }
    }
}

//Convert vector<vector<Point2f>> to vector<vector<Point>>
vector< vector<Point> > vectorFloat2Int(vector< vector<Point2f> > source)
{
    vector< vector<Point> > dst;
    int n_polygons = source.size();
    for (int i = 0 ; i < n_polygons ; ++i)
    {
        vector<Point> dst_tmp;
        int n_vertices = source[i].size();
        for (int j = 0 ; j < n_vertices ; ++j)
        {
            Point temp;
            temp.x = (int)source[i][j].x;
            temp.y = (int)source[i][j].y;
            dst_tmp.push_back(temp);
        }
        dst.push_back(dst_tmp);
    }
    return dst;
}

//Convert vector<Point2f> to vector<Point>
vector<Point> vectorFloat2Int(vector<Point2f> source)
{
    vector<Point> dst;
    int n_vertices = source.size();
    for (int i = 0 ; i < n_vertices ; ++i)
    {
        Point temp;
        temp.x = (int)source[i].x;
        temp.y = (int)source[i].y;
        dst.push_back(temp);
    }
    return dst;
}

//Convert Label Image into ShapeFile
void Label2Vector(const char* original_image, const char* src_filename,const char* dst_filename, bool isProjection)
{
    // original_image : image before the segmentation procedure
    // src_filename : labeled image obtained after the segmentation procedure
    // dst_filename : shapefile name for vector data

    const char* format = "ESRI Shapefile";
    const char* dst_layername = "NewLayer";
    const char* dst_fieldname = "DN";

    //Erase the shapefile if there is one with the same name
    QFile::remove(QString(dst_filename));

    // Convert RASTER to VECTOR
    //Verify if there is or not projection
    GDALAllRegister();
    OGRRegisterAll();
    GDALDatasetH src_orig = NULL;
    src_orig = GDALOpen(original_image,GA_ReadOnly);
    if (src_orig == NULL)
    {
        std::cout<<"Error opening Original image"<<std::endl;
        return;
    }

    GDALDatasetH src_ds = NULL;
    GDALRasterBandH src_band, mask_band;
    src_ds = GDALOpen(src_filename,GA_ReadOnly);
    if (src_ds == NULL)
    {
        std::cout<<"Error opening temporal Label image"<<std::endl;
        return;
    }
    src_band = GDALGetRasterBand(src_ds,1);
    mask_band = GDALGetMaskBand(src_band);

    //Create output file
    OGRSFDriverH drv = NULL;
    drv = OGRGetDriverByName(format);

    OGRDataSourceH dst_ds;
    dst_ds = OGR_Dr_CreateDataSource(drv,dst_filename,NULL);
    if (dst_ds == NULL)
    {
        std::cout<<"Error Creating dataset"<<std::endl;
        return;
    }

    //Create destination layer
    OGRLayerH dst_layer;
    //OGRGeometryH srs = NULL;
    OGRSpatialReferenceH hSRS = NULL;
    if (strcmp(GDALGetProjectionRef(src_orig),"")!=0)
    {
        char *ptr = (char*)GDALGetProjectionRef(src_orig);
        hSRS = OSRNewSpatialReference(ptr);
        //OGR_G_ImportFromWkt(srs,&ptr);
        std::cout<<"There is projection"<<std::endl;
        isProjection = true;
    }
    else
    {
         std::cout << "There is not projection"<< std::endl;
         isProjection = false;
    }

    dst_layer = OGR_DS_CreateLayer(dst_ds,dst_layername,hSRS,wkbMultiPolygon,NULL);

    OGRFieldDefnH fd;
    fd = OGR_Fld_Create(dst_fieldname,OFTInteger);
    OGR_L_CreateField(dst_layer,fd,TRUE);

    GDALPolygonize(src_band,mask_band,dst_layer,0,NULL,NULL,NULL);
    OGR_L_SyncToDisk(dst_layer);
    OGR_DS_Destroy(dst_ds);
    std::cout << "Shape File Created" << std::endl;
}

//Read information about an image
void ReadImg_Info(const char *filename, int *samples, int *lines, int *bands)
{
    //Reading input image
    GDALDataset  *poDataset;
    GDALAllRegister(); //Register all supported formats
    poDataset = (GDALDataset *) GDALOpen( filename , GA_ReadOnly );

    if( poDataset == NULL )
    {   printf("Error opening file...\n"); return;}

    *samples = poDataset->GetRasterXSize();
    *lines = poDataset->GetRasterYSize();
    *bands = poDataset->GetRasterCount();

    GDALClose(poDataset);
}
