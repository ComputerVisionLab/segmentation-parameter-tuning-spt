#include "mainwindow.h"
#include <QApplication>
#include "consolefunctions.h"

int main(int argc, char *argv[])
{

//    QApplication a(argc, argv);
//    QCoreApplication::addLibraryPath("./");
//    MainWindow w;
//    w.show();
//    return a.exec();

    if (argc == 1)
    {
        QApplication a(argc, argv);
        MainWindow w;
        w.show();
        return a.exec();
    }
    else if (argc == 2)
    {
        std::cout << "========Console mode==========\n";
        ExperimentData ConfigurationData;
        ConfigurationData = ReadConfigurationFIle(argv[1]);
        ConfigurationData.show();
        RunExperiment(ConfigurationData);
        return 1;
    }
//    else
//    {

//        std::cout << "a: " << std::endl;
//    }


}
