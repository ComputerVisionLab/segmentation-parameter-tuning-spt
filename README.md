# Segmentation Parameter Tuner (SPT)
Segmentation Parameter Tuning (SPT), is an open source software designed for automatic tuning of segmentation parameters based on a number of optimization algorithms using different quality metrics as fitness functions.
 
The current version of SPT is 3.9 which includes many features such as:
 -  Many raster formats supported (provided by  [GDAL](http://www.gdal.org/ "GDAL")).
 -  Metaheuristics available to find the best set of parameters:
	1.  Single-solution (S-metaheuristics)
		-  Generalized Pattern Search (GPS)
		- Mesh Adaptive Direct Search (MADS)
		- Nelder-Mead (NM)
	2.  Population-based (P-metaheuristics)
		 - Differential Evolution (DE)
	3.  Hybrid metaheuristics
		 -  DE + GPS
		 -  DE + MADS
		 -  DE + NM
 -  Segmentation algorithms available:
    1.  Multiresolution Region Growing
    2.  [EDISON Meanshift](http://coewww.rutgers.edu/riul/research/code/EDISON/ "EDISON Meanshift")
    3.  [Graph-based (Felzenszwalb’s Segmentation)](http://cs.brown.edu/~pff/segment/ "Graph-based (Felzenszwalb's Segmentation)")
    4.  SPRING
    5.  [MultiSeg](http://icaci.org/files/documents/ICC_proceedings/ICC2015/papers/17/641.html "MultiSeg")  (for Optical and Radar images).
 -  Metrics available:
    1.  Hoover Index
    2.  Area-Fit-Index
    3.  F-measure
    4.  Shape Index
    5.  Reference Bounded Segments Booster
    6.  Rand Index
    7.  Segmentation Covering
 -  Reference tool: Create, Edit and Save segmentation references in ESRI Shapefile format.
 -  Assessment tool: Measure the quality of a segmentation outcome (raster image or shapefile) with respect to a certain reference.

## Requirements
SPT was developed in C++ and Qt 5.4 for Windows Only (Testes on Windows 7/8/10)

## Download
[SPT 3.9](https://docs.google.com/forms/d/1qdEFJyaLpy-VQUy_IK4c38a7DY7CqWsQwCISfHhZfPA/viewform "SPT 3.9") (last update: September 1st, 2016)

[SPT 2.2](http://www.lvc.ele.puc-rio.br/downloads/arquivos_download/SPT%202.2_Setup.exe "SPT 2.2")  (Based on Genetic Algorithms)

## Documentation

[SPT User Guide 3.1.0](http://www.lvc.ele.puc-rio.br/downloads/arquivos_download/User_Guide_3.1.0.pdf "SPT User Guide 3.1.0")  (last update: June 7th)  
[SPT User Guide 2.2](http://www.lvc.ele.puc-rio.br/downloads/arquivos_download/UserGuide_SPT2.0.pdf "SPT User Guide 2.2")

## Demos

[SPT Demo 1  (TOLOMEO Project: Final Meeting)](https://vimeo.com/118866759 "SPT Demo 1 (TOLOMEO Project: Final Meeting)")  
[SPT Demo 2  (XVII SBSR: Oral Presentation)](https://youtu.be/cjxh5ebb_Hc "SPT Demo 2 (XVII SBSR: Oral Presentation)")

## License Terms

If you use the SPT Tool in any of your experiments or research that leads to a scientific publication, please cite one of the following papers:

AYMA QUIRITA, VICTOR ANDRES; ACHANCCARAY DIAZ, PEDRO; FEITOSA, RAUL Q. ; HAPP, PATRICK N.; COSTA, GILSON A. O. P.; KLINGER, TOBIAS; HEIPKE, CHRISTIAN.  [Metaheuristics for Supervised Parameter Tuning of Multiresolution Segmentation](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=7519028). IEEE Geoscience and Remote Sensing Letters (Print), v. 13(9), p. 1364 – 1368, 2016.

ACHANCCARAY, P.; AYMA, V. A.; JIMENEZ, L. I.; BERNABE, S. G.; HAPP, P. N.; FEITOSA, R. Q. ; COSTA, G. A. O. P.; PLAZA, A. J. SPT 3.1: A free software tool for automatic tuning of segmentation parameters in optical, hyperspectral and SAR images. In: IEEE International Geoscience and Remote Sensing Symposium – IGARSS 2015, 2015, Milan. IEEE International Geoscience and Remote Sensing Symposium – IGARSS 2015, 2015. p. 4332-4335.

## Development Team
- Pedro M. Achanccaray Diaz 
- Victor A. Ayma Quirita
- Patrick N. Happ
- Raul Queiroz Feitosa
