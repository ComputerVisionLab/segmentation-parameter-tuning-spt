#include "Segmentation.h"

//Constructor of class Areas_stats
Areas_stats::Areas_stats()
{
    area1 = 0;
    area2 = 0;
    intersection = 0;
    perimeter1 = 0;
    perimeter2 = 0;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------
//SEGMENTATION ALGORITHMS
//----------------------------------------------------------------------------------------------------------------------------------------------------------
//Region Growing (BAATZ) - Optical Images
void region_growing_baatz_rgb(string input_image,string output_image, int scale, float w_color, float w_compct, int rows, int cols)
{    
    //Delete previous temporal files
    QFile::remove("./result.dbf");
    QFile::remove("./result.shp");
    QFile::remove("./result.shx");
    QFile::remove("./result.prj");
    QFile::remove("./temp/result3.dbf");
    QFile::remove("./temp/result3.shp");
    QFile::remove("./temp/result3.shx");
    QFile::remove(QString(output_image.c_str()));

    //Generate .txt file for input parameters
    double band1=0.33,band2=0.33,band3=0.34;
    ofstream parameters;
    QString parameters_name = "./temp/parameters.txt";
    parameters.open(parameters_name.toStdString().c_str());
    parameters<<scale<<" "<<w_color<<" "<<w_compct<<endl;
    parameters<<band1<<" "<<band2<<" "<<band3<<endl;
    parameters.close();

    //Execute Segmentation
    string cmd = "\"./operators/SEQ_LMBF_OTB.exe\" \"./temp/parameters.txt\" \"" + input_image + "\"";
    QString executable(cmd.c_str());
    QProcess *launch_program = new QProcess();
    qDebug()<<QDir::toNativeSeparators(executable);
    launch_program->start(QDir::toNativeSeparators(executable));
    launch_program->waitForFinished(-1);
    delete[] launch_program;

    if (QFile::copy(QString("./operators/SEQ_LMBF_result.bmp"),QString(output_image.c_str())))
    {
        qDebug()<<"Result File Copied";
    }
    QFile::remove("./operators/SEQ_LMBF_result.bmp");
    //Scale shapefile
    QString new_shape = "./temp/result3.shp";
    QString old_shape = "./result.shp";
    convertShapeFile(new_shape.toStdString().c_str(),old_shape.toStdString().c_str(),rows,cols);
    QFile::remove("./result.dbf");
    QFile::remove("./result.shp");
    QFile::remove("./result.shx");
    QFile::remove("./result.prj");        
}

//Region Growing (BAATZ) - Hyperspectral Images
void region_growing_baatz_hdr(string input_image,string output_image, int scale, float w_color, float w_compct, int rows, int cols)
{
    //Delete previous temporal files
    QFile::remove(QString("./result.dbf"));
    QFile::remove(QString("./result.shp"));
    QFile::remove(QString("./result.shx"));
    QFile::remove(QString("./result.prj"));
    QFile::remove(QString("./temp/result3.dbf"));
    QFile::remove(QString("./temp/result3.shp"));
    QFile::remove(QString("./temp/result3.shx"));
    QFile::remove(QString(output_image.c_str()));

    QString input_name(input_image.c_str());
    QStringList parts = input_name.split(".");
    //Generate .txt file for input parameters
    double band1=0.33,band2=0.33,band3=0.34;
    ofstream parameters;
    QString parameters_name = "./temp/parameters.txt";
    parameters.open(parameters_name.toStdString().c_str());
    parameters<<scale<<" "<<w_color<<" "<<w_compct<<endl;
    parameters<<band1<<" "<<band2<<" "<<band3<<endl;
    parameters.close();

    //Execute Segmentation
    string cmd = "\"./operators/SEQ_LMBF_OTB.exe\" \"./temp/parameters.txt\" \"." + parts[1].toStdString() + "\" \"" + input_image + "\" \"400\" \"800\"";
    QString executable(cmd.c_str());
    QProcess *launch_program = new QProcess();
    qDebug()<<QDir::toNativeSeparators(executable);
    launch_program->start(QDir::toNativeSeparators(executable));
    launch_program->waitForFinished(-1);
    delete[] launch_program;

    if (QFile::copy(QString("./operators/SEQ_LMBF_result.bmp"),QString(output_image.c_str())))
    {
        qDebug()<<"Result File Copied";
    }
    QFile::remove(QString("./operators/SEQ_LMBF_result.bmp"));

    {
        QImage orgOutput(QString(output_image.c_str()));
        orgOutput = orgOutput.mirrored(false,true);
        orgOutput.save(QString(output_image.c_str()));
    }
    //Scale shapefile
    QString new_shape = "./temp/result2.shp";
    QString old_shape = "./result.shp";
    QString flip_shape = "./temp/result3.shp";
    convertShapeFile(new_shape.toStdString().c_str(),old_shape.toStdString().c_str(),rows,cols);
    flipShapeFile(flip_shape.toStdString().c_str(),new_shape.toStdString().c_str(),rows,cols);
    QFile::remove(QString("./result.dbf"));
    QFile::remove(QString("./result.shp"));
    QFile::remove(QString("./result.shx"));
    QFile::remove(QString("./result.prj"));
    QFile::remove(QString("./temp/result2.dbf"));
    QFile::remove(QString("./temp/result2.shp"));
    QFile::remove(QString("./temp/result2.shx"));
}

//Region Growing (BAATZ) - using GDAL
void region_growing_baatz(string input_image,string output_image, int scale, float w_color, float w_compct, int rows, int cols)
{
    //Delete previous temporal files
    QFile::remove("./result.dbf");
    QFile::remove("./result.shp");
    QFile::remove("./result.shx");
    QFile::remove("./result.prj");
    QFile::remove("./temp/result3.dbf");
    QFile::remove("./temp/result3.shp");
    QFile::remove("./temp/result3.shx");
    QFile::remove(QString(output_image.c_str()));

    //Generate .txt file for input parameters
    double band1=0.33,band2=0.33,band3=0.34;
    ofstream parameters;
    QString parameters_name = "./temp/parameters.txt";
    parameters.open(parameters_name.toStdString().c_str());
    parameters<<scale<<" "<<w_color<<" "<<w_compct<<endl;
    parameters<<band1<<" "<<band2<<" "<<band3<<endl;
    parameters.close();

    QString inputfilename(input_image.c_str());
    string cmd;
    //Execute Segmentation
    if (inputfilename.contains(".hdr"))
    {
        cmd = "\"./operators/SEQ_LMBF_GDALx64.exe\" \"./temp/parameters.txt\" \"" + inputfilename.remove(".hdr").toStdString() + "\"";
    }
    else
    {
        cmd = "\"./operators/SEQ_LMBF_GDALx64.exe\" \"./temp/parameters.txt\" \"" + input_image + "\"";
    }
    qDebug() << QDir::currentPath().toStdString().c_str();
    QString executable(cmd.c_str());
    QProcess *launch_program = new QProcess();
    qDebug()<<QDir::toNativeSeparators(executable);
    launch_program->start(QDir::toNativeSeparators(executable));
    launch_program->waitForFinished(-1);
    delete[] launch_program;

    if (QFile::copy(QString("./SEQ_LMBF_result.png"),QString(output_image.c_str())))
    {
        qDebug()<<"Result File Copied";
    }
    QFile::remove("./SEQ_LMBF_result.png");
    //Scale shapefile
    QString new_shape = "./temp/result2.shp";
    QString old_shape = "./result.shp";
    QString flip_shape = "./temp/result3.shp";
    convertShapeFile(new_shape.toStdString().c_str(),old_shape.toStdString().c_str(),rows,cols);
    flipShapeFile(flip_shape.toStdString().c_str(),new_shape.toStdString().c_str(),rows,cols);
    QFile::remove(QString("./result.dbf"));
    QFile::remove(QString("./result.shp"));
    QFile::remove(QString("./result.shx"));
    QFile::remove(QString("./result.prj"));
    QFile::remove(QString("./temp/result2.dbf"));
    QFile::remove(QString("./temp/result2.shp"));
    QFile::remove(QString("./temp/result2.shx"));
}

//Mean-Shift segmentation - EDISON
void mean_shift_edison(string input_image,string output_image, int spat_rad, int range_rad, int min_area, int rows, int cols)
{
    QFile::remove("./temp/result3.dbf");
    QFile::remove("./temp/result3.shp");
    QFile::remove("./temp/result3.shx");
    QFile::remove(QString(output_image.c_str()));
    //Execute Segmentation
    string cmd = "\"./operators/MeanShift.exe\" \"" + input_image + "\" \"" + stringify(spat_rad) + "\" \"" + stringify(range_rad) + "\" \"" + stringify(min_area) + "\" \"result1.shp\"";
    QString executable(cmd.c_str());

    QProcess *launch_program = new QProcess();
    qDebug()<<QDir::toNativeSeparators(executable);
    launch_program->start(QDir::toNativeSeparators(executable));
    launch_program->waitForFinished(-1);
    delete[] launch_program;

    QFile::copy(QString("./MShift_out.tif"),QString(output_image.c_str()));
    QFile::remove("./MShift_out.tif");
    //Scale Shapefile
    QString final_shape = "./temp/result3.shp";    
    convertShapeFile(final_shape.toStdString().c_str(),"./result1.shp",rows,cols);    
    QFile::remove("./result1.dbf");
    QFile::remove("./result1.shp");
    QFile::remove("./result1.shx");
    QFile::remove("./result1.prj");
}

void mean_shift_edison_hdr(string input_image, string output_image, int spat_rad, float range_rad, int min_area, int rows, int cols)
{
    QFile::remove("./temp/result3.dbf");
    QFile::remove("./temp/result3.shp");
    QFile::remove("./temp/result3.shx");
    if (QFile::remove(QString(output_image.c_str())))
    {
        qDebug() << "OutputFile removed";
    }
    else
    {
        qDebug() << "OutputFile not removed";
    }
    //Execute Segmentation
    string cmd = "\"./operators/EDISON.exe\" \"" + input_image + "\" \"" + stringify(spat_rad) + "\" \"" + stringify((double)range_rad) + "\" \"" + stringify(min_area) + "\"";
    QString executable(cmd.c_str());

    QProcess *launch_program = new QProcess();
    qDebug()<<QDir::toNativeSeparators(executable);
    launch_program->start(QDir::toNativeSeparators(executable));
    launch_program->waitForFinished(-1);
    delete[] launch_program;

    QFile::copy(QString("./EDISONLabeled.png"),QString(output_image.c_str()));
    if (QFile::remove("./EDISONLabeled.png"))
    {
        qDebug() << "EDISONLabeled.png removed";
    }
    else
    {
        qDebug() << "EDISONLabeled.png not removed";
    }

    //Raster to Vector
    QString final_shape = "./temp/result2.shp";
    QString flip_shape = "./temp/result3.shp";
    QFileInfo input_data(input_image.c_str());
    QString input_data_qstring = input_data.absolutePath() + "/" + input_data.baseName();
    qDebug()<<input_data_qstring.toStdString().c_str();
    bool isProjection = false;
    Label2Vector(input_data_qstring.toStdString().c_str(),output_image.c_str(),final_shape.toStdString().c_str(),isProjection);
    flipShapeFile(flip_shape.toStdString().c_str(),final_shape.toStdString().c_str(),rows,cols);
    QFile::remove("./temp/result2.dbf");
    QFile::remove("./temp/result2.shp");
    QFile::remove("./temp/result2.shx");
    QFile::remove("./temp/result2.prj");
}

//Mean-Shift segmentation - EDISON using GDAL
void mean_shift_edison_gdal(string input_image,string output_image, int spat_rad, int range_rad, int min_area, int rows, int cols)
{
    QFile::remove("./temp/result3.dbf");
    QFile::remove("./temp/result3.shp");
    QFile::remove("./temp/result3.shx");
    QFile::remove(QString(output_image.c_str()));

    QString inputfilename(input_image.c_str());
    string cmd;
    //Execute Segmentation
    if (inputfilename.contains(".hdr"))
    {
        cmd = "\"./operators/EDISON_GDALx64.exe\" \"" + inputfilename.remove(".hdr").toStdString() + "\" \"" + stringify(spat_rad) + "\" \"" + stringify(range_rad) + "\" \"" + stringify(min_area) + "\"";
    }
    else
    {
        cmd = "\"./operators/EDISON_GDALx64.exe\" \"" + input_image + "\" \"" + stringify(spat_rad) + "\" \"" + stringify(range_rad) + "\" \"" + stringify(min_area) + "\"";
    }

    QString executable(cmd.c_str());

    QProcess *launch_program = new QProcess();
    qDebug()<<QDir::toNativeSeparators(executable);
    launch_program->start(QDir::toNativeSeparators(executable));
    launch_program->waitForFinished(-1);
    delete[] launch_program;

    QFile::copy(QString("./EDISONLabeled.png"),QString(output_image.c_str()));
    QFile::remove("./EDISONLabeled.png");
    //Scale Shapefile
    QString new_shape = "./temp/result2.shp";
    QString old_shape = "./result.shp";
    QString flip_shape = "./temp/result3.shp";
    convertShapeFile(new_shape.toStdString().c_str(),old_shape.toStdString().c_str(),rows,cols);
    flipShapeFile(flip_shape.toStdString().c_str(),new_shape.toStdString().c_str(),rows,cols);
    QFile::remove(QString("./result.dbf"));
    QFile::remove(QString("./result.shp"));
    QFile::remove(QString("./result.shx"));
    QFile::remove(QString("./result.prj"));
    QFile::remove(QString("./temp/result2.dbf"));
    QFile::remove(QString("./temp/result2.shp"));
    QFile::remove(QString("./temp/result2.shx"));
}

//SPRING
void region_growing_spring(string input_image,string output_image, int min_distance, int min_area, int max_iter, int rows, int cols)
{
    QFile::remove("./temp/result3.dbf");
    QFile::remove("./temp/result3.shp");
    QFile::remove("./temp/result3.shx");
    QFile::remove(QString(output_image.c_str()));
    //Execute Segmentation
    string cmd = "\"./operators/SPRING_IT_OTB.exe\" \"" + input_image + "\" \"" + stringify(min_distance) + "\" \"" + stringify(min_area) + "\" \"" + stringify(max_iter) + "\" \"result1\"";
    QString executable(cmd.c_str());

    QProcess *launch_program = new QProcess();
    qDebug()<<QDir::toNativeSeparators(executable);
    launch_program->start(QDir::toNativeSeparators(executable));
    launch_program->waitForFinished(-1);
    delete[] launch_program;

    QFile::copy(QString("./result1pretty.tif"),QString(output_image.c_str()));
    QFile::remove("./result1pretty.tif");
    //Scale Shapefile
    QString flip_shape = "./temp/result3.shp";
    flipShapeFile(flip_shape.toStdString().c_str(),"./result1.shp",rows, cols);
    QFile::remove("./result1.dbf");
    QFile::remove("./result1.shp");
    QFile::remove("./result1.shx");
    QFile::remove("./result1.tif");
}

void region_growing_spring_gdal(string input_image,string output_image, int min_distance, int min_area, int rows, int cols)
{
    QFile::remove("./temp/result3.dbf");
    QFile::remove("./temp/result3.shp");
    QFile::remove("./temp/result3.shx");
    QFile::remove(QString(output_image.c_str()));
    //Execute Segmentation
    QString inputfilename(input_image.c_str());
    string cmd ;
    if (inputfilename.contains(".hdr"))
    {
        cmd = "\"./operators/SPRING_GDALx64.exe\" \"" + inputfilename.remove(".hdr").toStdString() + "\" \"" + stringify(min_distance) + "\" \"" + stringify(min_area) + "\"";
    }
    else
    {
        cmd = "\"./operators/SPRING_GDALx64.exe\" \"" + input_image + "\" \"" + stringify(min_distance) + "\" \"" + stringify(min_area) + "\"";
    }

    QString executable(cmd.c_str());

    QProcess *launch_program = new QProcess();
    qDebug()<<QDir::toNativeSeparators(executable);
    launch_program->start(QDir::toNativeSeparators(executable));
    launch_program->waitForFinished(-1);
    delete[] launch_program;

    QFile::copy(QString("./SPRINGLabeled.png"),QString(output_image.c_str()));
    QFile::remove("./SPRINGLabeled.png");
    //Scale Shapefile
    QString new_shape = "./temp/result2.shp";
    QString old_shape = "./result.shp";
    QString flip_shape = "./temp/result3.shp";
    convertShapeFile(new_shape.toStdString().c_str(),old_shape.toStdString().c_str(),rows,cols);
    flipShapeFile(flip_shape.toStdString().c_str(),new_shape.toStdString().c_str(),rows,cols);
    QFile::remove(QString("./result.dbf"));
    QFile::remove(QString("./result.shp"));
    QFile::remove(QString("./result.shx"));
    QFile::remove(QString("./result.prj"));
    QFile::remove(QString("./temp/result2.dbf"));
    QFile::remove(QString("./temp/result2.shp"));
    QFile::remove(QString("./temp/result2.shx"));
}

//Graph-based segmentation
void graph_based(string input_path,string output_image, double sigma, double threshold, int min_area, int rows, int cols)
{
    //Erasing temporal files
    QFile::remove("./temp/InputImage.ppm");
    QFile::remove(QString(output_image.c_str()));
    QFile::remove("./temp/result3.shp");
    QFile::remove("./temp/result3.dbf");
    QFile::remove("./temp/result3.shx");
    //Creating PPM file
    QImage *input_image = new QImage(input_path.c_str());
    const char* input_ppm_name = "./temp/InputImage.ppm";
    input_image->save(QString(input_ppm_name));
    delete[] input_image;

    //Execute Segmentation
    string cmd = "\"operators/GraphSeg2.exe\" \"" + stringify(sigma) + "\" \"" + stringify(threshold) + "\" \"" + stringify(min_area) + "\" \"" + input_ppm_name + "\" \"OutputImage.ppm\"";
    QString executable(cmd.c_str());

    QProcess *launch_program = new QProcess();
    qDebug()<<QDir::toNativeSeparators(executable);
    launch_program->start(QDir::toNativeSeparators(executable));
    launch_program->waitForFinished(-1);
    delete[] launch_program;

    QImage *result = new QImage(QString("./OutputImage.ppm"));
    result->save(QString(output_image.c_str()));
    delete[] result;

    QFile::remove("./OutputImage.ppm");
    //Scale Shapefile
    QString final_shape = "./temp/result2.shp";
    QString flip_shape = "./temp/result3.shp";
    bool isProjection = false;
    Label2Vector(input_path.c_str(),output_image.c_str(),final_shape.toStdString().c_str(),isProjection);
    flipShapeFile(flip_shape.toStdString().c_str(),final_shape.toStdString().c_str(),rows,cols);
    //Erasing temporal files
    QFile::remove("./temp/result2.shp");
    QFile::remove("./temp/result2.dbf");
    QFile::remove("./temp/result2.shx");
    QFile::remove("./temp/InputImage.ppm");
}

//Graph-based segmentation - Hyperspectral Images
void graph_based_hdr(string input_path,string output_image, double sigma, double threshold, int min_area, int rows, int cols)
{
    //Erasing temporal files
    QFile::remove("./temp/InputImage.ppm");
    QFile::remove(QString(output_image.c_str()));
    QFile::remove("./temp/result3.shp");
    QFile::remove("./temp/result3.dbf");
    QFile::remove("./temp/result3.shx");

    //Execute Segmentation
    string cmd = "\"operators/GraphSeg2.exe\" \"" + stringify(sigma) + "\" \"" + stringify(threshold) + "\" \"" + stringify(min_area) + "\" \"" + input_path.c_str() + "\" \"OutputImage.ppm\"";
    QString executable(cmd.c_str());

    QProcess *launch_program = new QProcess();
    qDebug()<<QDir::toNativeSeparators(executable);
    launch_program->start(QDir::toNativeSeparators(executable));
    launch_program->waitForFinished(-1);
    delete[] launch_program;

    QImage *result = new QImage(QString("./OutputImage.ppm"));
    result->save(QString(output_image.c_str()));
    delete[] result;

    QFile::remove("./OutputImage.ppm");
    //Scale Shapefile
    QString final_shape = "./temp/result2.shp";
    QString flip_shape = "./temp/result3.shp";
    QFileInfo input_data(input_path.c_str());
    QString input_data_qstring = input_data.absolutePath() + "/" + input_data.baseName();
    bool isProjection = false;
    Label2Vector(input_data_qstring.toStdString().c_str(),output_image.c_str(),final_shape.toStdString().c_str(),isProjection);
    flipShapeFile(flip_shape.toStdString().c_str(),final_shape.toStdString().c_str(),rows,cols);
    //Erasing temporal files
    QFile::remove("./temp/result2.shp");
    QFile::remove("./temp/result2.dbf");
    QFile::remove("./temp/result2.shx");    
}

// Graph based segmentation using GDAL
void graph_based_gdal(string input_path,string output_image, double sigma, double threshold, int min_area, int rows, int cols)
{
    QFile::remove("./temp/result3.dbf");
    QFile::remove("./temp/result3.shp");
    QFile::remove("./temp/result3.shx");
    QFile::remove(QString(output_image.c_str()));
    //Execute Segmentation
    QString inputfilename(input_path.c_str());
    string cmd ;
    if (inputfilename.contains(".hdr"))
    {
        cmd = "\"./operators/GraphSeg_GDALx64.exe\" \"" + inputfilename.remove(".hdr").toStdString() + "\" \"" + stringify(sigma) + "\" \"" + stringify(threshold) + "\" \"" + stringify(min_area) + "\"";
    }
    else
    {
        cmd = "\"./operators/GraphSeg_GDALx64.exe\" \"" + input_path + "\" \"" + stringify(sigma) + "\" \"" + stringify(threshold) + "\" \"" + stringify(min_area) + "\"";
    }

    QString executable(cmd.c_str());

    QProcess *launch_program = new QProcess();
    qDebug()<<QDir::toNativeSeparators(executable);
    launch_program->start(QDir::toNativeSeparators(executable));
    launch_program->waitForFinished(-1);
    delete[] launch_program;

    QFile::copy(QString("./GraphSeGLabeled.png"),QString(output_image.c_str()));
    QFile::remove("./GraphSeGLabeled.png");
    //Scale Shapefile
    QString new_shape = "./temp/result2.shp";
    QString old_shape = "./result.shp";
    QString flip_shape = "./temp/result3.shp";
    convertShapeFile(new_shape.toStdString().c_str(),old_shape.toStdString().c_str(),rows,cols);
    flipShapeFile(flip_shape.toStdString().c_str(),new_shape.toStdString().c_str(),rows,cols);
    QFile::remove(QString("./result.dbf"));
    QFile::remove(QString("./result.shp"));
    QFile::remove(QString("./result.shx"));
    QFile::remove(QString("./result.prj"));
    QFile::remove(QString("./temp/result2.dbf"));
    QFile::remove(QString("./temp/result2.shp"));
    QFile::remove(QString("./temp/result2.shx"));
}

//MultiSeg
void multi_seg(string input_image, string output_image, int image_model, int image_type, int radar_format, int levels, double simil,int conf,int min_area, int n_elo, string bands, int rows, int cols)
{
    QFile::remove(QString(output_image.c_str()));
    QFile::remove("./temp/result3.shp");
    QFile::remove("./temp/result3.dbf");
    QFile::remove("./temp/result3.shx");
    //IMAGE MODEL
    string img_model = "";
    switch (image_model)
    {
    case 0:
        img_model = "Cartoon";
        break;
    default:
        break;
    }
    //IMAGE TYPE
    string img_type = "";
    switch (image_type)
    {
    case 0:
        img_type = "Radar";
        break;
    case 1:
        img_type = "Optical";
        break;
    default:
        break;
    }
    //RADAR FORMAT
    string rad_format = "";
    if (strcmp(img_type.c_str(),"Radar")==0)
    {
        switch (radar_format)
        {
        case 0:
            rad_format = "Amplitude";
            break;
        case 1:
            rad_format = "Intensity";
            break;
        default:
            break;
        }
    }
    else
    {
        rad_format = "";
    }
    //Conf: 0.80(1), 0.85(2), 0.90(3), 0.95(4), 0.99(5), 0.995(6), 0.999(7)
    double confiability = 0;
    switch (conf)
    {
    case 1:
        confiability = 0.80;
        break;
    case 2:
        confiability = 0.85;
        break;
    case 3:
        confiability = 0.90;
        break;
    case 4:
        confiability = 0.95;
        break;
    case 5:
        confiability = 0.99;
        break;
    case 6:
        confiability = 0.995;
        break;
    case 7:
        confiability = 0.999;
        break;
    default:
        break;
    }

    string cmd = "\"./MultiSeg/mseg-seg2.exe\" \"" + input_image + "\" \"result1\" \"" + img_model + "\" \"" + img_type + "\" \"" + rad_format + "\" \"" + stringify(levels) + "\" \"" + stringify(simil) + "\" \"" + stringify(confiability) + "\" \"" + stringify(min_area) + "\" \"" + stringify(n_elo) + "\" \"" + bands + "\"";
    QString executable(cmd.c_str());
    QProcess *launch_program = new QProcess();
    qDebug()<<QDir::toNativeSeparators(executable);
    launch_program->start(QDir::toNativeSeparators(executable));
    launch_program->waitForFinished(-1);
    delete[] launch_program;
    QFile::copy("./result1pretty_image.jpg",QString(output_image.c_str()));
    QFile::remove("./result1pretty_image.jpg");
    QFile::copy("./result1_vector_level_0.dbf","./temp/result3.dbf");
    QFile::copy("./result1_vector_level_0.shp","./temp/result3.shp");
    QFile::copy("./result1_vector_level_0.shx","./temp/result3.shx");
    QFile::remove("./result1_vector_level_0.dbf");
    QFile::remove("./result1_vector_level_0.shp");
    QFile::remove("./result1_vector_level_0.shx");
}


void multi_seg2(string input_image, string output_image, int image_model, int image_type, int radar_format, int levels, double simil, int conf, int min_area, int n_elo, double coeff_variation, string bands, int rows, int cols)
{
    QFile::remove(QString(output_image.c_str()));
    QFile::remove("./temp/result3.shp");
    QFile::remove("./temp/result3.dbf");
    QFile::remove("./temp/result3.shx");
    //IMAGE MODEL
    string img_model = "";
    switch (image_model)
    {
    case 0:
        img_model = "Cartoon";
        break;
    default:
        img_model = "Cartoon";
        break;
    }
    //IMAGE TYPE
    string img_type = "";
    switch (image_type)
    {
    case 0:
        img_type = "Radar";
        break;
    case 1:
        img_type = "Optical";
        break;
    default:
        img_type = "Radar";
        break;
    }
    //RADAR FORMAT
    string rad_format = "";
    switch (radar_format)
    {
    case 0:
        rad_format = "Amplitude";
        break;
    case 1:
        rad_format = "Intensity";
        break;
    default:
        rad_format = "Amplitude";
        break;
    }

    //Conf: 0.80(1), 0.85(2), 0.90(3), 0.95(4), 0.99(5), 0.995(6), 0.999(7)
    double confiability = 0;
    switch (conf)
    {
    case 1:
        confiability = 0.80;
        break;
    case 2:
        confiability = 0.85;
        break;
    case 3:
        confiability = 0.90;
        break;
    case 4:
        confiability = 0.95;
        break;
    case 5:
        confiability = 0.99;
        break;
    case 6:
        confiability = 0.995;
        break;
    case 7:
        confiability = 0.999;
    case 8:
        confiability = 1.0;
        break;
    default:
        break;
    }
    //using multiseg from 17-08-2016
    string cmd = "\"./MultiSeg/mseg-seg2.exe\" \"" + input_image + "\" \"result1\" \"" + img_model + "\" \"" + img_type + "\" \"" + rad_format + "\" \"" + stringify(levels) + "\" \"" + stringify(simil) + "\" \"" + stringify(confiability) + "\" \"" + stringify(min_area) + "\" \"" + stringify(n_elo) + "\" \"" + stringify(coeff_variation) + "\" \"" + bands + "\"";
    QString executable(cmd.c_str());
    QProcess *launch_program = new QProcess();
    qDebug()<<QDir::toNativeSeparators(executable);
    launch_program->start(QDir::toNativeSeparators(executable));
    launch_program->waitForFinished(-1);
    delete[] launch_program;
    QFile::copy("./result1pretty_image.jpg",QString(output_image.c_str()));
    QFile::remove("./result1pretty_image.jpg");
    QFile::copy("./result1_vector_level_0.dbf","./temp/result3.dbf");
    QFile::copy("./result1_vector_level_0.shp","./temp/result3.shp");
    QFile::copy("./result1_vector_level_0.shx","./temp/result3.shx");
    QFile::remove("./result1_vector_level_0.dbf");
    QFile::remove("./result1_vector_level_0.shp");
    QFile::remove("./result1_vector_level_0.shx");
}

//Use the selected Segmentation
void evaluate_segmentation(string input_image, string output_image, double* vars, double* fixed, int rows, int cols, int index, string bands_string)
{
    // Segmentation:
    // 0: Region Growing (Baatz)
    // 1: MeanShift
    // 2: SPRING
    // 3: Graph-based
    // 4: MultiSeg
    QString input_path(input_image.c_str());    
    switch (index)
    {
    case 0://Region Growing (Baatz)
        region_growing_baatz(input_image,output_image,(int)vars[0],(float)vars[1],(float)vars[2],rows,cols);
        break;
    case 1://MeanShift
        mean_shift_edison_gdal(input_image,output_image,(int)vars[0],(float)vars[1],(int)vars[2],rows,cols);
        break;
    case 2://SPRING
        region_growing_spring_gdal(input_image,output_image,(int)vars[0],(int)vars[1],rows,cols);
        break;
    case 3://Graph-based
        graph_based_gdal(input_image,output_image,(double)vars[0],(double)vars[1],(int)vars[2],rows,cols);
        break;
    case 4://MultiSeg
        //qDebug() << QString("Img.Model: %1  Img.Type: %2 RadarFormat: %3 n_elo: %4").arg(fixed[0]).arg(fixed[1]).arg(fixed[2]).arg(fixed[3]);
        //multi_seg(input_image,output_image,
        if(fixed[1] == 0)//Radar
        {
            multi_seg2(input_image,output_image,
                      (int)fixed[0],//Image Model: Cartoon
                      (int)fixed[1],//Image Type: Radar, Optical
                      (int)fixed[2],//Radar Format: Intensity, Amplitude
                      (int)vars[0],//Levels:1,2,3,4,5
                      ((int)((double)vars[1]/0.2))*0.2,//Simil: 0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8
                      (int)vars[2],//Conf: 0.80(1), 0.85(2), 0.90(3), 0.95(4), 0.99(5), 0.995(6), 0.999(7), 1.0(8)
                      (int)vars[3],//Min Area: <2,100>
                      (int)vars[4],//n_elo: depends on each image
                      0,//Coefficient of Variation
                      bands_string,//bands to be processed [0,1,2,...]
                      rows,cols);
        }
        else if(fixed[1] == 1)//Optical
        {
            multi_seg2(input_image,output_image,
                      (int)fixed[0],//Image Model: Cartoon
                      (int)fixed[1],//Image Type: Radar, Optical
                      (int)fixed[2],//Radar Format: Intensity, Amplitude
                      (int)vars[0],//Levels:1,2,3,4,5
                      ((int)((double)vars[1]/0.2))*0.2,//Simil: 0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8
                      (int)vars[2],//Conf: 0.80(1), 0.85(2), 0.90(3), 0.95(4), 0.99(5), 0.995(6), 0.999(7), 1.0(8)
                      (int)vars[3],//Min Area: <2,100>
                      fixed[3],//n_elo: depends on each image
                      vars[4],//Coefficient of Variation
                      bands_string,//bands to be processed [0,1,2,...]
                      rows,cols);
        }

        break;
    default:
        break;
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------
//METRICS
//----------------------------------------------------------------------------------------------------------------------------------------------------------
//Hoover Index [Hoover, 1996]
float Hoover_index(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, float threshold,int rows, int cols)
{
    int times = 0;
    float result = -1;
    omp_set_num_threads(NUM_THREADS);

    #pragma omp parallel for reduction(+:times)
    for (int i = 0 ; i < GT.size() ; i++)
    {
        int posMS = 100000;
        bool correct = correct_detection(i,GT,MS,rows,cols,threshold,posMS);
        if (correct)
        {
            times++;
        }
        else{}
    }
    result = 1.0 - (times*1.0)/GT.size();//According to Jian et al.(2006)
    return result;
}

//Area Fit Index [Lucieer, 2004]
float Area_Fit_Index(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT,int rows, int cols)
{
    float result = -1;
    bool state = 0;
    int Areference = 0, Alargest =0;
    float AFI = 0;

    omp_set_num_threads(NUM_THREADS);
    #pragma omp parallel for reduction(+:AFI,state)
    for (int i = 0 ; i < GT.size() ; i++)
    {
        Areas_stats areas_temp;
        bool correct = largest_overlapping(i,GT,MS,rows,cols,MIN_PERCENTAGE,areas_temp);
        if(correct)
        {
            Areference =  areas_temp.area1;
            Alargest = areas_temp.area2;
            AFI = AFI + (abs(Areference - Alargest))*1.0/Areference;
            state = state + 1;
        }
    }

    if(state == 0)
    {
        result = 1;//There were any segment that overlaps more than the half of the reference segment
    }
    else
    {
        result = (AFI*1.0)/GT.size();
    }
    return result;
}

//Segmentation Covering [Arbelaez et al.,2011]
float Segmentation_Covering(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT,int rows, int cols)
{
    float result = 0, overlap = 0;
    int total_area = 0;

    omp_set_num_threads(NUM_THREADS);
    #pragma omp parallel for reduction(+:total_area)
    for (int i = 0 ; i < GT.size() ; i++)
    {
        Areas_stats areas_temp;
        bool correct = largest_overlapping(i,GT,MS,rows,cols,MIN_PERCENTAGE,areas_temp);
        if(correct)
        {
            overlap = overlap + areas_temp.area1*(areas_temp.intersection*1.0)/(areas_temp.area1+areas_temp.area2-areas_temp.intersection);
            total_area = total_area + areas_temp.area1;
        }
    }
    if(total_area != 0)
    {
        result = 1 - (overlap*1.0)/total_area;
    }
    else
    {
        result = 1;
    }
    return result;
}

//Reference Bounded Segments Booster [Feitosa, 2006]
float RBSB(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols)
{    
    float result = 0, symetric_difference = 0;
    int n_segments = 0;

    omp_set_num_threads(NUM_THREADS);
    #pragma omp parallel for reduction(+:symetric_difference)
    for (int i = 0 ; i < GT.size() ; i++)
    {
        Areas_stats areas_temp;
        bool correct = largest_overlapping(i,GT,MS,rows,cols,MIN_PERCENTAGE,areas_temp);
        if(correct)
        {
            symetric_difference = symetric_difference + (areas_temp.area1+areas_temp.area2-2*areas_temp.intersection)*1.0/(areas_temp.area2);
            n_segments++;
        }
    }
    if (n_segments != 0)
    {
        result = (symetric_difference*1.0)/GT.size();
    }
    else
    {
        result = 1;
    }
    return result;
}

//Shape Index (Average Difference between them) [Neubert, 2008]
float Shape_Index(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT,int rows, int cols)
{
    float result = 0, S_I = 0,S_I_2 = 0;
    int n_segments = 0;

    for (int i = 0 ; i < GT.size() ; i++)
    {
        Areas_stats areas_temp;
        bool correct = overlap_area_perimeter(i,GT,MS,rows,cols,MIN_PERCENTAGE,areas_temp,true);
        if(correct)
        {
            S_I = S_I + abs((areas_temp.perimeter1*1.0)/(4*sqrt(1.0*areas_temp.area1)) - (areas_temp.perimeter2*1.0)/(4*sqrt(1.0*areas_temp.area2)));
            n_segments++;
        }
        S_I_2 = S_I_2 + abs((areas_temp.perimeter1*1.0)/(4*sqrt(1.0*areas_temp.area1)));
    }
    if (n_segments != 0)
    {
        result = (S_I*1.0)/(S_I_2*GT.size());
    }
    else
    {
        result = 1;
    }
    return result;
}

//Rand Index (PRI) [Pont Tuset, 2013]
float PRI(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT,int rows, int cols)
{        
    float result = 0;
    float n, c, d, a, RI = 0;
    int n_segments=0;

    omp_set_num_threads(NUM_THREADS);
    #pragma omp parallel for reduction(+:RI)
    for (int i = 0 ; i < GT.size() ; i++)
    {
        Areas_stats areas_temp;
        bool correct = overlap_area_perimeter(i,GT,MS,rows,cols,MIN_PERCENTAGE,areas_temp,false);
        if(correct)
        {
            n = areas_temp.area1 + areas_temp.area2 - areas_temp.intersection;
            n = (n*(n-1)*1.0)/2;
            c = areas_temp.area1 - areas_temp.intersection;
            c = (c*(c-1)*1.0)/2;
            d = areas_temp.area2 - areas_temp.intersection;
            d = (d*(d-1)*1.0)/2;
            a = n-c-d;
            RI = RI + (a*1.0)/n;
            n_segments++;
        }
    }
    if (n_segments != 0)
    {
        result = 1 - (RI*1.0)/GT.size();
    }
    else
    {
        result = 1;
    }
    return result;
}

//Precision and Recall
float Precision_Recall(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT,float &P, float &R,int rows, int cols)
{    
    P = 0; R = 0;
    float P_1 = 0,R_1 = 0;
    float result = 0;
    float n, c, d, a, F = 0, alfa = 0.5;
    float Pi,Ri;
    int n_segments=0;

    omp_set_num_threads(NUM_THREADS);
    #pragma omp parallel for reduction(+:F,P_1,R_1)
    for (int i = 0 ; i < GT.size() ; i++)
    {
        Areas_stats areas_temp;
        bool correct = overlap_area_perimeter(i,GT,MS,rows,cols,MIN_PERCENTAGE,areas_temp,false);
        if(correct)
        {
            n = areas_temp.area1 + areas_temp.area2 - areas_temp.intersection;
            n = (n*(n-1)*1.0)/2;
            c = areas_temp.area1 - areas_temp.intersection;
            c = (c*(c-1)*1.0)/2;
            d = areas_temp.area2 - areas_temp.intersection;
            d = (d*(d-1)*1.0)/2;
            a = n-c-d;
            P_1 = P_1 + (a*1.0)/(a+c);
            R_1 = R_1 + (a*1.0)/(a+d);
            Pi = (a*1.0)/(a+c);
            Ri = (a*1.0)/(a+d);
            F = F + (Pi*Ri*1.0)/(alfa*Ri+(1-alfa)*Pi);
            n_segments++;
        }        
    }
    if (n_segments != 0)
    {
        P = (P_1*1.0)/GT.size();
        R = (R_1*1.0)/GT.size();
        result = 1 - (F*1.0)/GT.size();
    }
    else
    {
        result = 1;
    }
    return result;
}

//Use the selected metric:
float evaluate_metric(vector<vector<Point2f> > MS, vector<vector<Point2f> >GT, int index, int rows, int cols)
{
    // Metrics:
    // 1: Hoover metric
    // 2: Area-Fit-Index
    // 3: Shape Index
    // 4: Rand Index
    // 5: Precision and Recall
    // 6: Segmentation Covering
    // 7: RBSB
    float result = -1;
    float P = -1, R = -1;
    switch (index)
    {
    case 0:
        result = Hoover_index(MS,GT,0.8,rows,cols);
        break;
    case 1:
        result = Area_Fit_Index(MS,GT,rows,cols);
        break;
    case 2:
        result = Shape_Index(MS,GT,rows,cols);
        break;
    case 3:
        result = PRI(MS,GT,rows,cols);
        break;
    case 4:
        result = Precision_Recall(MS,GT,P,R,rows,cols);
        break;
    case 5:
        result = Segmentation_Covering(MS,GT,rows,cols);
        break;
    case 6:
        result = RBSB(MS,GT,rows,cols);
        break;
    default:
        break;
    }
    return result;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------
//OTHER FUNCTIONS
//----------------------------------------------------------------------------------------------------------------------------------------------------------
//Find the intersection between two contours
Areas_stats AreaofIntersection(vector<vector<Point2f> > contour1 , vector<vector<Point2f> > contour2,int pos1, int pos2, int rows, int cols, bool show,bool perimeter)
{
    vector<Point> contour1_int;
    vector<Point> contour2_int;
    vector<vector<Point> > contour1_int_all;
    vector<vector<Point> > contour2_int_all;
    contour1_int = vectorFloat2Int(contour1[pos1]);
    contour2_int = vectorFloat2Int(contour2[pos2]);
    contour1_int_all.push_back(contour1_int);
    contour2_int_all.push_back(contour2_int);
    Areas_stats result;

    Mat mask1 = Mat::zeros(rows,cols,CV_8UC1);
    Mat mask2 = Mat::zeros(rows,cols,CV_8UC1);

    drawContours(mask1,contour1_int_all,0,Scalar(255),CV_FILLED);
    drawContours(mask2,contour2_int_all,0,Scalar(255),CV_FILLED);

    Mat mask_and = Mat::zeros(rows,cols,CV_8UC1);
    bitwise_and(mask1,mask2,mask_and);

    result.area1 = countNonZero(mask1);
    result.area2 = countNonZero(mask2);
    result.intersection = countNonZero(mask_and);

    if (perimeter)
    {
        Mat mask3 = Mat::zeros(rows,cols,CV_8UC1);
        Mat mask4 = Mat::zeros(rows,cols,CV_8UC1);

        drawContours(mask3,contour1_int_all,0,Scalar(255),1);
        drawContours(mask4,contour2_int_all,0,Scalar(255),1);

        result.perimeter1 = countNonZero(mask3);
        result.perimeter2 = countNonZero(mask4);
    }
    if(show)
    {
        imshow("Mask1",mask1);
        imshow("Mask2",mask2);
        imshow("Mask_and",mask_and);
        waitKey(0);
    }
    return result;
}

//Get the area and perimeter of the reference with its largest segment that overlaps it
bool overlap_area_perimeter(int posMS, vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols, float threshold, Areas_stats &areas_temp, bool perimeter)
{
    bool result = false;
    int Omn = 0, Pm = 0, Pn = 0;
    int max_overlap_area = 0, id_max_overlap = 0;

    for (unsigned int i=0 ; i < GT.size() ; i++)
    {
        areas_temp = AreaofIntersection(MS,GT,posMS,i,rows,cols,false,perimeter);
        if (areas_temp.intersection != 0)
        {
            if(areas_temp.intersection > max_overlap_area)
            {
                max_overlap_area = areas_temp.intersection;
                id_max_overlap = i;
            }
        }
    }    
    areas_temp = AreaofIntersection(MS,GT,posMS,id_max_overlap,rows,cols,false,perimeter);
    result = true;
    return result;
}

//Get the largest overlaping
bool largest_overlapping(int posMS, vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols, float threshold, Areas_stats &areas_temp)
{
    bool result = 0;
    int Omn = 0, Pm = 0, Pn = 0;
    int max_overlap_area = 0, id_max_overlap = 0;

    for ( int i=0 ; i < GT.size() ; i++)
    {
        areas_temp = AreaofIntersection(MS,GT,posMS,i,rows,cols,false,false);
        if (areas_temp.intersection != 0)
        {
            if(areas_temp.intersection > max_overlap_area)
            {
                max_overlap_area = areas_temp.intersection;
                id_max_overlap = i;
            }
        }
    }
    areas_temp = AreaofIntersection(MS,GT,posMS,id_max_overlap,rows,cols,false,false);
    result = true;
    return result;
}

//Analyze if there is a correct detection between two contours
bool correct_detection(int posMS, vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols, float threshold, int &posGT)
{
    bool result = 0;
    Areas_stats areas_temp;
    int Omn = 0, Pm = 0, Pn = 0;

    for (int i=0 ; i < GT.size() ; i++)
    {
        areas_temp = AreaofIntersection(MS,GT,posMS,i,rows,cols,false,false);
        if (areas_temp.intersection != 0)
        {
            Omn = areas_temp.intersection;
            Pm = areas_temp.area1;
            Pn = areas_temp.area2;

            if (((Omn*1.0/Pm)>=threshold)&&((Omn*1.0/Pn)>=threshold))
            {
                result = 1;
                posGT = i;
                break;
            }
        }        
    }
    return result;
}

//Gets all overlapping contours between two vector of contours
void getAllOverlapping(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT,int rows, int cols, vector<int> &GT_index, vector<vector<int> > &MS_index)
{
    for (unsigned int i = 0 ; i < GT.size() ; i++)
    {
        vector<int> MS_temp;
        for (unsigned int j = 0 ; j < MS.size() ; j++)
        {
            Areas_stats area_temp;
            area_temp = AreaofIntersection(GT,MS,i,j,rows, cols,false,false);
            if (j == 0)
            {
                //add one element to each vector to avoid empty vectors
                MS_temp.push_back(-1);
            }
            if(area_temp.intersection > 10 )
            {
                MS_temp.push_back(j);
            }            
        }        
        GT_index.push_back(i);
        MS_index.push_back(MS_temp);        
        MS_temp.clear();
    }    
}

//Gets all the contours that overlaps to one specific contour, if there is any, it only have one element, index -1
void getOverlaps(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols, int posMS, vector<int> &GT_index)
{
    for (unsigned int j = 0 ; j < GT.size() ; j++)
    {
        Areas_stats area_temp;
        area_temp = AreaofIntersection(MS,GT,posMS,j,rows,cols,false,false);
        if (j == 0)
        {
            //add one element to each vector to avoid empty vectors
            GT_index.push_back(-1);
        }
        if(area_temp.intersection != 0 )
        {
            GT_index.push_back(j);            
        }
    }    
}

//Convert a PLM file to any other image format
unsigned char read_plm(string f_input, string f_output)
{
   unsigned char estatus=7, a=6, b=5, c=3;

   QString filename(f_input.c_str());
   QString output_file(f_output.c_str());

   if (filename.isEmpty())
   { estatus = estatus & a;}

   QString _file;
   _file=filename;
   QFile file(filename);

   if (!file.open(QIODevice::ReadOnly))
   { estatus = estatus & b;}

   char buf[1024];

   //Reading image type
   file.readLine(buf,sizeof(buf));

   QString type=QString(buf).simplified().left(2);
   QString _endianness;

   if ((type=="F1") || (type=="F5"))
   {
       int count=0, cols=0, rows=0;
       double min=0, max=0;

       while (count < 3)
       {   file.readLine(buf, sizeof(buf));
           QString str = QString(buf).simplified();

           if (str.left(1) != "#")
           {   count++;
               if (count == 1)
               {   QString endianness = str.left(1);
                   if (endianness == "L")
                       _endianness = "LITTLE";
                   else
                       _endianness = "BIG";
               }
               else if (count == 2)
               {   QStringList list1 = str.split(" ");
                   cols = list1.at(0).toInt();
                   rows = list1.at(1).toInt();
               }
               else if (count == 3)
               {   QStringList list2 = str.split(" ");
                   min = list2.at(0).toInt();
                   max = list2.at(1).toInt();
               }
           }
       }

       int _sizeX,_sizeY, _minValue, _maxValue;

       _sizeX = cols; _sizeY = rows;
       _minValue = int(min); _maxValue = int(max);

       QDataStream data(&file);

       if (_endianness == "LITTLE")
           data.setByteOrder(QDataStream::LittleEndian);

       qint32 a;
       int c1 = 0, i = 0, j = 0;

       cv::Mat M = cv::Mat::zeros(_sizeY,_sizeX, CV_32F);

       while (!data.atEnd())
       {
           data >> a;
           c1++;

           j=(c1%(_sizeX+1))-1;
           M.at<float>(i,j)=(a-float(_minValue))/float(_maxValue); //Normalization

           if(c1==_sizeX)
           {   c1=c1-_sizeX;
               i++;
           }
       }
       M=M*255.;
       cv::imwrite(output_file.toUtf8().constData(), M);
   }
   else
   { estatus = estatus & c;}
   file.close();
   return estatus;
}

//Convert double/int to string
string stringify(double x)
 {
   std::ostringstream o;
  o << x;
   return o.str();
 }
string stringify(int x)
 {
   std::ostringstream o;
  o << x;
   return o.str();
 }
