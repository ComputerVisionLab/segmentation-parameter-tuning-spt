#ifndef POLYGON_CANVAS_H
#define POLYGON_CANVAS_H

#include <QPolygon>
#include <QWidget>
#include <QtGui>
#include <QList>

class Polygon2 : private QPolygonF
{
public:
    Polygon2();
    ~Polygon2();

    void draw(QPainter& painter) const;
    void print(QPainter& painter, const QColor& color) const;

    void close();
    void activate();
    void deactivate();

    inline bool isClosed() const { return m_isClosed; }
    inline bool isActive() const { return m_isActive; }

    bool insertPoint(const QPointF& point);
    void selectPoint(int index);
    void removeActivePoint();
    void moveActivePoint(const QPointF& position, const QPointF& old);
    int getIndexAtPoint(const QPointF& point, int threshold) const;

    using QPolygonF::at;
    using QPolygonF::containsPoint;
    using QPolygonF::size;
    using QPolygonF::append;

private:
    int getActiveIndex() const;

    int m_activePoint;
    bool m_isClosed;
    bool m_isActive;
};

/*****************************************************************************/

class QImage;

class PolygonCanvas : public QWidget
{
    friend class PolygonCanvasProxy;
    Q_OBJECT
public:
    PolygonCanvas(QWidget* parent = 0);

    void setAsProxy(PolygonCanvas* dataCanvas);

    enum Tool {
        TOOL_NONE = 0,
        TOOL_ZOOM,
        TOOL_PAN,
        TOOL_CREATE,
        TOOL_EDIT,
        TOOL_DELETE
    };
    void selectTool(Tool tool);

    void setImage(QImage* image, bool updatePosition = true);
    void updatePosition();
    void setGeoCoordinates(double west, double north, double east, double south);
    bool saveRegions(const QString& fileName);
    bool saveShapes(const QString& fileName);
    bool loadShapes(const QString& fileName);
    void clearRegions();
    bool hasShapes() const;
    double getGeoWest();
    double getGeoNorth();
    double getGeoEast();
    double getGeoSouth();

public slots:
    void selectTool(int tool);
    void zoomIn();
    void zoomOut();

protected:
    void paintEvent(QPaintEvent* event);
    void paintEvent(QPaintEvent* event, QPainter& painter) const;
    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void mouseDoubleClickEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);

protected:
    bool selectAtPoint(const QPointF& point);
    void insertAtPoint(const QPointF& point);
    void removeAtPoint(const QPointF& point);
    void closeActivePolygon();
    void moveActivePoint(const QPointF& position, const QPointF& old);
    void removeActivePoint();

    struct DataSet {
        QImage* m_image;
        QList<Polygon2> m_polygons;
        Polygon2* m_activePolygon;
        int m_activeIndex;
    } *m_dataSet;

private:
    QRect getViewArea() const;
    QPoint imageToWindowCoords(const QPointF& point) const;
    QPointF windowToImageCoords(const QPoint& point) const;
    void insertPolygon();
    void removePolygon(int index);
    void selectPolygon(int index);
    void deselectPolygon();
    int getIndexAtPoint(const QPointF& point);
    void updateScaling();
    void clearNonPolygons();

    bool m_dataOwner;

    double m_west;
    double m_north;
    double m_east;
    double m_south;

    Tool m_currentTool;
    QPoint m_eventStart;
    QPoint m_eventEnd;
    bool m_eventActive;

    QPointF m_viewCenter;
    float m_scaleFactor;
};

#endif
