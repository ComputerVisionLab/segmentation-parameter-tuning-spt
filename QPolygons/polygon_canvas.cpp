#include "polygon_canvas.h"
#include "shapefil.h"

#include <algorithm>
#include <QPainter>
#include <QPen>

/*sheriton*/
//#define DEBUG_MSG
#ifdef DEBUG_MSG
#include <QDebug>
#endif


#define QT2STR(s) (s).toAscii().constData()

static const int ZOOM_DRAG_THRESHOLD = 5;
static const int POINT_SELECT_THRESHOLD = 6;

Polygon2::Polygon2()
{
    m_activePoint = -1;
    m_isClosed = false;
    m_isActive = false;
}

Polygon2::~Polygon2()
{
}

static QPen aux_createPen(const QColor& color, qreal width)
{
    QPen pen = QPen(color, width, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin);
    pen.setCosmetic(true);
    return pen;
}

void Polygon2::draw(QPainter& painter) const
{
    int n = size();
    if(n == 0) {
        return;
    }

    if(m_isActive) {
        painter.setPen(aux_createPen(Qt::red, 2));
        painter.setBrush(QBrush(QColor(255, 100, 100, 100), Qt::SolidPattern));
    } else {
        painter.setPen(aux_createPen(Qt::white, 2));
        painter.setBrush(QBrush(QColor(100, 100, 255, 50), Qt::SolidPattern));
    }

    // Edges and interiour
    if(!m_isClosed) {
        painter.drawPolyline(*this);
    } else {
        painter.drawPolygon(*this, Qt::WindingFill);
    }

    // Vertices
    if(m_isActive) {
        painter.setPen(aux_createPen(Qt::black, 3));
        painter.drawPoints(*this);
    }

    // Active vertex
    if(m_isActive) {
        int activeIndex = getActiveIndex();
        if(activeIndex >= 0) {
            painter.setPen(aux_createPen(Qt::yellow, 3));
            painter.drawPoint(at(activeIndex));
        }
    }
}

void Polygon2::print(QPainter& painter, const QColor& color) const
{
    int n = size();
    if(n < 3) {
        return;
    }

     painter.setPen(QPen(color, 2, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
     painter.setBrush(QBrush(color, Qt::SolidPattern));
     painter.drawPolygon(*this, Qt::WindingFill);
}

void Polygon2::close()
{
    m_isClosed = true;
}

void Polygon2::activate()
{
    m_isActive = true;
        m_activePoint = -1;
}

void Polygon2::deactivate()
{
    m_isActive = false;
}

bool Polygon2::insertPoint(const QPointF& point)
{
    if(size() > 1 && getIndexAtPoint(point, POINT_SELECT_THRESHOLD) == 0) {
        close();
        return false;
    }
    int index = getActiveIndex() + 1;
    insert(index, point);
    m_activePoint = index;
    return true;
}

void Polygon2::selectPoint(int index)
{
    if(index >= 0 && index < size()) {
        m_activePoint = index;
    } else {
        m_activePoint = -1;
    }
}

void Polygon2::removeActivePoint()
{
    int index = getActiveIndex();
    if (index!=-1) {
        remove(index);
        m_activePoint = index - 1;
    }
}

inline static double _length2(const QPointF& vector)
{
    return (vector.x() * vector.x()) + (vector.y() * vector.y());
}

void Polygon2::moveActivePoint(const QPointF& position, const QPointF& old)
{
    int activeIndex = getActiveIndex();
    if(activeIndex >= 0) {
        if (_length2(old - at(activeIndex))
                    < (POINT_SELECT_THRESHOLD * POINT_SELECT_THRESHOLD))
            replace(activeIndex, position);
    }
}

int Polygon2::getIndexAtPoint(const QPointF& point, int threshold) const
{
    double distance = 99999.9f;
    int index = -1;
    for(int i = 0; i < size(); i++) {
        double d = _length2(point - at(i));
        if(d < distance) {
            distance = d;
            index = i;
        }
    }

    if(index != -1 && distance <= (threshold * threshold)) {
        return index;
    } else {
        return -1;
    }
}

int Polygon2::getActiveIndex() const
{
    return m_activePoint;
}

/******************************************************************************
******************************************************************************/

PolygonCanvas::PolygonCanvas(QWidget* parent)
    : QWidget(parent)
{
    m_dataSet = new DataSet;
    m_dataOwner = true;
    m_dataSet->m_image = NULL;
    m_dataSet->m_activePolygon = NULL;
    m_dataSet->m_activeIndex = -1;

    m_currentTool = TOOL_NONE;

    m_west = 0.0;
    m_north = 0.0;
    m_east = 1.0;
    m_south = 1.0;

    m_eventActive = false;
    m_scaleFactor = 1.0f;
}

void PolygonCanvas::setAsProxy(PolygonCanvas* dataCanvas)
{
    if(m_dataOwner) {
        delete m_dataSet;
        m_dataOwner = false;
    }
    m_dataSet = dataCanvas->m_dataSet;
}

void PolygonCanvas::selectTool(Tool tool)
{
    closeActivePolygon();
    deselectPolygon();
    clearNonPolygons();
    m_currentTool = tool;

    if (tool == TOOL_PAN)
        setCursor(Qt::OpenHandCursor);
    else if (tool == TOOL_EDIT)
        setCursor(Qt::ArrowCursor);
    else if (tool == TOOL_CREATE)
        setCursor(Qt::CrossCursor);
    else if (tool == TOOL_ZOOM)
        setCursor(QCursor(QPixmap(":QPolygons/icons/zoom_cursor.png")));
    else if (tool == TOOL_DELETE)
        setCursor(QCursor(QPixmap(":QPolygons/icons/delete_cursor.png")));
    else
        setCursor(Qt::ArrowCursor);

}

void PolygonCanvas::setImage(QImage* image, bool _updatePosition)
{
#ifdef CANVAS_DONO_IMAGEM
    if(m_dataSet->m_image != NULL) {
        delete m_dataSet->m_image;
    }
#endif
    m_dataSet->m_image = image;
    if(_updatePosition) {
        updatePosition();
    }
}

void PolygonCanvas::updatePosition()
{
    if(m_dataSet->m_image != NULL) {
        m_viewCenter = QPointF(m_dataSet->m_image->width()/2.0f, m_dataSet->m_image->height()/2.0f);

        QRect view = getViewArea();
        double hRatio = static_cast<float>(view.width()) / m_dataSet->m_image->width();
        double vRatio = static_cast<float>(view.height()) / m_dataSet->m_image->height();

        m_scaleFactor = std::min(hRatio, vRatio);
    }
    updateScaling();
}

void PolygonCanvas::setGeoCoordinates(double west, double north, double east, double south)
{
    m_west = west;
    m_north = north;
    m_east = east;
    m_south = south;
}

bool PolygonCanvas::saveRegions(const QString& fileName)
{
    if(m_dataSet->m_image == NULL) {
        return false;
    }

    clearNonPolygons();

    QImage outImage(m_dataSet->m_image->size(), QImage::Format_RGB888);
    outImage.fill(0);

    QPainter painter(&outImage);

    const unsigned char COLOR_BASE = 96;
    const int NUM_COMBINATIONS = 7;
    const char channelCombinations[NUM_COMBINATIONS][3] = {
        {1,0,0}, {0,1,0}, {0,0,1},
        {1,1,0}, {1,0,1}, {0,1,1},
        {1,1,1}
    };
    unsigned int levelStep = 0;
    if(m_dataSet->m_polygons.size() > NUM_COMBINATIONS) {
        // A seguinte opera��o � o teto da divis�o dos inteiros
        // "m_dataSet->m_polygons.size()" e "NUM_COMBINATIONS"
        unsigned int levelsPerCombination =
            (m_dataSet->m_polygons.size() + (NUM_COMBINATIONS-1)) / NUM_COMBINATIONS;

        levelStep = (256 - COLOR_BASE) / (levelsPerCombination - 1);
        if(levelStep == 0) {
            return false;
        }
    }

    unsigned int indexComb = 0;
    unsigned char colorLevel = 255;
    QList<Polygon2>::iterator it;
    for(it = m_dataSet->m_polygons.begin(); it != m_dataSet->m_polygons.end(); it++) {
        const char* combination = channelCombinations[indexComb];
        indexComb = (indexComb + 1) % NUM_COMBINATIONS;

        QColor color(combination[0] * colorLevel,
            combination[1] * colorLevel,
            combination[2] * colorLevel);

        if(indexComb == 0) {
            colorLevel -= levelStep;
        }

        it->print(painter, color);
    }
    return outImage.save(QString(fileName), "TIFF");
}

/*
    if(m_dataSet->m_image == NULL) {
        return false;
    }

    clearNonPolygons();

    QImage outImage(m_dataSet->m_image->size(), QImage::Format_Mono);
    outImage.fill(0);

    QPainter painter(&outImage);

    QList<Polygon>::iterator it;
    for(it = m_dataSet->m_polygons.begin(); it != m_dataSet->m_polygons.end(); it++) {
        it->print(painter);
    }
    return outImage.save(fileName);
*/

static QString _aux_removeExtension(const QString& str)
{
    int bar = std::max(str.lastIndexOf("/"), str.lastIndexOf("\\"));
    int dot = str.lastIndexOf(".");
    if(dot > bar) {
        return str.left(dot);
    }
    return str;
}

bool PolygonCanvas::saveShapes(const QString& _fileName)
{

    if(m_dataSet->m_image == NULL) {
        return false;
    }

    clearNonPolygons();

    QString fileName = _aux_removeExtension(_fileName);

    DBFHandle dbfHandle = NULL;
    SHPHandle shapeHandle = NULL;
    SHPObject* shapeObject = NULL;
    bool ok = true;

    try {
        // SHP and SHX
        shapeHandle = SHPCreate(fileName.toStdString().c_str(), SHPT_POLYGON);

        if(shapeHandle == NULL) throw false;

        // DBF
        dbfHandle = DBFCreate(fileName.toStdString().c_str());

        if(dbfHandle == NULL) throw false;

        int field = DBFAddField(dbfHandle, "id", FTInteger, 1, 0);

        if(field < 0) throw false;

        // Data
        double xOffset = m_west;
        double yOffset = m_north;
        double xScale = ((m_east - m_west) / static_cast<double>(m_dataSet->m_image->width()));
        double yScale = ((m_south - m_north) / static_cast<double>(m_dataSet->m_image->height()));
        QList<Polygon2>::iterator it;
        for(it = m_dataSet->m_polygons.begin(); it != m_dataSet->m_polygons.end(); it++) {
            int n = it->size();
            if(n == 0) continue;

            double* x = new double[n+1];
            double* y = new double[n+1];
            for(int i = 0; i < n; i++) {
                x[i] = (static_cast<double>(it->at(i).x()) * xScale) + xOffset;
                y[i] = (static_cast<double>(it->at(i).y()) * yScale) + yOffset;
            }

            x[n] = x[0];
            y[n] = y[0];

            shapeObject = SHPCreateSimpleObject(SHPT_POLYGON, n+1, x, y, NULL);
/*sheriton*/
#ifdef DEBUG_MSG
            if(shapeObject == NULL) {
                qDebug() << "saveShapes() - shapeObject == NULL";
                throw false;
            }
#else
            if(shapeObject == NULL) throw false;
#endif

            int shape = SHPWriteObject(shapeHandle, -1, shapeObject);
/*sheriton*/
#ifdef DEBUG_MSG
            if(shape < 0) {
                qDebug() << "saveShapes() - shape < 0";
                throw false;
            }
#else
            if(shape < 0) throw false;
#endif
            SHPDestroyObject(shapeObject);
            shapeObject = NULL;
/*sheriton*/
#ifdef DEBUG_MSG
            qDebug() << "iShape = " << shape;
            qDebug() << "iField = " << field;
            qDebug() << "nFieldValue = " << shape << "\n";
#endif

            /* sheriton */
            DBFWriteIntegerAttribute(dbfHandle, shape, field, shape);

            /* comentado por sheriton */
            /* por alguma raz�o, o codigo abaixo impedia que fosse criado um shapefile
              com mais de 10 pol�gonos */
//            if(DBFWriteIntegerAttribute(dbfHandle, shape, field, shape) != TRUE) {
//                throw false;
//            }
        }
    } catch(bool e) {
        ok = e;
    }

    if(shapeObject != NULL) { SHPDestroyObject(shapeObject); }
    if(shapeHandle != NULL) { SHPClose(shapeHandle); }
    if(dbfHandle != NULL) { DBFClose(dbfHandle); }

    return ok;
}

bool PolygonCanvas::loadShapes(const QString& _fileName)
{
    if(m_dataSet->m_image == NULL) {
        return false;
    }

    QString fileName = _aux_removeExtension(_fileName);

    SHPHandle shapeHandle = NULL;
    SHPObject* shapeObject = NULL;
    bool ok = true;

    try {
        shapeHandle = SHPOpen(fileName.toStdString().c_str(), "rb");
        if(shapeHandle == NULL) throw false;

        int n, type;
        SHPGetInfo(shapeHandle, &n, &type, NULL, NULL);
        if(n == 0) throw true; // true = not an error
        if(type != SHPT_POLYGON) throw false;

        for(int s = 0; s < n; s++) {
            shapeObject = SHPReadObject(shapeHandle, s);
            if(shapeObject == NULL) throw false;
            if(shapeObject->nSHPType != SHPT_POLYGON) continue;
            if(shapeObject->nVertices <= 0) continue;

            m_dataSet->m_polygons.append(Polygon2());
            Polygon2* polygon = &m_dataSet->m_polygons.back();

            double xOffset = m_west;
            double yOffset = m_north;
            double xScale = ((m_east - m_west) / static_cast<double>(m_dataSet->m_image->width()));
            double yScale = ((m_south - m_north) / static_cast<double>(m_dataSet->m_image->height()));
            for(int v = 0; v < shapeObject->nVertices-1; v++) {
                polygon->append(QPointF(
                    (shapeObject->padfX[v] - xOffset) / xScale,
                    (shapeObject->padfY[v] - yOffset) / yScale
                ));
            }
            polygon->close();

            SHPDestroyObject(shapeObject);
            shapeObject = NULL;
        }
    } catch(bool e) {
        ok = e;
    }

    if(shapeObject != NULL) { SHPDestroyObject(shapeObject); }
    if(shapeHandle != NULL) { SHPClose(shapeHandle); }

    repaint();
    return ok;
}

void PolygonCanvas::clearRegions()
{
    m_dataSet->m_polygons.clear();
    m_dataSet->m_activePolygon = NULL;
    m_dataSet->m_activeIndex = -1;
    repaint();
}

bool PolygonCanvas::hasShapes() const
{
    return (m_dataSet->m_polygons.size() != 0);
}

void PolygonCanvas::selectTool(int tool)
{
    selectTool(static_cast<Tool>(tool));
}

void PolygonCanvas::zoomIn()
{
    m_scaleFactor *= 1.5f;
    updateScaling();
}

void PolygonCanvas::zoomOut()
{
    m_scaleFactor /= 1.5f;
    updateScaling();
    repaint();
}

void PolygonCanvas::paintEvent(QPaintEvent* event)
{
    QPainter painter(this);
    paintEvent(event, painter);
}

void PolygonCanvas::paintEvent(QPaintEvent* event, QPainter& painter) const
{
    // Background
    QRect view = painter.viewport();
    painter.fillRect(view, QColor(96, 96, 96, 255));
    // Image
    QRectF source;
    QPointF origin = windowToImageCoords(QPoint(0, 0));
    source.setTopLeft(origin);
    source.setBottomRight(windowToImageCoords(QPoint(view.width(), view.height())));
    if(m_dataSet->m_image != NULL) {
        painter.drawImage(view, *m_dataSet->m_image, source);
    }

    // Polygons
    painter.scale(m_scaleFactor, m_scaleFactor);
    painter.translate(-origin);
    QList<Polygon2>::const_iterator it;
    for(it = m_dataSet->m_polygons.begin(); it != m_dataSet->m_polygons.end(); it++) {
        it->draw(painter);
    }

    // Events (zoom area)
    painter.setTransform(QTransform(), false);
    if(m_currentTool == TOOL_ZOOM && m_eventActive) {
        painter.setPen(aux_createPen(Qt::yellow, 1));
        painter.setBrush(QBrush(QColor(255, 180, 40, 100), Qt::SolidPattern));
        QRect rect(m_eventStart, m_eventEnd);
        rect = rect.normalized();
        painter.drawRect(rect);
    }
}

void PolygonCanvas::mousePressEvent(QMouseEvent* event)
{

    if(m_currentTool == TOOL_PAN) {
        setCursor(Qt::ClosedHandCursor);
    }

    QPoint relativePoint = event->pos();
    QPointF absolutePoint = windowToImageCoords(relativePoint);
    Qt::MouseButton button = event->button();
    m_eventStart = relativePoint;
    m_eventEnd = relativePoint;
    m_eventActive = (button == Qt::LeftButton);
    switch(m_currentTool) {
        case TOOL_CREATE:
            if(button == Qt::LeftButton) {
                insertAtPoint(absolutePoint);
            } else if(button == Qt::RightButton) {
                removeActivePoint();
            }
            break;
        case TOOL_EDIT:
            if(button == Qt::LeftButton) {
                if (!selectAtPoint(absolutePoint))
                {
                    int index = getIndexAtPoint(absolutePoint);
                    if(index >= 0)
                        selectPolygon(index);
                }
                repaint();

            }
            break;
        case TOOL_DELETE:
            if(button == Qt::LeftButton) {
                removeAtPoint(absolutePoint);
            }
            break;
        default:
            break;
    }
}

void PolygonCanvas::mouseReleaseEvent(QMouseEvent* event)
{

    if(m_currentTool == TOOL_PAN) {
        setCursor(Qt::OpenHandCursor);
    }

    QPoint relativePoint = event->pos();
    QPointF absolutePoint = windowToImageCoords(relativePoint);
    Qt::MouseButton button = event->button();
    m_eventActive = false;
    switch(m_currentTool) {
        case TOOL_ZOOM:
            if(button == Qt::RightButton) {
                //m_viewCenter = absolutePoint;
                zoomOut();
            } else if(button == Qt::LeftButton) {
                if(_length2(m_eventEnd - m_eventStart)
                    < (ZOOM_DRAG_THRESHOLD * ZOOM_DRAG_THRESHOLD)) { // didn't drag
                        m_viewCenter = absolutePoint;
                        zoomIn();
                } else { // dragged
                    QRect view = getViewArea();
                    QRectF area(windowToImageCoords(m_eventStart), windowToImageCoords(m_eventEnd));
                    area = area.normalized();
                    double hRatio = static_cast<float>(view.width()) / area.width();
                    double vRatio = static_cast<float>(view.height()) / area.height();
                    m_scaleFactor = std::min(hRatio, vRatio);
                    m_viewCenter = area.center();
                }
            }
            break;
        default:
            break;
    }
    repaint();
}

void PolygonCanvas::mouseDoubleClickEvent(QMouseEvent* event)
{
    if(m_currentTool == TOOL_CREATE && event->button() == Qt::LeftButton) {
        removeActivePoint();
        closeActivePolygon();
    } else {
        // Makes double click work as two separate clicks
        mousePressEvent(event);
    }
}

void PolygonCanvas::mouseMoveEvent(QMouseEvent* event)
{
    QPoint relativePoint = event->pos();
    QPointF absolutePoint = windowToImageCoords(relativePoint);
    m_eventEnd = relativePoint;
    switch(m_currentTool) {
        case TOOL_ZOOM:
            repaint();
            break;
        case TOOL_PAN:
            m_viewCenter -= QPointF(m_eventEnd - m_eventStart) / m_scaleFactor;
            m_eventStart = m_eventEnd;
            repaint();
            break;
        case TOOL_CREATE:
        case TOOL_EDIT:
            if(event->buttons() & Qt::LeftButton) {
                moveActivePoint(absolutePoint,windowToImageCoords(m_eventStart));
                m_eventStart = m_eventEnd;
            }
            break;
        default:
            break;
    }
}

bool PolygonCanvas::selectAtPoint(const QPointF& point)
{
    for(int i = m_dataSet->m_polygons.size()-1; i >= 0; i--) {
        int index = m_dataSet->m_polygons.at(i).getIndexAtPoint(point, POINT_SELECT_THRESHOLD);
        if(index >= 0) {
            selectPolygon(i);
            m_dataSet->m_activePolygon->selectPoint(index);
            repaint();
            return true;
        }
    }
    deselectPolygon();
    return false;
}

void PolygonCanvas::insertAtPoint(const QPointF& point)
{
    if(m_dataSet->m_activePolygon == NULL || m_dataSet->m_activePolygon->isClosed()) {
        insertPolygon();
    }
    if(!m_dataSet->m_activePolygon->insertPoint(point)) {
        deselectPolygon();
    }
    repaint();
}

void PolygonCanvas::removeAtPoint(const QPointF& point)
{
    if(selectAtPoint(point)) {
        removeActivePoint();
        repaint();
    } else {
        int index = getIndexAtPoint(point);
        if(index >= 0) {
            removePolygon(index);
            repaint();
        }
    }
}

void PolygonCanvas::closeActivePolygon()
{
    if(m_dataSet->m_activePolygon != NULL) {
        m_dataSet->m_activePolygon->close();
        deselectPolygon();
        repaint();
    }
}

void PolygonCanvas::moveActivePoint(const QPointF& position, const QPointF& old)
{
    if(m_dataSet->m_activePolygon != NULL) {
        m_dataSet->m_activePolygon->moveActivePoint(position, old);
        repaint();
    }
}

void PolygonCanvas::removeActivePoint()
{
    if(m_dataSet->m_activePolygon != NULL) {
        m_dataSet->m_activePolygon->removeActivePoint();
        if(m_dataSet->m_activePolygon->size() == 0) {
            removePolygon(m_dataSet->m_activeIndex);
        }
        repaint();
    }
}

QRect PolygonCanvas::getViewArea() const
{
    return rect();
}

QPoint PolygonCanvas::imageToWindowCoords(const QPointF& point) const
{
    QRect window = getViewArea();
    int x = static_cast<int>((point.x() - m_viewCenter.x()) + (m_scaleFactor * window.width()  / 2));
    int y = static_cast<int>((point.y() - m_viewCenter.y()) + (m_scaleFactor * window.height() / 2));
    return QPoint(x, y);
}

QPointF PolygonCanvas::windowToImageCoords(const QPoint& point) const
{
    QRect window = getViewArea();
    double x = ((point.x() - (window.width()  / 2.0f)) / m_scaleFactor) + m_viewCenter.x();
    double y = ((point.y() - (window.height() / 2.0f)) / m_scaleFactor) + m_viewCenter.y();
    return QPointF(x, y);
}

void PolygonCanvas::insertPolygon()
{
    m_dataSet->m_polygons.append(Polygon2());
    selectPolygon(m_dataSet->m_polygons.size() - 1);
}

void PolygonCanvas::removePolygon(int index)
{
    if(m_dataSet->m_activeIndex == index) {
        deselectPolygon();
    }
    m_dataSet->m_polygons.removeAt(index);
}

void PolygonCanvas::selectPolygon(int index)
{
    if(index < 0 || index >= m_dataSet->m_polygons.size()) {
        deselectPolygon();
    } else if(index != m_dataSet->m_activeIndex) {
        deselectPolygon();
        m_dataSet->m_activeIndex = index;
        m_dataSet->m_activePolygon = const_cast<Polygon2*>(&m_dataSet->m_polygons.at(index));
        m_dataSet->m_activePolygon->activate();
    }
}

void PolygonCanvas::deselectPolygon()
{
    if(m_dataSet->m_activeIndex >= 0) {
        m_dataSet->m_activePolygon->deactivate();
        m_dataSet->m_activeIndex = -1;
        m_dataSet->m_activePolygon = NULL;
    }
}

int PolygonCanvas::getIndexAtPoint(const QPointF& point)
{
    for(int i = m_dataSet->m_polygons.size()-1; i >= 0; i--) {
        if(m_dataSet->m_polygons.at(i).containsPoint(point, Qt::WindingFill)) {
            return i;
        }
    }
    return -1;
}

void PolygonCanvas::updateScaling()
{
    repaint();
}

void PolygonCanvas::clearNonPolygons()
{
    for(int i = m_dataSet->m_polygons.size()-1; i >= 0; i--) {
        if(m_dataSet->m_polygons.at(i).size() < 3) {
            removePolygon(i);
        }
    }
    repaint();
}


double PolygonCanvas::getGeoWest()
{
    return m_west;
}

double PolygonCanvas::getGeoNorth()
{
    return m_north;
}

double PolygonCanvas::getGeoEast()
{
    return m_east;
}

double PolygonCanvas::getGeoSouth()
{
    return m_south;
}
