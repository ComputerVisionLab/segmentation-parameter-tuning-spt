#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "DESolver.h"
#include "PSSolver.h"
#include "NMSolver.h"
#include "ShpFunct.h"
#include "polygon_canvas.h"
#include "optinfo.h"
#include <QLabel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    //Tab Reference: Load Image Button
    void on_ref_loadImButton_clicked();
    //Tab Reference: Load Reference Button
    void on_ref_loadRefButton_clicked();
    //Tab Optimization: Index of Optimization Algorithms Combo Box changed
    void on_opt_optAlgComboBox_currentIndexChanged(int index);
    //Tab Optimization: Index of Segmentation Algorithms Combo Box changed
    void on_opt_segAlgComboBox_currentIndexChanged(int index);
    //Tab Optimization: Run Optimization button
    void on_opt_runPushButton_clicked();
    //Tab Reference: Compose Image button
    void on_ref_composeButton_clicked();
    //Function that execute the optimization algorithms
    void execute_optimization();    
    //Tab Reference: Save Reference button
    void on_ref_saveRefButton_clicked();
    //Tab Reference: Clear Reference button
    void on_ref_clearRefButton_clicked();
    //Tab Optimization: Show Output button
    void on_opt_outPushButton_clicked();
    //Tab Segmentation: Index of Segmentation Algorithms Combo Box changed
    void on_seg_segAlgComboBox_currentIndexChanged(int index);
    //Tab Segmentation: Run button
    void on_seg_runPushButton_clicked();
    //Tab Segmentation: Load Reference button
    void on_seg_refLoadPushButton_clicked();
    //Tab Assessment: Load Image button
    void on_assess_LoadImButton_clicked();
    //Tab Assessment: Compose button
    void on_assess_composeButton_clicked();
    //Tab Assessment: Load Segmentation Outcome button
    void on_assess_LoadSegOut_clicked();
    //Tab Assessment: Load Reference button
    void on_assess_LoadRef_clicked();
    //Tab Assessment: Clear Reference button
    void on_assess_ClearRef_clicked();
    //Tab Assessment: Calculate button
    void on_assess_Calculate_clicked();
    //Tab Optimization: Multiseg - Image type
    void on_opt_segAlgComboBox3_currentIndexChanged(int index);
    //Tab Segmentation: Multiseg - Image type
    void on_seg_segAlgComboBox3_currentIndexChanged(int index);
    //Tab Segmentation: Show Output button
    void on_seg_outPushButton_clicked();
    //Tab Segmentation: Calculate button
    void on_seg_refCalculatePushButton_clicked();
    //Tab Segmentation: Clear References button
    void on_seg_clearRefButton_clicked();
    //Tab Reference: SLOT to update the Table to list the segments
    void ref_updateSegmentsTable();
    //Tab Optimization: SLOT to show the number of iterations
    void opt_number_iterations(int n);
    //Tab Optimization: Slot to get the ID of current experiment
    void opt_n_ID(int temp_ID);
    //Tab Optimization: Slot to get the number of executions of target function
    void opt_n_executions(int temp_exec);
    //Tab Optimization: Slot to display a QMessageBox when the experiments have finished
    void opt_show_result();
    //Tab Optimization: Slot to set the value of the progress bar to 100% when it finished before 100%
    void set_FullProcess();
    //Tab Reference: Select all the available references to be saved
    void on_ref_SelectAllButton_clicked();
    //Tab Reference: Clear selection of all or selected references to be saved
    void on_ref_ClearAllButton_clicked();
    //Tab Segmentation: Load a TXT File with many segmentation values to be evaluated (research purpose)
    void on_seg_LoadParamsFilePushButton_clicked();    
    //Tab Optimization: Read how many lines the file with initial points have
    void opt_readLinesInitialPoints();

    void on_opt_optAdvComboBox_currentIndexChanged(int index);

    void on_opt_optAdvComboBox_currentTextChanged(const QString &arg1);

    void on_ref_loadRefTestButton_clicked();

signals:
    void loading_showed(int m);
    void cancel_pressed();

private:
    Ui::MainWindow *ui;    
    QString version;//Current version of SPT
    QString orgComposedName;//Name of RGB composed image with selected bands by the user (Reference Tab)
    QString initialImgFileName;//Original Name of Input Image in Reference and Optimization Tabs
    QString initialRefFileName;//Original Name of Reference (Ground Truth) in Reference and Optimization Tabs
    QString orgImFileName;//Name of Input Image copied to ./temp folder for further processing in Reference and Optimization Tabs
    QString refImFileName;//Name of Reference or Ground Truth (raster or vector) for Reference and Optimization Tabs
    QString refImTestFileName;//Name of Reference or Ground Truth (raster or vector) for Reference and Optimization Tabs
    vector<vector<Point2f> >  ui_refVectorPoints;//Vector of Polygons containing the Reference for Reference and Optimization Tabs
    vector<vector<Point2f> >  ui_refTestVectorPoints;//Vector of Polygons containing the Reference for Testing in Optimization Tabs
    bool is_twfFile;//logical variable that indicates if there is a TFW file (Reference Tab)
    double *twfValues;//Pointer with the values of the TFW file in the following order: x_res,y_res,rot_x,rot_y,upperleft_east,upperleft_north
    int ui_segmentationIndex;//Current index of Seg. Algorithms Combo box (Optimization Tab)
    int ui_metricIndex;//Current index of Metrics Combo box (Optimization Tab)
    int ui_nDim;//Number of dimensions of search space, it represents the number of variables to be tuned by SPT (Optimization Tab)
    int ui_population, ui_generations, ui_strategy;//Parameters related to the available optimization algorithms (Optimization Tab)
    int ui_maxIter;//Maximum number of iterations allowed for each optimization algorithm (Optimization Tab)
    double ui_error, ui_minMeshSize;//Parameters related to the available optimization algorithms (Optimization Tab)
    QString ui_search;//Search Strategy for GPS and MADS algorithms (Optimization Tab)
    double *ui_min;//Minimum values for each parameter to be tuned (Optimization Tab)
    double *ui_max;//Maximum values for each parameter to be tuned (Optimization Tab)
    double *ui_solutions;//Final values of each parameter after the optimization phase (Optimization Tab)
    double *ui_firstTrialPoint;//First Trial Point of the optimization procedure (Optimization Tab)
    double ui_stop;//Final value of the fitness function (Optimization Tab)
    int ui_iteration;//Number of iterations performed by the optimization algorithm (Optimization Tab) Actually, it refers to the number of times that the target function has been executed.
    int ui_n_iterations;//Number of iterations performed by the optimization algorithm (Optimization Tab)
    double* ui_fixed;//Fixed parameters belonging to a segmentation procedure (Optimization Tab)
    int ui_nDimFixed;//Number of fixed parameters belonging to a segmentation procedure (Optimization Tab)
    std::string ui_out_name;//Name of Segmentation Outcome produced by the optimal parameters found (Optimization Tab)
    std::string ui_final_result_name;//Name of Segmentation Outcome (raster) produced by the optimal parameters found copied to ./Result folder (Optimization Tab)
    std::string ui_final_shapefiles;//Name of Segmentation Outcome (vector) produced by the optimal parameters found copied to ./Result folder (Optimization Tab)
    PolygonCanvas* ui_canvas;//Widget oriented to show the input image and reference segments in Reference Tab
    PolygonCanvas* ui_canvas_opt;//Widget oriented to show the input image and reference segments in Optimization Tab
    PolygonCanvas* ui_canvas_seg;//Widget oriented to show the input image and reference segments in Segmentation Tab
    PolygonCanvas* ui_canvas_assess_out;//Widget oriented to show the segmentation outcome (vector) in Assessment Tab
    PolygonCanvas* ui_canvas_assess_ref;//Widget oriented to show the reference segments (vector) in Assesment Tab
    QString assessOrigImgName;//Original name of base image for Assessment Tab
    QString assessImFileName;//Name of Input Image for Assessment Tab
    QString assess_SegOutName;//Name of a Segmentation Outcome (raster or vector) copied to based path for Assessment Tab
    QString assess_RefName;//Name of Reference or Ground Truth (raster or vector) copied to based path for Assessment Tab
    QString assess_orig_SegOutName;//Original Name of a Segmentation Outcome (raster or vector) for Assessment Tab
    QString assess_orig_RefName;//Original Name of Reference or Ground Truth (raster or vector) for Assessment Tab
    vector< vector<Point2f> > assess_SegOutPolygons;//Vector of Polygons containing the Segmentation Outcome for Assessment Tab
    vector< vector<Point2f> > assess_RefPolygons;//Vector of Polygons containing the Reference Segments for Assessment Tab
    int ui_state_out_button;//State of Show Output button in Optimization Tab
    int ui_output_info;//State of the Output Information button in Optimization Tab
    int ui_state_segOut_button;//State of Show Output button in Segmentation Tab
    QString COLOR_SELECTED;//Color of a button to be selected
    QString COLOR_DEFAULT;//Default color of any button
    vector<vector<Point2f> >  seg_RefPolygons;//Vector of Polygons containing the Reference Segments for Segmentation Tab
    vector<vector<Point2f> > seg_SegOutPolygons;//Vector of Polygons containing the Segmentation Outcome for Segmentation Tab
    std::string ui_seg_OutName;//Name of Segmentation Outcome (raster) for Segmentation Tab
    QString seg_SegOutName;//Name of Segmentation Outcome (vector) for Segmentation Tab
    QString seg_RefName;//Name of Reference Segments (raster or vector) for Segmentation Tab
    //Rows, Cols and Bands for Reference, Optimization and Segmentation Tabs
    int ui_rows_ref;
    int ui_cols_ref;
    int ui_bands_ref;
    //Rows, Cols and Bands for Assessment Tab
    int ui_rows_assess;
    int ui_cols_assess;
    int ui_bands_assess;
    //Centroids of Seg. Outcome and Reference (Ground Truth)
    vector<Point2f> ui_ref_RefCentroids;
    vector<Point2f> ui_assess_SegOutCentroids;
    vector<Point2f> ui_assess_RefCentroids;
    //QImages to save results and just call them to show them in the GUI
    QImage *img_seg_output;
    QImage *img_seg_composed;
    //Number of times to repeat the optimization procedure
    int ui_times_to_run;
    OptInfo GlobalInfo;//Class to storage all the global variables necessary to generate the final report with the results of the optmization step.
    //Threads Definition
    QThread* thread_opt;
    int seg_nSegRun;//Number of times that a segmentation have been executed (Tab Segmentation)    
    //QString for tool tips for each label for Optimization Tab
    //Optimization
    QString opt_optAlgComboBox_toolTipMsg;
    QString opt_optLabel1_toolTipMsg;
    QString opt_optLabel2_toolTipMsg;
    QString opt_optLabel3_toolTipMsg;
    QString opt_optLabel4_toolTipMsg;
    QString opt_optLabel5_toolTipMsg;
    //Segmentation
    QString opt_segLabel1_toolTipMsg;
    QString opt_segLabel2_toolTipMsg;
    QString opt_segLabel3_toolTipMsg;
    QString opt_segLabel4_toolTipMsg;
    QString opt_segLabel5_toolTipMsg;
    QString opt_segLabel5Coeff_toolTipMsg;
    QString opt_segLabelBands_toolTipMsg;
    //Fitness Function
    QString opt_metric_toolTipMsg;
    //QString for tool tips for each label for Segmentation Tab
    QString seg_segLabel1_toolTipMsg;
    QString seg_segLabel2_toolTipMsg;
    QString seg_segLabel3_toolTipMsg;
    QString seg_segLabel4_toolTipMsg;
    QString seg_segLabel5_toolTipMsg;
    QString seg_segLabel9_toolTipMsg;
    QString seg_segLabelBands_toolTipMsg;

public:    
    bool readInf(QString fileName, int &height, int &width, int &bands);//Read information from TXT file generated with the values of rows,cols and bands.

private:
    void init_variables();
    void init_reference_editor_tab(Ui::MainWindow *ui);
    void init_optimization_tab(Ui::MainWindow *ui);
    void init_segmentation_tab(Ui::MainWindow *ui);
    void init_assessment_tab(Ui::MainWindow *ui);
    void init_about_tab(Ui::MainWindow *ui);

    void fill_tooltip_reference_editor_tab(Ui::MainWindow *ui);
    void fill_tooltip_optimization_tab(Ui::MainWindow *ui);
    void fill_tooltip_segmentation_tab(Ui::MainWindow *ui);

    void clean_temp_dirs();
};

#endif // MAINWINDOW_H
