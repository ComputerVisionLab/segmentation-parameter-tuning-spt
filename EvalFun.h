#ifndef EVALFUN_H
#define EVALFUN_H
#include <math.h>
#include "ShpFunct.h"
#include "Segmentation.h"

class EvalFun
{
public:    
    static double EnergyFunction(double *var,
                                 QString input_image,
                                 QString output_image,
                                 vector<vector<Point2f> > reference,
                                 int segIdx, int metIdx, int rows, int cols,
                                 double* fixParams, double* min, double* max, int nDim, string bands_string);
};

#endif // EVALFUN_H
