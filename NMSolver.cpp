#include "NMSolver.h"

using namespace std;

NMSolver::NMSolver(int dim) :
                    nDim(dim), iterations(0), bestEnergy(1.0E20), bestSolution(0)
{
    bestSolution  = new double[nDim];
    min_ = new double[nDim];
    max_ = new double[nDim];
    firstTrialPoint = new double[nDim];
    return;
}

NMSolver::~NMSolver(void)
{
    if (bestSolution) delete bestSolution;
    if (min_) delete min_;
    if (max_) delete max_;
    if (firstTrialPoint) delete firstTrialPoint;
    bestSolution = 0;
    return;
}

double NMSolver::EnergyFunction(double *trial)
{
        iterations++;
        emit n_Executions(iterations);

        bool is_outLimits = false;
        for (int id = 0 ; id < nDim ; id++)
        {
            if (trial[id] < min_[id] || trial[id] > max_[id])
                is_outLimits = true;
        }

        if (is_outLimits)
            m_currentEnergy = 1.0E20;
        else
        {
            m_currentEnergy =  EvalFun::EnergyFunction(trial,m_input_image,m_output_image,m_reference,
                                               m_seg_index,m_metric_index,m_img_rows,m_img_cols,
                                               m_fixed,min_,max_,nDim,m_GlobalInfo.get_bands_string().toStdString());
        }

        emit eval_exec(m_current_File);               

        ofstream myFile;
        myFile.open(m_current_File.toStdString().c_str(),std::ios::app);
        for (int i = 0 ; i < nDim ; i++)
        {
            if (i == (nDim-1))
            {   myFile << trial[i] << "\n";}
            else
            {   myFile << trial[i] << "	";}
        }
        myFile.close();
        return m_currentEnergy;
}

void NMSolver::Setup(double min[], double max[], QString searchOpt,
                     QString input_image, QString output_image,
                     vector<vector<Point2f> > reference,
                     int segIdx, int metIdx, int ref_rows, int ref_cols,
                     double* fixParams, int nDimFixed, int maxIt, double error, OptInfo GlobalInfo)
{
    //////////////////////////////////////////////////////
    //Added by Pedro
    //Parameters for the segmentation and metric
    m_input_image = input_image;
    m_output_image = output_image;
    m_reference = reference;
    m_seg_index = segIdx;
    m_metric_index = metIdx;
    m_img_rows = ref_rows;
    m_img_cols = ref_cols;
    m_nDimFixed = nDimFixed;
    m_fixed = new double[m_nDimFixed];
    m_maxIt = maxIt;
    m_error = error;
    m_searchOpt = searchOpt;    
    m_GlobalInfo.copyFrom(GlobalInfo);    
    m_times_to_run =  m_GlobalInfo.get_times_to_run();

    if(nDimFixed > 1)
    {
        for(int i = 0 ; i < nDimFixed ; i++)
        {
            m_fixed[i] = fixParams[i];            
        }
    }
    else
    {
        m_fixed[0] = 0;
    }
    qDebug()<<"Passed parameters";
    //////////////////////////////////////////////////////

    for (int i=0; i < nDim; i++)
    {
       min_[i]=min[i];
       max_[i]=max[i];
    }

    if (m_searchOpt == "Set_P[0]")
    {
        SearchPo();
    }

    return;
}

void NMSolver::SearchPo()
{
    QFile file("Init_P0.txt");
    m_P0.clear();
    m_bestEnergyP0.clear();

    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug()<<"Error opening with initial points.";
    }

    QTextStream iter(&file);
    while(!iter.atEnd())
    {
        QString line = iter.readLine();
        QStringList fields = line.split(",");
        vector<double> temp;
        for (int p = 0 ; p < (fields.length()-1); p++)
        {
            temp.push_back(fields.at(p).toDouble());
        }
        m_P0.push_back(temp);
        m_bestEnergyP0.push_back(fields.at(fields.length()-1).toDouble());
    }
    file.close();
}

void NMSolver::Solve()
{
    //Run many times the experiment, each time, it will save the results
    for(int times = 0 ; times < m_times_to_run ; times++)
    {
        //Emit and send the ID of current experiment
        iterations = 0;
        emit n_ID(times+1);
        double time_process = 0;
        clock_t start, end;
        start = clock();

        QString name_energy("Energy_NM_");
        name_energy.append(QString::number(times));
        name_energy.append(".txt");
        ofstream myfile;
        myfile.open(name_energy.toStdString().c_str());
        myfile << "trialEnergy	bestEnergy	trialPoint...\n";
        myfile.close();
        m_current_File = name_energy;
        bestEnergy = 1.0E20;

        //Setup values for an experiment
        for (int i=0; i < nDim; i++)
            bestSolution[i] = 1.0E20;

        //Initialization with or without DE
        if(m_searchOpt == "SearchOff")
            SearchOff(min_,max_);
        else if (m_searchOpt == "SearchDE")
        {
            //TXT File to save the intermediate results related to DE initialization
            QString DE_name("Energy_NM_DE_");
            DE_name.append(QString::number(times));
            DE_name.append(".txt");
            SearchDE(min_,max_);
            QFile::copy("Energy_DE_0.txt",DE_name);
            QFile::remove("Energy_DE_0.txt");
        }
        else if (m_searchOpt == "Set_P[0]")
        {
            vector<double> firstPoint;
            firstPoint = m_P0.at(times);

            for (int i = 1; i <= nDim; i++)
            {
                V[0][i] = firstPoint.at(i-1);
            }
            bestEnergy = m_bestEnergyP0.at(times);
        }

        //Providing the initial Point to begin the Nelder Mead Optimization
        for (int i = 1; i <= nDim; i++)
            firstTrialPoint[i-1] = V[0][i];

        qDebug() << "Solve process begin";

        //bool bAtSolution = false;
        int N;                     /* Number of variables in EnergyFunction */
        int m = 0;                 /* While loop counter                    */
        double Tol = 1E-6;                /* Convergence criterion                 */
        N = nDim;

        InitialSimplex(N);

        /* Compute function value at V_j */
        for (int j = 0; j <= N; j++)
        {
            for (int i = 1; i <= N; i++)
            {
//                if (V[j][i] < min_[i-1]) V[j][i] = min_[i-1];
//                if (V[j][i] > max_[i-1]) V[j][i] = max_[i-1];
                Farg[i-1] = V[j][i];
            }
            Y[j] = EnergyFunction(Farg);
        }
        qDebug() << "Compute first initial energies";

        Order(N);

        while ( (Y[Hi] > (Y[Lo] + Tol)) && (m < m_maxIt))
//        while (m < m_maxIt)
        {
            emit n_iterations(m);
            Newpoints(N);
            Improve(N);
            Order(N);
            bestEnergy = Y[Lo];
            m = iterations;                            /* Increment the counter */
            if(bestEnergy <= m_error)
                break;
        }
        Size(N);

    /* Output */
        for (int i = 1; i <= N; i++)
        {
            bestSolution[i-1] = V[Lo][i];
//            if (bestSolution[i-1] < min_[i-1]) bestSolution[i-1] = min_[i-1];
//            if (bestSolution[i-1] > max_[i-1]) bestSolution[i-1] = max_[i-1];
        }
        qDebug()<<"bestSolution point: "<< bestSolution[0] << bestSolution[1] << bestSolution[2];
        qDebug()<<"firstTrialPoint point: "<< firstTrialPoint[0] << firstTrialPoint[1] << firstTrialPoint[2];
        qDebug() << "Solve process end";
        end = clock();
        time_process = ((double)end - (double)start)/CLOCKS_PER_SEC;
        Save_Results(times,m,time_process);        
    }    
    emit finished();
}

void NMSolver::Order(int N)
{
    int j;

    /* Find indices : Lo, best vertex; Hi, worst vertex */
    Lo = 0.0;
    Hi = 0.0;

    for ( j = 1; j <= N; j++ )
    {
        if (Y[j] < Y[Lo] ) Lo = j;
        if (Y[j] > Y[Hi] ) Hi = j;
    }

    /* Find indices : Li, best second vertex; Ho, worst second vertex */
    Li = Hi;
    Ho = Lo;

    for ( j = 0; j <= N; j++ )
    {
        if ( (j != Lo) && (Y[j] < Y[Li] ) ) Li = j;
        if ( (j != Hi) && (Y[j] > Y[Ho] ) ) Ho = j;
    }
    /* The order will be: Lo Li ... Ho Hi */
}

void NMSolver::Size(int N)
{
   /* Size of simplex */
    int i, j;
    double S;

    Norm = 0.0;
    for ( j = 0; j <= N; j++ )
    {
        S = 0;
        for ( i = 1; i <= N; i++ )  S += pow( (V[Lo][i] - V[j][i]), 2.0 );
        if ( S > Norm ) Norm = S;
    }
    Norm = sqrt(Norm);

}

void NMSolver::Newpoints(int N)
{
    // V[simplex point][dimensions of each point]
    int k, j;
    double S;

    for ( k = 1; k <= N; k++ )
    {
        S = 0.0;
        for ( j = 0; j <= N; j++ ) S += V[j][k];
        M[k] = ( S - V[Hi][k] ) / N;
    }

    //another way to manage the limits of the search space is to penalize
    //the value of the target function giving a very high value
//    bool outBound = false;
//    for ( k = 1; k <= N; k++ )
//    {
//        R[k] = 2.0 * M[k] - V[Hi][k];
//        if (R[k] < min_[k-1]) outBound = true;
//        if (R[k] > max_[k-1]) outBound = true;
//        Farg[k-1] = R[k];
//    }
//    if(outBound)
//        YR = 1E20;
//    else
//        YR = EnergyFunction(Farg);

    for ( k = 1; k <= N; k++ )
    {
        R[k] = 2.0 * M[k] - V[Hi][k];
//        if (R[k] < min_[k-1]) R[k] = min_[k-1];
//        if (R[k] > max_[k-1]) R[k] = max_[k-1];
        Farg[k-1] = R[k];
    }
    YR = EnergyFunction(Farg);
}

void NMSolver::Shrink(int N)
{
    int j, k;

    for ( j = 0; j <= N; j++ )
    {
        if( j != Lo )
        {
          for ( k = 1; k <= N; k++ )
          {
              V[j][k] = ( V[j][k] + V[Lo][k] )/ 2.0;
//              if (V[j][k] < min_[k-1]) V[j][k] = min_[k-1];
//              if (V[j][k] > max_[k-1]) V[j][k] = max_[k-1];
              Farg[k-1] = V[j][k];
          }
          Y[j] = EnergyFunction(Farg);
        }
    }
}

void NMSolver::Replace(int N)
{
    int k;
    /* Replace V with R */
    for ( k = 1; k <= N; k++ ) V[Hi][k] = R[k];
    Y[Hi] = YR;
}

void NMSolver::Improve(int N)
{
    int k;
    double E[VarMax], C[VarMax];
    double YE, YC;

    /* N-M : use Y[Lo], not Y[Li] */
    if ( YR < Y[Ho] )
    {
        if ( Y[Li] < YR ) Replace(N);   /* Replace V with R */
        else
        {
            /* Construct E */
            for (k = 1; k <= N; k++)
            {
                E[k] = 2.0 * R[k] - M[k];
//                if (E[k] < min_[k-1]) E[k] = min_[k-1];
//                if (E[k] > max_[k-1]) E[k] = max_[k-1];
                Farg[k-1] = E[k];
            }
            YE = EnergyFunction(Farg);

            /* N-M: use Y[Lo], not Y[Li] */
            if ( YE < Y[Li] )
            {
               /* Replace V with E */
               for (k = 1; k <= N; k++) V[Hi][k] = E[k];
               Y[Hi] = YE;
            }
            else Replace(N);   /* Replave V with R */
        }
    }
    else
    {
       if( YR < Y[Hi] ) Replace(N);  /* Replace V with R */

       for (k = 1; k <= N; k++)
       {
           C[k] = ( V[Hi][k] + M[k] ) / 2.0;
//           if (C[k] < min_[k-1]) C[k] = min_[k-1];
//           if (C[k] > max_[k-1]) C[k] = max_[k-1];
           Farg[k-1] = C[k];
       }
       YC = EnergyFunction(Farg);

       if( YC < Y[Hi] )
       {
           for (k = 1; k <= N; k++) V[Hi][k] = C[k]; /* Replace V with C */
           Y[Hi] = YC;
       }
       else Shrink(N);
    }  /* End of all conditions */
}

void NMSolver::InitialSimplex(int N)
{
    double *p = new double[nDim];
    double *q = new double[nDim];
    double *sizeSimplex = new double[nDim];
    double S;
//    double p,q, sizeSimplex, S;
    double e[VarMax][VarMax], sumq[VarMax];   /* unit base vectors */

    for (int j = 0; j < N; j++)
    {
        sizeSimplex[j] = (max_[j]-min_[j])*0.2;

        p[j] = sizeSimplex[j]*(sqrt((double)(N+1))+N-1)/(N*sqrt(2.0));
        q[j] = sizeSimplex[j]*(sqrt((double)(N+1))-1)/(N*sqrt(2.0));
    }

//For the basis vectors
    for (int j = 0; j <= N; j++)
    {
        for (int i = 1; i <= N; i++)
        {
            e[j][i]= 0;
            if (i==j)
                e[j][i]= 1;
        }
    }
    qDebug() << "Vector basis defined";

//For the new points
    for (int j = 1; j <= N; j++)
    {
        for ( int a = 1; a <= N; a++ )
        {
            S = 0;
            for ( int b = 1; b <= N; b++ ) if( b!=j ) S += q[b-1]*e[b][a];
            sumq[a] = S;
        }

        for (int i = 1; i <= N; i++)
        {
            V[j][i] = V[0][i]+p[i-1]*e[j][i]+sumq[i];
            qDebug()<< "V[" << j << "][" << i << "] = " << V[j][i]<<"   ";
        }
    }
    qDebug() << "Initialization process ends";
}

void NMSolver::SearchOff(double *min, double *max)
{
    qDebug()<< "SearchOff start";
    //For the start point
    srand((unsigned)time(0));
    double randD;
    randD = rand()%(100)+1;

    for (int i = 1; i <= nDim; i++)
    {
        V[0][i] = min[i-1] + randD*(max[i-1] - min[i-1])/125;
    }

    qDebug()<< "SearchOff end";
    return;
}

void NMSolver::SearchDE(double *min, double *max)
{
    qDebug()<< "DESolver start";    

    DESolver *solverSearchDE = new DESolver(nDim,10);
    solverSearchDE->Setup(min,max,stBest1Exp,0.5,0.8,1E-10,
                         m_input_image,m_output_image,
                         m_reference,m_seg_index,m_metric_index,
                         m_img_rows,m_img_cols,m_fixed,m_nDimFixed,4,false,m_GlobalInfo);

    connect(solverSearchDE,SIGNAL(eval_exec(QString)),solverSearchDE,SLOT(Write_Energy(QString)));

    qDebug()<< "DESolver setup";
    solverSearchDE->Solve();

    double* firstPoint = new double [nDim];
    bestEnergy = solverSearchDE->Energy();
    firstPoint = solverSearchDE->Solution();

    disconnect(solverSearchDE,SIGNAL(eval_exec(QString)),solverSearchDE,SLOT(Write_Energy(QString)));

    for (int i = 1; i <= nDim; i++)
    {
        V[0][i] = firstPoint[i-1];
        //qDebug()<< "V[" << firstPoint[i-1] << "][" << i << "] = " << V[0][i]<<"   ";
    }
    qDebug()<< "DESolver end";
    return;
}

void NMSolver::Save_Results(int ID,int m,double time)
{
    double stop = EnergyFunction(bestSolution);//Y[Lo];
    string final_result_name, final_shapefiles;

    qDebug() << QString("Fitness: %1  Iterations: %2").arg(stop).arg(iterations);

    //Only for Region Growing (BAATZ), input and output images have to be BMPs
    if(m_seg_index == 0)
    {
        final_result_name = QString("./Results/").toStdString() + m_GlobalInfo.get_opt_outLineEdit().toStdString() + ".png";
    }
    else if(m_seg_index == 4)//Only for MultiSeg
    {
        final_result_name = QString("./Results/").toStdString() + m_GlobalInfo.get_opt_outLineEdit().toStdString() + ".jpg";
    }
    else
    {
        final_result_name = QString("./Results/").toStdString() + m_GlobalInfo.get_opt_outLineEdit().toStdString() + ".png";
    }

    final_shapefiles = QString("./Results/OutputVector").toStdString();

    //Copying result files from ./temp to ./Results
    QFile::copy(QString(m_GlobalInfo.get_out_name().c_str()),QString(final_result_name.c_str()));
    QFile::copy(QString("./temp/result3.shp"),QString::fromStdString(final_shapefiles+".shp"));
    QFile::copy(QString("./temp/result3.shx"),QString::fromStdString(final_shapefiles+".shx"));
    QFile::copy(QString("./temp/result3.dbf"),QString::fromStdString(final_shapefiles+".dbf"));

    float result_testing = evaluate_metric(readShapeFile(final_shapefiles),
                                           readShapeFile(m_GlobalInfo.get_refImTestFileName().toStdString()),
                                           m_metric_index,
                                           m_img_rows,m_img_cols);

    QFile data(QString("./Results/report")+QString::number(ID)+(".txt"));
    data.open(QFile::WriteOnly | QFile::Truncate);
    QTextStream output(&data);
    output << "              Segmentation Parameter Tuner v." << m_GlobalInfo.get_version() << "\n";
    output << "**************************************************************\n\n";
    output << "General Information: \n";
    output << " - Base Image:   " << m_GlobalInfo.get_initialImgFileName() << "\n";
    output << " - Reference Training:    " << m_GlobalInfo.get_initialRefFileName() << "\n";
    output << " - Reference Testing:    " << m_GlobalInfo.get_refImTestFileName()<< "\n";
    output << " - Image Dim.:   " << QString::number(m_GlobalInfo.get_rows_ref()) + " x " +
                                     QString::number(m_GlobalInfo.get_cols_ref()) + " x " +
                                     QString::number(m_GlobalInfo.get_bands_ref()) << "\n\n\n";
    output << "General Configurations: \n";
    output << " - Optimization Algorithm:   " << m_GlobalInfo.get_optAlgComboBox_text() << "\n";
    output << " - Segmentation Algorithm:   " << m_GlobalInfo.get_segAlgComboBox_text() << "\n";
    output << " - Fitness Function:         " << m_GlobalInfo.get_fitComboBox_text() << "\n\n\n";
    output << "Output Results: \n";
    output << " - Output Image:     " << QString(final_result_name.c_str()) << "\n";
    output << " - Vector Data:      " << QString::fromStdString(final_shapefiles+".shp") << "\n";
    output << " - Fitness Value (Training):    " << stop << "\n";
    output << " - Fitness Value (Testing):  " << result_testing<< "\n";
    output << " - Num. Execution of Target Function:  " << iterations << "\n";
    output << " - Num. Iterations:  " << m << "\n";
    output << " - Processing Time:  " << time << "\n";
    output << " - ID :" << ID << "\n";

    output << " - First Trial Point:" << "\n";
    for(int p = 0 ; p < nDim; p++)
    {
        output<<"              " << firstTrialPoint[p] << "\n";
    }

    output << " - Parameters tuned: \n";

    //Label 1 ///////////////////////////////////////////////////////////////////////////////////////////
    if (m_seg_index == 3)//only for Graph-based, it is double
    {
        if (!m_GlobalInfo.get_opt_segLabel1_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel1_text() << ":   " << (double)bestSolution[0] << "\n";
    }
    else//for the others, it is INT
    {
        if (!m_GlobalInfo.get_opt_segLabel1_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel1_text() << ":   " << (int)bestSolution[0] << "\n";
    }

    //Label 2 ///////////////////////////////////////////////////////////////////////////////////////////
    if((m_seg_index == 0) || (m_seg_index == 3))//only for Baatz and Graph-based
    {
        if (!m_GlobalInfo.get_opt_segLabel2_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel2_text() << ":   " << (double)bestSolution[1] << "\n";

    }
    else if ((m_seg_index == 1) || (m_seg_index == 2))//for Meanshift and SPRING, it is INT
    {
        if (!m_GlobalInfo.get_opt_segLabel2_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel2_text() << ":   " << (int)bestSolution[1] << "\n";
    }
    else if(m_seg_index == 4)
    {
        if (!m_GlobalInfo.get_opt_segLabel2_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel2_text() << ":   " << ((int)((double)bestSolution[1]/0.2))*0.2 << "\n";
    }

    //Label 3 ///////////////////////////////////////////////////////////////////////////////////////////
    if(m_seg_index == 0)
    {
        if (!m_GlobalInfo.get_opt_segLabel3_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel3_text() << ":   " << (double)bestSolution[2] << "\n";
    }
    else if((m_seg_index == 1)||(m_seg_index == 2)||(m_seg_index == 3))
    {
        if (!m_GlobalInfo.get_opt_segLabel3_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel3_text() << ":   " << (int)bestSolution[2] << "\n";
    }
    else if(m_seg_index == 4)//Just for Multiseg
    {
        int conf = (int)bestSolution[2];
        double conf_real;
        switch (conf)
        {
        case 1:
            conf_real = 0.80;break;
        case 2:
            conf_real = 0.85;break;
        case 3:
            conf_real = 0.90;break;
        case 4:
            conf_real = 0.95;break;
        case 5:
            conf_real = 0.99;break;
        case 6:
            conf_real = 0.995;break;
        case 7:
            conf_real = 0.999;break;
        default:
            break;
        }
        if (!m_GlobalInfo.get_opt_segLabel3_bool())
            output << "              " << m_GlobalInfo.get_opt_segLabel3_text() << ":   " << QString::number(conf_real) << "\n";
    }

   //Label 4 ///////////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel4_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel4_text() << ":   " << (int)bestSolution[3] << "\n";

   //Label 5 ///////////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel5_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel5_text() << ":   " << (int)bestSolution[4] << "\n";

   //Label 6 ///////////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel6_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel6_text() << ":   " << m_GlobalInfo.get_opt_segAlgComboBox2_text() << "\n";

   //Label 7 ///////////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel7_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel7_text() << ":   " << m_GlobalInfo.get_opt_segAlgComboBox3_text() << "\n";

   //Label 8 ///////////////////////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel8_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel8_text() << ":   " << m_GlobalInfo.get_opt_segAlgComboBox4_text() << "\n";
   //Label 5 Coeff of Variation/////////////////////////////////////////////////////////////////////////
   if (!m_GlobalInfo.get_opt_segLabel5_Coeff_bool())
        output << "              " << m_GlobalInfo.get_opt_segLabel5_Coeff_text() << ":   " << bestSolution[4] << "\n";
    data.close();
}

void NMSolver::Write_Energy(QString myFileName)
{
    ofstream myFile;
    myFile.open(myFileName.toStdString().c_str(),std::ios::app);
    myFile << m_currentEnergy << "	" << bestEnergy <<"	";
    myFile.close();
}
