#ifndef CONSOLEFUNCTIONS_H
#define CONSOLEFUNCTIONS_H

#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <string>
#include <QString>
#include <QtCore>
#include <QObject>
#include "DESolver.h"
#include "PSSolver.h"
#include "NMSolver.h"
#include "optinfo.h"
#include "ShpFunct.h"

struct ExperimentData
{
    ExperimentData();

    QString InputImage;
    QString TrainingReference;
    QString TestingReference;

    QString OptimizationAlgorithm;
    std::vector<double> OptimizationParameters;

    QString SegmentationAlgorithm;
    std::vector<double> SegParameter1MinMax;
    std::vector<double> SegParameter2MinMax;
    std::vector<double> SegParameter3MinMax;
    std::vector<double> SegParameter4MinMax;
    std::vector<double> SegParameter5MinMax;
    int SegParameterFixed1;
    QString SegParameterFixed2;
    int ImageType;
    int ImageModel;
    int Format;
    int Metric;
    int TimesToRun;
    QString OutputFileName;

    std::vector<double> twfValues;
    bool is_twfFile;

    //Functions
    void show();
    QThread *thread_console;

};

//Functions
ExperimentData ReadConfigurationFIle (const char* filename);

void display_vector(const std::vector<double> &v);

OptInfo FillGlobalInfo(ExperimentData ConfigurationData);

int GetnDim(ExperimentData ConfigurationData);

double* Getui_min(ExperimentData ConfigurationData, int ui_nDim);
double* Getui_max(ExperimentData ConfigurationData, int ui_nDim);

QString Get_TestingReferenceName(ExperimentData ConfigurationData);

std::vector<std::vector<cv::Point2f> > Getui_refVectorPoints(ExperimentData &ConfigurationData);

void Get_uiFixed_uinDimFixed(double* &ui_fixed, int &ui_nDimFixed, ExperimentData ConfigurationData);

int Getui_segmentationIndex(ExperimentData ConfigurationData);

void RunExperiment(ExperimentData ConfigurationData);


#endif // CONSOLEFUNCTIONS_H
