#ifndef SEGMENTATION_H
#define SEGMENTATION_H

#include <QDebug>
#include <QtDebug>
#include <fstream>
#include <QProcess>
#include <QStringList>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "shapefil.h"
#include "ShpFunct.h"
#include "omp.h"

#define NUM_THREADS 8
#define MIN_PERCENTAGE 0.60

using namespace cv;
using namespace std;

//Class to save stats of two contours
class Areas_stats
{
public:
    Areas_stats();
    int area1;
    int area2;
    int intersection;
    int perimeter1;
    int perimeter2;
};

//----------------------------------------------------------------------------------------------------------------------------------------------------------
//SEGMENTATION ALGORITHMS
//----------------------------------------------------------------------------------------------------------------------------------------------------------
//Region Growing (BAATZ)
void region_growing_baatz_rgb(string input_image,string output_image, int scale, float w_color, float w_compct, int rows, int cols);
void region_growing_baatz_hdr(string input_image,string output_image, int scale, float w_color, float w_compct, int rows, int cols);
void region_growing_baatz(string input_image,string output_image, int scale, float w_color, float w_compct, int rows, int cols);
//Mean-Shift segmentation - EDISON
void mean_shift_edison(string input_image,string output_image, int spat_rad, int range_rad, int min_area, int rows, int cols);
void mean_shift_edison_hdr(string input_image,string output_image, int spat_rad, float range_rad, int min_area, int rows, int cols);
void mean_shift_edison_gdal(string input_image,string output_image, int spat_rad, int range_rad, int min_area, int rows, int cols);
//SPRING
void region_growing_spring(string input_image,string output_image, int min_distance, int min_area, int max_iter, int rows, int cols);
void region_growing_spring_gdal(string input_image,string output_image, int min_distance, int min_area, int rows, int cols);
//Graph-based segmentation
void graph_based(string input_path,string output_image, double sigma, double threshold, int min_area, int rows, int cols);
void graph_based_hdr(string input_path,string output_image, double sigma, double threshold, int min_area, int rows, int cols);
void graph_based_gdal(string input_path,string output_image, double sigma, double threshold, int min_area, int rows, int cols);
//MultiSeg
void multi_seg(string input_image, string output_image, int image_model, int image_type, int radar_format, int levels, double simil,int conf,int min_area, int n_elo, string bands, int rows, int cols);
void multi_seg2(string input_image, string output_image, int image_model, int image_type, int radar_format, int levels, double simil, int conf, int min_area, int n_elo, double coeff_variation, string bands, int rows, int cols);
//Use the selected Segmentation
void evaluate_segmentation(string input_image, string output_image, double* vars, double *fixed, int rows, int cols, int index, string bands_string);

//----------------------------------------------------------------------------------------------------------------------------------------------------------
//METRICS
//----------------------------------------------------------------------------------------------------------------------------------------------------------
//Hoover Index - H [Hoover, 1996]
float Hoover_index(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, float threshold, int rows, int cols);
//Area Fit Index - AFI [Lucieer, 2004]
float Area_Fit_Index(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols);
//Segmentation Covering - C [Point Tuset, 2013]
float Segmentation_Covering(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols);
//Reference Bounded Segments Booster - RBSB [Feitosa, 2006]
float RBSB(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols);
//Shape Index (Average Difference between them) - SI [Neubert, 2008]
float Shape_Index(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols);
//Rand Index - (RI) [Pont Tuset, 2013]
float PRI(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols);
//Precision and Recall - F measure
float Precision_Recall(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, float &P, float &R, int rows, int cols);
//Use the selected metric:
float evaluate_metric(vector<vector<Point2f> > MS, vector<vector<Point2f> >GT, int index, int rows, int cols);

//----------------------------------------------------------------------------------------------------------------------------------------------------------
//OTHER FUNCTIONS
//----------------------------------------------------------------------------------------------------------------------------------------------------------
//Find the intersection between two contours
Areas_stats AreaofIntersection(vector<vector<Point2f> > contour1 , vector<vector<Point2f> > contour2,int pos1, int pos2, int rows, int cols, bool show,bool perimeter);
//Get the area and perimeter of the reference with its largest segment that overlaps it
bool overlap_area_perimeter(int posMS, vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols, float threshold, Areas_stats &areas_temp, bool perimeter);
//Get the largest overlaping
bool largest_overlapping(int posMS, vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols, float threshold, Areas_stats &areas_temp);
//Analyze if there is a correct detection between two contours
bool correct_detection(int posMS, vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols, float threshold, int &posGT);
//Gets all overlapping contours between two vector of contours
void getAllOverlapping(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols, vector<int> &GT_index, vector<vector<int> > &MS_index);
//Gets all the contours that overlaps to one specific contour, if there is any, it only have one element, index -1
void getOverlaps(vector<vector<Point2f> > MS, vector<vector<Point2f> > GT, int rows, int cols, int posMS, vector<int> &GT_index);
//Convert a PLM file to any other image format
unsigned char read_plm(string f_input, string f_output);
//Convert double/int to string
string stringify(double x);
string stringify(int x);
//Reduce contours to leave out small contours
vector< vector<Point2f> > reduce_contour(vector< vector<Point2f> > MS, int rows, int cols);

#endif // SEGMENTATION_H
