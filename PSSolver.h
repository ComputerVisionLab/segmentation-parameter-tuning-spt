#ifndef PSSOLVER_H
#define PSSOLVER_H
#include <qstring.h>
#include <memory.h>
#include <ctime>
#include "ShpFunct.h"
#include "EvalFun.h"
#include "DESolver.h"
#include <QObject>
#include "optinfo.h"

class PSSolver : public QObject
{
    Q_OBJECT
public:
    PSSolver(int dim);
    ~PSSolver(void);

    void Setup(double min[], double max[],QString searchOpt,
               QString input_image, QString output_image,
               vector<vector<Point2f> > reference,
               int segIdx, int metIdx, int ref_rows, int ref_cols,
               double *fixParams, int nDimFixed,
               int meshIterMax, double meshSizeMin, double error, OptInfo GlobalInfo);

    void SearchOff(double min[],double max[]);
    void SearchUD(double min[],double max[]);
    void SearchDE(double min[],double max[]);
    void SearchPo();

public slots:
    virtual void Solve();
    void Write_Energy(QString myFileName, int i);

    ////////////////////////////////////////////////////////
    //              MADS IMPLEMENTATION                   //
    ////////////////////////////////////////////////////////
public:
    void SetupMADS(double *min, double *max, QString searchOpt,
                   QString input_image, QString output_image,
                   vector<vector<Point2f> > reference,
                   int segIdx, int metIdx, int ref_rows, int ref_cols,
                   double *fixParams, int nDimFixed,
                   int meshIterMax, double meshSizeMin, double error, OptInfo GlobalInfo);
public slots:
    virtual void SolveMADS();
signals:
    void n_iterations(int n);
    void n_ID(int temp_ID);
    void n_Executions(int temp_exec);
    void finished();
     void eval_exec(QString current_file, int i);
public:
    void BasisFrame(void);

    double EnergyFunction(double trial[]);

    // Call these functions after Solve() to get results.
    int Dimension(void) { return(nDim); }
    double Energy(void) { return(bestEnergy); }
    double *Solution(void) { return(bestSolution); }
    int Iterations(void) { return(iterations); }
    double MeshSize(void) { return(delta); }
    double *GetFirstTrialPoint(void) { return(firstTrialPoint);}
    void Save_Results(int ID, int m, double time);

protected:
    int nDim;               //Dimension of the problem
    int iterations;         //Number of evalutaions

    //Initialization
    double *firstTrialPoint;

    double trialEnergy;     //Energy at each function evaluation
    double bestEnergy;      //Best Energy finded so far
    double *trialPoint;     //Trial points to evaluate the new pattern coordinates with the best points
    double *trialSolution;  //Best actual solution, with wich the trialPoint will be made
    double *bestSolution;   //Best points, with the smallest trial energy
    double *popEnergy;      //Energy evaluation for each pattern coordinate vector
    double *population;     //Vector where all the trial points are stored.
    double *pattern;        //Pattern vector
    double *min_;           //Minimum values for the trial points
    double *max_;           //Maximum values for the trial points
    double delta;           //Actual mesh size

    double *initPoint;
    double *initEnergy;
    //Variables that used to be in Solve()
    int m_meshIterMax;
    double m_meshSizeMin;
    double m_error;

    //MADS Variables
    double deltaM;
    double deltaP;
    double m_meshSize;
    double* scaleDelta;

    //Variables for Segmentation
    QString m_input_image;
    QString m_output_image;
    vector<vector<Point2f> >  m_reference;
    int m_seg_index;
    int m_metric_index;
    int m_img_rows;
    int m_img_cols;
    double* m_fixed;
    int m_nDimFixed;
    OptInfo m_GlobalInfo;
    int m_times_to_run;
    QString m_searchOpt;
    double m_currentEnergy;
    QString m_current_File;
    vector<vector<double>> m_P0;
    vector<double> m_bestEnergyP0;

private:
};

#endif // PSSOLVER_H
