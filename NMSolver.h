#ifndef NMSOLVER_H
#define NMSOLVER_H
#include <math.h>
#include "ShpFunct.h"
#include "EvalFun.h"
#include "DESolver.h"
#define  VarMax  10     /* Upper limit for number of variables */
#include <QObject>
#include "optinfo.h"

class NMSolver : public QObject
{    
    Q_OBJECT
public:
    NMSolver(int dim);
    ~NMSolver(void);

    void Setup(double min[], double max[], QString searchOpt,
               QString input_image, QString output_image,
               vector<vector<Point2f> > reference,
               int segIdx, int metIdx, int ref_rows, int ref_cols,
               double *fixParams, int nDimFixed, int maxIt, double error, OptInfo GlobalInfo);

    void SearchOff(double min[],double max[]);
    void SearchDE(double min[],double max[]);
    void SearchPo();

public slots:
    virtual void Solve();    
    void Write_Energy(QString myFileName);
signals:
    void n_iterations(int n);
    void n_ID(int temp_ID);
    void n_Executions(int temp_exec);
    void finished();
    void eval_exec(QString current_file);
public:
    void Order(int N);
    void Size(int N);
    void Newpoints(int N);
    void Shrink(int N);
    void Replace(int N);
    void Improve(int N);
    void InitialSimplex(int N);

    double EnergyFunction(double trial[]);

    int Dimension(void) { return(nDim); }
    double Energy(void) { return(Y[Lo]); }
    double *Solution(void) { return(bestSolution); }    
    int Iterations(void) { return(iterations); }
    double MeshSize(void) { return(Norm); }
    double *GetFirstTrialPoint(void) {return(firstTrialPoint);}
    void Save_Results(int ID, int m, double time);

protected:

    //Initialization
    double *firstTrialPoint;

    double V[VarMax][VarMax];
    double Y[VarMax], R[VarMax], M[VarMax];
    int Lo, Li, Ho, Hi;
    double Norm, YR;
    double Farg[VarMax];    /* vector used as argument for ffunction */

    int nDim;               //Dimension of the problem
    int iterations;         //Number of evalutaions

    double bestEnergy;      //Best Energy finded so far
    double *bestSolution;   //Best points, with the smallest trial energy

    double *min_;           //Minimum values for the trial points
    double *max_;           //Maximum values for the trial points
    int m_maxIt;
    double m_error;

    //Variables for Segmentation
    QString m_input_image;
    QString m_output_image;
    vector<vector<Point2f> >  m_reference;
    int m_seg_index;
    int m_metric_index;
    int m_img_rows;
    int m_img_cols;
    double* m_fixed;
    int m_nDimFixed;
    int m_times_to_run;//How many times the experiment will be repeated
    QString m_searchOpt;
    OptInfo m_GlobalInfo;
    double m_currentEnergy;
    QString m_current_File;
    vector<vector<double>> m_P0;
    vector<double> m_bestEnergyP0;

};

#endif // NMSOLVER_H
